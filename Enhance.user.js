// ==UserScript==
// @name         Enhance
// @namespace    http://tampermonkey.net/
// @version      04.07.2020
// @description  Enhance your fodder!
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

// Settings

const delayToConfirmResources = 5; // delay in seconds to cancel script in case of fodder inbalance
const useManySRs = false; // Use more SRs, save more Rs ( true = SR3, false = SR4)
const useBonus = false; // Use +1 weapons as fodder
const onlyEnhanceToMax = false; // Don't level fodder halfway


// Setting Presets

const sr3Settings = {
    r: { target_level: 1, min_remaining: 0 },
    sr: { target_level: 3, min_remaining: 10 },
    ssr: { target_level: 5, min_remaining: 1000 },
    rGrail: { target_level: 4, min_remaining: 0 },
    srGrail: { target_level: 5, min_remaining: 0 },
    allowedExcess: 10
}

const sr4Settings = {
    r: { target_level: 1, min_remaining: 0 },
    sr: { target_level: 4, min_remaining: 10 },
    ssr: { target_level: 5, min_remaining: 1000 },
    rGrail: { target_level: 3, min_remaining: 0 },
    srGrail: { target_level: 5, min_remaining: 0 },
    allowedExcess: 5
}

// End Settings



// Script Start

var settings;

class Settings {
    constructor(settings) {
        this.r = settings.r;
        this.sr = settings.sr;
        this.ssr = settings.ssr;
        this.rGrail = settings.rGrail;
        this.srGrail = settings.srGrail;
        this.allowedExcess = settings.allowedExcess;
        this.useBonus = useBonus;
        this.onlyEnhanceToMax = onlyEnhanceToMax;
    }
}

if (useManySRs) {
    settings = new Settings(sr3Settings);
} else {
    settings = new Settings(sr4Settings);
}

async function start(){
    while (!(kh && kh.createInstance && kh.Api && kh.Api.AWeapons)) {
        console.log("sleep");
        await sleep(1000);
    }

    try {
        let enhanceManager = new EnhanceManager();
        enhanceManager.run();
    } catch (exception) {
        console.log(exception.message)
        await sleep(1000)
        start();
    }
}

class EnhanceManager {
    constructor() {
        this.weaponApi = kh.createInstance("apiAWeapons");
        this.fodder = [];
        this.targets = [];
        this.fodderTracker = new Tracker();
        this.targetTracker = new Tracker();
    }

    async run(){
        let availableWeapons = (await this.weaponApi.getList(1,1,1000)).body.data.filter(x => !x.is_locked && x.skill_level > 0).reverse(); // Get all available weapons
        this.prepareWeapons(availableWeapons); // Map weapons to containers
        await sleep(delayToConfirmResources * 1000); // Give people time to cancel enhance manually
        await this.enhanceTargets();
        console.log("Done enhancing.");
    }

    async prepareWeapons(availableWeapons) {
        availableWeapons.forEach(weaponObj => {
            this.assignWeapon(new Weapon(weaponObj)); // Create containers and split into fodder and targets
        });

        this.sortLists();
        this.logTracker();
    }

    assignWeapon(weapon) {
        if (weapon.isTarget()) {
            weapon.calculateRequiredPoints()
            this.targetTracker.add(weapon.type);
            this.targets.push(weapon);
        } else {
            if (!weapon.obj.is_equipped && (settings.useBonus || weapon.obj.bonus === 0)) {
                weapon.calculateEnhancePoints();
                this.fodderTracker.add(weapon.type);
                this.fodder.push(weapon);
            }
        }
    }

    sortLists() {
        this.sortTargets();
        this.sortFodder();
    }

    sortTargets() {
        this.targets.sort((a,b) => b.requiredPoints - a.requiredPoints);
    }

    sortFodder() {
        this.fodder.sort((a,b) => a.enhancePoints - b.enhancePoints);
    }

    logTracker() {
        this.targetTracker.log("Targets");
        this.fodderTracker.logWithMinimum("Fodder");
    }

    async enhanceTargets() {
        for (let i = this.targets.length - 1; i >= 0 ; i--) { // Loop Targets
            const target = this.targets[i];
            let requiredPoints = target.requiredPoints;
            let selectedIndizes = [];
            for (let j = this.fodder.length - 1; j >= 0; j--) { // Loop Fodder
                const fodder = this.fodder[j];
                if (!this.fodderTracker.canRemove(fodder.type) || fodder.enhancePoints > requiredPoints + settings.allowedExcess) { // Check if usable
                    continue;
                }
                selectedIndizes.push(j);
                this.fodderTracker.remove(fodder.type); // Remove fodder from tracker
                requiredPoints -= fodder.enhancePoints; // Update points counter
                if (requiredPoints <= 0) { // Break if enough fodder was gathered
                    break;
                }
            }

            if (selectedIndizes.length === 0 || settings.onlyEnhanceToMax && requiredPoints > 0) { // Check if target can be maxxed
                selectedIndizes.forEach(idx => {
                    this.fodderTracker.add(this.fodder[idx]); // Undo tracker changes, as we won't be using the fodder
                });
                continue;
            }

            let selectedFodder = []; // Map to fodder + remove fodder and target from lists
            selectedIndizes.forEach(idx => {
                let fodder = this.fodder.splice(idx, 1)[0];
                selectedFodder.push(fodder);
            });
            this.targets.splice(i, 1);
            this.targetTracker.remove(target.type);


            console.log(`Enhancing ${target.type} with ${selectedFodder.map(x => x.type + x.skill_level).join("+")}`);
            let result = (await this.weaponApi.enhance(target.obj.a_weapon_id, selectedFodder.map(x => x.obj.a_weapon_id))).body;
            this.assignWeapon(new Weapon(result.weapon)); // Create a new weapon
            this.sortFodder(); // In case new weapon got assigned as fodder, sort fodder list.
            this.logTracker();
        }
    }
}

class Weapon {
    constructor(weaponObj) {
        this.obj = weaponObj;
        this.skill_level = weaponObj.skill_level;
        this.requiredPoints = 0;
        this.enhancePoints = 0;
        this.type = this.getWeaponType();
        this.targetLevel = this.getTargetLevel();
    }

    getWeaponType() {
        switch (this.obj.rare) {
            case "R":
                return this.obj.name === "Arcane Grail" ? weaponType.R_GRAIL: weaponType.R;
            case "SR":
                return this.obj.name === "False Grail Yaldabaoth" ? weaponType.SR_GRAIL: weaponType.SR;
            case "SSR":
            return weaponType.SSR;
        }
    }

    getTargetLevel() {
        switch (this.type) {
            case weaponType.R:
                return settings.r.target_level;
            case weaponType.SR:
                return settings.sr.target_level;
            case weaponType.SSR:
                return settings.ssr.target_level;
            case weaponType.R_GRAIL:
                return settings.rGrail.target_level;
            case weaponType.SR_GRAIL:
                return settings.srGrail.target_level;
        }
    }

    isTarget() {
        return this.obj.skill_level < this.targetLevel;
    }

    calculateEnhancePoints() {
        switch (this.type) {
            case weaponType.R:
                this.enhancePoints = 10 * this.obj.skill_level;
                break;
            case weaponType.SR:
                this.enhancePoints = 35 * this.obj.skill_level;
                break;
            case weaponType.SSR:
                this.enhancePoints = 250 + 100 * (this.obj.skill_level - 1);
                break;
            case weaponType.R_GRAIL:
                this.enhancePoints = 20 * this.obj.skill_level;
                break;
            case weaponType.SR_GRAIL:
                this.enhancePoints = 50 * this.obj.skill_level;
                break;
        }
    }

    calculateRequiredPoints() {
        let target = this.targetLevel;
        let level = this.obj.skill_level;
        switch (this.type) {
            case weaponType.R:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 / 2 + this.obj.skill.next_exp;
                break;
            case weaponType.SR:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 + this.obj.skill.next_exp;
                break;
            case weaponType.SSR:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 10 + this.obj.skill.next_exp;
                break;
            case weaponType.R_GRAIL:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 / 2 + this.obj.skill.next_exp;
                break;
            case weaponType.SR_GRAIL:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 + this.obj.skill.next_exp;
                break;
        }
    }
}

class Tracker {
    constructor() {
        this.r = new TypeTracker(settings.r);
        this.rGrail = new TypeTracker(settings.rGrail);
        this.srGrail = new TypeTracker(settings.srGrail);
        this.sr = new TypeTracker(settings.sr);
        this.ssr = new TypeTracker(settings.ssr);
    }

    add(kind) {
        switch (kind) {
            case weaponType.R:
                this.r.add();
                break;
            case weaponType.R_GRAIL:
                this.rGrail.add();
                break;
            case weaponType.SR:
                this.sr.add();
                break;
            case weaponType.SR_GRAIL:
                this.srGrail.add();
                break;
            case weaponType.SSR:
                this.ssr.add();
                break;
        }
    }

    remove(kind) {
        switch (kind) {
            case weaponType.R:
                this.r.remove();
                break;
            case weaponType.R_GRAIL:
                this.rGrail.remove();
                break;
            case weaponType.SR:
                this.sr.remove();
                break;
            case weaponType.SR_GRAIL:
                this.srGrail.remove();
                break;
            case weaponType.SSR:
                this.ssr.remove();
                break;
        }
    }

    canRemove(kind) {
        switch (kind) {
            case weaponType.R:
                return this.r.canRemove();
            case weaponType.R_GRAIL:
                return this.rGrail.canRemove();
            case weaponType.SR:
                return this.sr.canRemove();
            case weaponType.SR_GRAIL:
                return this.srGrail.canRemove();
            case weaponType.SSR:
                return this.ssr.canRemove();
        }
    }

    logWithMinimum(name) {
        console.log(`${name}: r: ${this.r.current}(${this.r.minimum}), rg: ${this.rGrail.current}(${this.rGrail.minimum}), ` +
            `sr: ${this.sr.current}(${this.sr.minimum}), srg: ${this.srGrail.current}(${this.srGrail.minimum}), ssr: ${this.ssr.current}(${this.ssr.minimum})`);
    }

    log(name) {
        console.log(`${name}: r: ${this.r.current}, rg: ${this.rGrail.current}, ` +
            `sr: ${this.sr.current}, srg: ${this.srGrail.current}, ssr: ${this.ssr.current}`);
    }
}

class TypeTracker {
    constructor(setting) {
        this.minimum = setting.min_remaining;
        this.current = 0;
    }

    add() {
        this.current++;
    }

    remove() {
        this.current--;
    }

    canRemove() {
        return this.current > this.minimum;
    }
}

const weaponType = {
    R: "r",
    SR: "sr",
    SSR: "ssr",
    R_GRAIL: "rGrail",
    SR_GRAIL: "srGrail"
}

function sleep(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

function has(obj) {
    var prop;
    if (obj !== Object(obj)) {
        return false;
    }
    for (var i = 1; i < arguments.length; i++) {
        prop = arguments[i];
        if ((prop in obj) && obj[prop] !== null && obj[prop] !== 'undefined') {
            obj = obj[prop];
        } else {
            return false;
        }
    }
    return true;
}

console.log("Starting enhance");
start();
