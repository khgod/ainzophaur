declare const kh: any;
declare const botSettings: any;
declare const claimSettings: any;
declare const enhanceSettings: any;
declare const shopSettings: any;
declare const shopPresets: any;

class KhBot {
    private enhanceManager: EnhanceManager;
    private gemcha: Gemcha;
    private giftBox: GiftBox;
    private shop: Shop;
    private harem: Harem;
    private missions: Mission;
    private autoSellChecker: AutoSellChecker;
    private spaceTracker: SpaceTracker;
    private weaponBalancer: WeaponBalancer;

    constructor() {
        this.enhanceManager = new EnhanceManager();
        this.gemcha = new Gemcha();
        this.giftBox = new GiftBox();
        this.shop = new Shop();
        this.harem = new Harem();
        this.missions = new Mission();
        this.autoSellChecker = new AutoSellChecker();
    }

    public async start(): Promise<void> {
        await this.prepare();
        await this.run();
    }

    private async prepare(): Promise<void> {
        console.log("Prepare...");
        let giftBoxPromise = this.giftBox.prepare(this.enhanceManager.settings);
        let promises = [this.enhanceManager.prepareWeapons(), this.gemcha.prepare(), this.autoSellChecker.prepare()];

        this.spaceTracker = await giftBoxPromise;
        this.weaponBalancer = new WeaponBalancer(this.enhanceManager, this.gemcha, this.spaceTracker, this.autoSellChecker, this.giftBox);
        await Promise.all(promises);
    }
    
    private async run(): Promise<void> {
        await this.giftBox.claimOther(this.spaceTracker, this.autoSellChecker);
        await this.giftBox.claimSummons(this.spaceTracker, this.autoSellChecker);
        
        // Use all current enhance mats in weapons
        if (enhanceSettings.useEnhanceMats) {
            await this.weaponBalancer.useEnhanceMats(false);
        }

        // Enhance + Gemcha
        if (botSettings.enhanceWeapons) {
            await this.weaponBalancer.startEnhancing();
        } else if (botSettings.doGemcha) {
            await this.gemcha.pull(this.spaceTracker, this.autoSellChecker, this.spaceTracker.freeWeaponSlots(), this.spaceTracker.freeSummonSlots());
        }

        // Do onetime things
        if (botSettings.doShop) {
            await this.shop.exchange();
        }
        if (botSettings.watchHarem) {
            await this.harem.doHaremScenes();
        }
        if (botSettings.claimMissionRewards) {
            await this.missions.claim();
        }
        await this.giftBox.claimOther(this.spaceTracker, this.autoSellChecker);
        await this.giftBox.claimSummons(this.spaceTracker, this.autoSellChecker);

        // Use all (new) enhance mats from GiftBox
        if (enhanceSettings.useEnhanceMats) {
            await this.weaponBalancer.useEnhanceMats(true);
        }

        console.log("Done.");
        this.spaceTracker.log();
        console.log(`Open Gemcha pulls: ${this.gemcha.availablePulls}`);
        this.missions.log();
    }
}