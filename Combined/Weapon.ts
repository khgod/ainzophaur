enum WeaponType {
    R = "r",
    SR = "sr",
    SSR = "ssr",
    R_GRAIL = "rGrail",
    SR_GRAIL = "srGrail"
}

class Weapon {
    private targetLevel: number;

    public obj: any;
    public skillLevel: number;
    public requiredPoints: number;
    public enhancePoints: number;
    public type: WeaponType;

    constructor(weaponObj: any, settings: EnhanceSettings) {
        this.obj = weaponObj;
        this.skillLevel = weaponObj.skill_level;
        this.requiredPoints = 0;
        this.enhancePoints = 0;
        this.type = this.getWeaponType();
        this.targetLevel = this.getTargetLevel(settings);
    }

    public getWeaponType(): WeaponType {
        switch (this.obj.rare) {
            case "R":
                return this.obj.name === "Arcane Grail" ? WeaponType.R_GRAIL: WeaponType.R;
            case "SR":
                return this.obj.name === "False Grail Yaldabaoth" ? WeaponType.SR_GRAIL: WeaponType.SR;
            case "SSR":
                return WeaponType.SSR;
        }
    }

    private getTargetLevel(settings: EnhanceSettings): number {
        return settings.getSetting(this.type).targetLevel;
    }

    public isTarget(): boolean {
        return this.skillLevel < this.targetLevel;
    }
    
    public calculateEnhancePoints(): void {
        switch (this.type) {
            case WeaponType.R:
                this.enhancePoints = 10 * this.skillLevel;
                break;
            case WeaponType.SR:
                this.enhancePoints = 35 * this.skillLevel;
                break;
            case WeaponType.SSR:
                this.enhancePoints = 250 + 100 * (this.skillLevel - 1);
                break;
            case WeaponType.R_GRAIL:
                this.enhancePoints = 20 * this.skillLevel;
                break;
            case WeaponType.SR_GRAIL:
                this.enhancePoints = 50 * this.skillLevel;
                break;
        }
    }

    public calculateRequiredPoints(): void {
        let target = this.targetLevel;
        let level = this.skillLevel;
        switch (this.type) {
            case WeaponType.R:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 / 2 + this.obj.skill.next_exp;
                break;
            case WeaponType.SR:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 + this.obj.skill.next_exp;
                break;
            case WeaponType.SSR:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 10 + this.obj.skill.next_exp;
                break;
            case WeaponType.R_GRAIL:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 / 2 + this.obj.skill.next_exp;
                break;
            case WeaponType.SR_GRAIL:
                this.requiredPoints = (target * (target - 1) - level * (level + 1))* 5 + this.obj.skill.next_exp;
                break;
        }
    }
}