// ==UserScript==
// @name         KH Bot
// @namespace    http://tampermonkey.net/
// @version      04.07.2020
// @description  Auto Enhance weapons, claim gifts, do gemcha, ...
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @require      file:///{Your Path}\Combined\src\Helper.js
// @require      file:///{Your Path}\Combined\src\Settings.js
// @require      file:///{Your Path}\Combined\src\AutoSellChecker.js
// @require      file:///{Your Path}\Combined\src\Gemcha.js
// @require      file:///{Your Path}\Combined\src\GiftBox.js
// @require      file:///{Your Path}\Combined\src\Weapon.js
// @require      file:///{Your Path}\Combined\src\Tracker.js
// @require      file:///{Your Path}\Combined\src\Enhance.js
// @require      file:///{Your Path}\Combined\src\Shop.js
// @require      file:///{Your Path}\Combined\src\HaremScenes.js
// @require      file:///{Your Path}\Combined\src\Missions.js
// @require      file:///{Your Path}\Combined\src\WeaponBalancer.js
// @require      file:///{Your Path}\Combined\src\KhBot.js
// @grant        none
// @run-at       document-end
// ==/UserScript==

const botSettings = {
    enhanceWeapons: true,
    doGemcha: true,
    doShop: true,
    watchHarem: true,
    claimMissionRewards: true,
    minWeaponSlots: 20,
    minSummonSlots: 20
}

const claimSettings = {
    batchSize: 20, // Number of presents claimed at once
    claimSSRs: true, // If Bot claims SSRs in GiftBox
}

const enhanceSettings = {
    useSR4Settings: true, // Set which of the two settings below you want to use
    useBonus: false, // Use more +1 Rs
    onlyEnhanceToMax: true, // Don't level fodder halfway
    useEnhanceMats: false, // Use enhance mats on the highest level locked SSR
    batchSize: 5, // Number of enhance mats used at once (not calculating it atm, overhead might lead to loss in materials)
    
    // Presets    
    sr3Settings: {
        r: { target_level: 1, min_remaining: 0, tier: 1, allowed_excess: 0 },
        rGrail: { target_level: 1, min_remaining: 0, tier: 1, allowed_excess: 0 },
        sr: { target_level: 3, min_remaining: 10, tier: 2, allowed_excess: 0 },
        srGrail: { target_level: 5, min_remaining: 0, tier: 3, allowed_excess: 5 },
        ssr: { target_level: 5, min_remaining: 1000, tier: 3, allowed_excess: 10 }
    },

    sr4Settings: {
        r: { target_level: 1, min_remaining: 0, tier: 1, allowed_excess: 0 },
        rGrail: { target_level: 3, min_remaining: 0, tier: 2, allowed_excess: 5 },
        sr: { target_level: 4, min_remaining: 10, tier: 2, allowed_excess: 0 },
        srGrail: { target_level: 5, min_remaining: 0, tier: 2, allowed_excess: 0 },
        ssr: { target_level: 5, min_remaining: 1000, tier: 3, allowed_excess: 0 }
    }
}

const shopPresets = {
    dragonBones: { name: "Dragon Bone" , shop: "Material", min_rem: 500},
    lithographs: { name: "Lithograph", shop: "Material", min_rem: 500},
    runes: { name: "Rune", shop: "Material", min_rem: 500},
    crystal_1: { name: "Holy Jewel of Light" , shop: "Material", min_rem: 200 },
    crystal_2: { name: "Holy Crystal of Light" , shop: "Material", min_rem: 200 },
    crystal_3: { name: "Holy Star of Light" , shop: "Material", min_rem: 200 },
    regalia: { name: "Regalia" , shop: "Material", max_cost: 5},
    oris: { name: "Orichalcon" , shop: "Material", min_rem: 200},
    fangs: { name: "Fang", shop: "Material", min_rem: 100},
    he: { name: "Half Elixir", shop: "Eidolon Orb Exchange", min_rem: 15000 },
    eye: { name: "Draconic Eye", shop: "Draconic Eye" },
    acc_ring1: { name: "Inferno Ring", shop: "Acce P Exchange", precise_name: true },
    acc_ring2: { name: "Deluge Ring", shop: "Acce P Exchange", precise_name: true },
    acc_ring3: { name: "Hurricane Ring", shop: "Acce P Exchange", precise_name: true },
    acc_ring4: { name: "Plasmic Ring", shop: "Acce P Exchange", precise_name: true },
    acc_ring5: { name: "Divine Ring", shop: "Acce P Exchange", precise_name: true },
    acc_ring6: { name: "Demonic Ring", shop: "Acce P Exchange", precise_name: true },
}

const shopSettings = {
    doShop: true,
    items: [shopPresets.dragonBones, shopPresets.lithographs, shopPresets.runes, shopPresets.regalia, shopPresets.oris,
        shopPresets.crystal_1, shopPresets.crystal_2, shopPresets.crystal_3, shopPresets.fangs, shopPresets.eye]
}

start();