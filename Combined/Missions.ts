class Mission {
    private missionsApi: any;
    private unionsApi: any;
    private dailyMissions: any[];
    private weeklyMissions: any[];
    private eventMissions: any[];

    constructor() {
        this.missionsApi = kh.createInstance("apiAMissions");
        this.unionsApi = kh.createInstance("apiAUnions");
    }

    public async claim(): Promise<void> {
        console.log("Check missions...");
        await this.getMissions();
        await this.doDonationMission();

        // TODO: Use Break Limit on an Eidolon/Weapon

        // Dailies / Onetime
        let dailyPromise = this.getReward("daily", this.dailyMissions);
        let eventPromise = this.getReward("event", this.eventMissions);        
        await dailyPromise;
        await eventPromise;
        
        // Weekly
        this.weeklyMissions = (await this.missionsApi.getWeekly()).body.missions;
        await this.getReward("weekly", this.weeklyMissions);
    }
    
    private async getMissions(): Promise<void> {
        let dailyPromise = this.missionsApi.getDaily();
        let eventPromise = this.missionsApi.getEvent();
        let weeklyPromise = this.missionsApi.getWeekly();
        this.dailyMissions = (await dailyPromise).body.missions;
        this.eventMissions = (await eventPromise).body.missions;
        this.weeklyMissions = (await weeklyPromise).body.missions;
    }
    
    private async doDonationMission(): Promise<void> {
        let mission = this.dailyMissions.find(x => x.title === "Donate to a Union"); // Fix
        if (mission !== undefined && !mission.clear) {
            await this.unionsApi.donation(1);
            this.dailyMissions = (await this.missionsApi.getDaily()).body.missions;
        }
    }

    private async getReward(type: String, missions: any[]): Promise<void> {
        let promises = [];
        missions.forEach(mission => {
            if (mission.clear) {
                // TODO: Update trackers
                console.log(`Claim ${type}: ${mission.description}`);
                promises.push(this.missionsApi.receiveMissionReward(type, mission.a_mission_id));
            }
        });
        await Promise.all(promises);        
    }

    public async log(): Promise<void> {
        await this.getMissions();
        console.log(`Missions left - Daily: ${this.dailyMissions.length}, Weekly: ${this.weeklyMissions.length}, Event: ${this.eventMissions.length}`)
    }
}