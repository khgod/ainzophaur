function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
async function start() {
    console.log("Waiting for page.");
    while (!(kh && kh.createInstance && kh.Api && kh.Api.AWeapons)) {
        console.log("sleep");
        await sleep(1000);
    }
    try {
        console.log("Commence bot invasion!");
        let bot = new KhBot();
        await bot.start();
    }
    catch (exception) {
        console.log(exception.message);
        await sleep(1000);
        start();
    }
}
function isEnhanceWeapon(name) {
    return name.includes("Seraph ") || name.includes("Cherub ");
}
function isEnhanceSummon(name) {
    return name.includes(" Spirit");
}
