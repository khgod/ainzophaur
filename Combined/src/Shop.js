class Shop {
    constructor() {
        this.shopApi = kh.createInstance("apiShop");
    }
    async exchange() {
        console.log("Exchange shop items...");
        let promises = [];
        for (let c = 4; c < 9; c++) { // Loop all categories
            promises.push(this.exchangeCategory(c));
        }
        await Promise.all(promises);
        console.log("Done shopping.");
    }
    async exchangeCategory(categoryId) {
        let promises = [];
        let shops = (await this.shopApi.getShop(categoryId)).body.catalogs;
        for (let s = 0; s < shops.length; s++) { // Loop all stores
            promises.push(this.exchangeShop(shops[s]));
        }
        await Promise.all(promises);
    }
    async exchangeShop(shop) {
        let products = shop.products.filter(x => x.can_buy);
        for (let i = 0; i < products.length; i++) {
            let item = new ShopItem(products[i]);
            if (!item.isIncluded(shop.name)) { // Match item
                continue;
            }
            let amount = item.calculateAmount(); // Get amount you can exchange
            if (amount > 0) {
                await this.shopApi.buyProduct(item.product.product_id, amount); // Exchange
                console.log("Exchanged " + amount + " " + item.product.name);
            }
        }
    }
}
class ShopItem {
    constructor(product) {
        this.product = product;
    }
    isIncluded(shopName) {
        let include = false;
        shopSettings.items.forEach(item => {
            if (item.name
                && ((!(item.precise_name) && this.product.name.includes(item.name)) || this.product.name === item.name) // Check if included
                && (!item.shop || item.shop === shopName) // Check if shop matches
                && (!item.max_cost || this.product.materials[0].required_amount <= item.max_cost)) { // Check if max cost matches
                this.info = item;
                include = true;
            }
        });
        return include;
    }
    calculateAmount() {
        let max = this.product.stock_info.amount != "" ? this.product.stock_info.amount : Infinity;
        let minRemaining = this.info.min_rem ? this.info.min_rem : 0; // Get min remaining property
        this.product.materials.forEach(function (mat) {
            let can_afford = Math.floor((mat.current_amount - minRemaining) / mat.required_amount); // Calculate maximum amount you can afford
            if (max > can_afford) {
                max = can_afford;
            }
        });
        return max;
    }
}
