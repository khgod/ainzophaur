class AutoSellChecker {
    constructor() {
        this.playerApi = kh.createInstance("apiAPlayers");
    }
    async prepare() {
        let weaponSettingsPromise = this.playerApi.getAutoSpendConfig("weapon");
        let summonSettingsPromise = this.playerApi.getAutoSpendConfig("summon");
        this.weaponSettings = new AutoSellSetting((await weaponSettingsPromise).body.auto_spend_config);
        this.summonSettings = new AutoSellSetting((await summonSettingsPromise).body.auto_spend_config);
    }
    sellWeapon(weapon) {
        if (isEnhanceWeapon(weapon.name)) {
            return this.weaponSettings.checkEnhance(weapon);
        }
        else {
            return this.weaponSettings.checkNormal(weapon);
        }
    }
    sellSummon(summon) {
        if (isEnhanceSummon(summon.name)) {
            return this.summonSettings.checkEnhance(summon);
        }
        else {
            return this.summonSettings.checkNormal(summon);
        }
    }
}
class AutoSellSetting {
    constructor(obj) {
        this.enabled = obj.enabled;
        this.sellBonus = obj.includes_has_bonus;
        this.enhanceMatRarities = obj.buildup_rarities;
        this.normalRarities = obj.rarities;
    }
    checkEnhance(item) {
        if (!this.enabled || !this.enhanceMatRarities.includes(item.rare)) {
            return false;
        }
        return item.bonus === 0 || this.sellBonus;
    }
    checkNormal(item) {
        if (!this.enabled || !this.normalRarities.includes(item.rare)) {
            return false;
        }
        return item.bonus === 0 || this.sellBonus;
    }
}
