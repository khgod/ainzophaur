class EnhanceTracker {
    constructor(settings) {
        this.typeTracker = [];
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.R)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SR)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SSR)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.R_GRAIL)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SR_GRAIL)));
    }
    getTracker(weaponType) {
        return this.typeTracker.find(x => x.weaponType == weaponType);
    }
    add(weaponType) {
        this.getTracker(weaponType).add();
    }
    remove(weaponType) {
        this.getTracker(weaponType).remove();
    }
    canRemove(weaponType) {
        return this.getTracker(weaponType).canRemove();
    }
    logWithMinimum(name) {
        console.log(`${name}: r: ${this.getTracker(WeaponType.R).current}(${this.getTracker(WeaponType.R).minimum}), ` +
            `rg: ${this.getTracker(WeaponType.R_GRAIL).current}(${this.getTracker(WeaponType.R_GRAIL).minimum}), ` +
            `sr: ${this.getTracker(WeaponType.SR).current}(${this.getTracker(WeaponType.SR).minimum}), ` +
            `srg: ${this.getTracker(WeaponType.SR_GRAIL).current}(${this.getTracker(WeaponType.SR_GRAIL).minimum}), ` +
            `ssr: ${this.getTracker(WeaponType.SSR).current}(${this.getTracker(WeaponType.SSR).minimum})`);
    }
    log(name) {
        console.log(`${name}: r: ${this.getTracker(WeaponType.R).current}, ` +
            `rg: ${this.getTracker(WeaponType.R_GRAIL).current}, ` +
            `sr: ${this.getTracker(WeaponType.SR).current}, ` +
            `srg: ${this.getTracker(WeaponType.SR_GRAIL).current}, ` +
            `ssr: ${this.getTracker(WeaponType.SSR).current}`);
    }
}
class TypeTracker {
    constructor(setting) {
        this.weaponType = setting.weaponType;
        this.minimum = setting.minRemaining;
        this.current = 0;
    }
    add() {
        this.current++;
    }
    remove() {
        this.current--;
    }
    canRemove() {
        return this.current > this.minimum;
    }
}
class SpaceTracker {
    constructor(me, weaponCount, summonCount) {
        this.weaponLimit = me.max_weapon_num;
        this.summonLimit = me.max_summon_num;
        this.weaponCount = weaponCount;
        this.summonCount = summonCount;
        this.minWeaponSlots = botSettings.minWeaponSlots;
        this.minSummonSlots = botSettings.minSummonSlots;
    }
    removeWeapon(amount = 1) {
        this.weaponCount -= amount;
    }
    addWeapon(amount = 1) {
        this.weaponCount += amount;
    }
    removeSummon(amount = 1) {
        this.summonCount -= amount;
    }
    addSummon(amount = 1) {
        this.summonCount += amount;
    }
    freeWeaponSlots() {
        return this.weaponLimit - this.weaponCount - this.minWeaponSlots;
    }
    freeSummonSlots() {
        return this.summonLimit - this.summonCount - this.minSummonSlots;
    }
    log() {
        console.log(`${this.weaponCount} weapons, ${this.summonCount} summons.`);
    }
}
