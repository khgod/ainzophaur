class FodderBalancer {
    constructor(spaceTracker, enhanceManager, gemcha, giftBox) {
        this.spaceTracker = spaceTracker;
        this.enhanceManager = enhanceManager;
        this.gemcha = gemcha;
        this.giftBox = giftBox;
    }
    // async getFodder() {
    //     let fodderPoints = this.enhanceManager.getFodderPoints();
    //     let targetPoints = this.enhanceManager.getTargetPoints();
    //     console.log(fodderPoints);
    //     console.log(targetPoints);
    //     let t1Balance = targetPoints[1] - fodderPoints[0];
    //     let t2Balance = targetPoints[2] - fodderPoints[1];
    //     let t1PresentsEnhancePoints = this.giftBox.getEnhancePoints(1);
    //     let t1PresentsRequiredPoints = this.giftBox.getRequiredPoints(1);
    // }
    async balanceTier(tier) {
        console.log("Balance tier " + tier);
        let fodderPoints = this.enhanceManager.getFodderPoints();
        let targetPoints = this.enhanceManager.getTargetPoints();
        let balance = targetPoints[tier] - fodderPoints[tier - 1];
        console.log(balance);
        if (balance > 0) { // get fodder (tier)
            this.giftBox.getWeaponFodder(tier, balance, this.spaceTracker);
        }
        else { // get targets (tier + 1)
            this.giftBox.getWeaponTargets(tier + 1, balance, this.spaceTracker);
        }
        return await this.enhanceManager.enhanceTargets(this.spaceTracker);
    }
}
