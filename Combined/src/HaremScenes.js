class Harem {
    constructor() {
        this.questApi = kh.createInstance("apiAQuests");
        this.battleApi = kh.createInstance("apiABattles");
    }
    async doHaremScenes() {
        console.log("Check Harem Scenes.");
        this.unconfirmedScenes = (await this.questApi.getUnconfirmedHaremEpisodes(1, 100000)).body.data;
        while (this.unconfirmedScenes.length > 0) {
            await this.doQuest();
            this.unconfirmedScenes = (await this.questApi.getUnconfirmedHaremEpisodes(1, 100000)).body.data;
        }
        console.log("All Harem Scenes done.");
    }
    async doQuest() {
        let scene = this.unconfirmedScenes[0];
        console.log(`Watching ${scene.harem_episode_info.heroines[0].name} - ${scene.title}`);
        await this.questApi.startQuest(scene.quest_id, scene.type); // Start quest
        while ((await this.questApi.getNextState(scene.a_quest_id, scene.type)).body.next_info.next_kind !== "episode_result") { } // Get next stage until we get episode result
        await this.battleApi.getBattleResult(scene.a_quest_id, scene.type);
    }
}
