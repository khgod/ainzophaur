class EnhanceSettings {
    constructor(settings) {
        this.rarities = [];
        this.rarities.push(new EnhanceSettingsTuple(settings.r, WeaponType.R));
        this.rarities.push(new EnhanceSettingsTuple(settings.sr, WeaponType.SR));
        this.rarities.push(new EnhanceSettingsTuple(settings.ssr, WeaponType.SSR));
        this.rarities.push(new EnhanceSettingsTuple(settings.rGrail, WeaponType.R_GRAIL));
        this.rarities.push(new EnhanceSettingsTuple(settings.srGrail, WeaponType.SR_GRAIL));
        this.useBonus = enhanceSettings.useBonus;
        this.onlyEnhanceToMax = enhanceSettings.onlyEnhanceToMax;
    }
    getSetting(weaponType) {
        return this.rarities.find(x => x.weaponType === weaponType);
    }
}
class EnhanceSettingsTuple {
    constructor(obj, weaponType) {
        this.weaponType = weaponType;
        this.targetLevel = obj.target_level;
        this.minRemaining = obj.min_remaining;
        this.tier = obj.tier;
        this.allowedExcess = obj.allowed_excess;
    }
    getMaxedPoints() {
        switch (this.weaponType) {
            case WeaponType.R:
                return this.targetLevel * 10;
            case WeaponType.SR:
                return this.targetLevel * 35;
            case WeaponType.SSR:
                return this.targetLevel * 100 + 150;
            case WeaponType.R_GRAIL:
                return this.targetLevel * 20;
            case WeaponType.SR_GRAIL:
                return this.targetLevel * 50;
        }
    }
    getMaxRequiredPoints() {
        switch (this.weaponType) {
            case WeaponType.R:
                return this.targetLevel * (this.targetLevel - 1) * 5 / 2;
            case WeaponType.SR:
                return this.targetLevel * (this.targetLevel - 1) * 5;
            case WeaponType.SSR:
                return this.targetLevel * (this.targetLevel - 1) * 10;
            case WeaponType.R_GRAIL:
                return this.targetLevel * (this.targetLevel - 1) * 5 / 2;
            case WeaponType.SR_GRAIL:
                return this.targetLevel * (this.targetLevel - 1) * 5;
        }
    }
    getMinRemaining() {
        return this.getMaxedPoints() * this.minRemaining;
    }
}
