class EnhanceManager {
    constructor() {
        if (enhanceSettings.useSR4Settings) {
            this.settings = new EnhanceSettings(enhanceSettings.sr4Settings);
        }
        else {
            this.settings = new EnhanceSettings(enhanceSettings.sr3Settings);
        }
        this.weaponApi = kh.createInstance("apiAWeapons");
        this.fodder = [];
        this.targets = [];
        this.weaponCatalog = [];
        this.enhanceWeapons = [];
        this.fodderTracker = new EnhanceTracker(this.settings);
        this.targetTracker = new EnhanceTracker(this.settings);
    }
    async prepareWeapons() {
        let availableWeapons = (await this.weaponApi.getList(1, 1, 1000)).body.data.filter(x => !x.is_locked && x.rare !== "N"); // Get all available weapons
        availableWeapons.forEach(weaponObj => {
            this.assignWeapon(weaponObj); // Create containers and split into fodder and targets
        });
        this.sortLists();
    }
    assignWeapon(weaponObj) {
        if (this.weaponCatalog.includes(weaponObj.a_weapon_id)) {
            return;
        }
        this.weaponCatalog.push(weaponObj.a_weapon_id);
        let weapon = new Weapon(weaponObj, this.settings);
        if (weapon.skillLevel == 0) {
            this.enhanceWeapons.push(weapon);
            return;
        }
        if (weapon.isTarget()) {
            weapon.calculateRequiredPoints();
            this.targetTracker.add(weapon.type);
            this.targets.push(weapon);
        }
        else {
            if (!weapon.obj.is_equipped && (this.settings.useBonus || weapon.obj.bonus === 0)) {
                weapon.calculateEnhancePoints();
                this.fodderTracker.add(weapon.type);
                this.fodder.push(weapon);
            }
        }
    }
    sortLists() {
        this.sortTargets();
        this.sortFodder();
    }
    sortTargets() {
        this.targets.sort((a, b) => b.requiredPoints - a.requiredPoints);
    }
    sortFodder() {
        this.fodder.sort((a, b) => a.enhancePoints - b.enhancePoints);
    }
    async enhanceTargets(spaceTracker) {
        console.log("Start enhancing.");
        let success = false; // To keep track if any weapons were enhanced
        await this.prepareWeapons();
        this.logTracker();
        for (let i = this.targets.length - 1; i >= 0; i--) {
            const target = this.targets[i];
            let requiredPoints = target.requiredPoints;
            let allowedExcess = this.settings.getSetting(target.type).allowedExcess;
            let selectedIndizes = [];
            for (let j = this.fodder.length - 1; j >= 0; j--) {
                const fodder = this.fodder[j];
                if (!this.fodderTracker.canRemove(fodder.type) || fodder.enhancePoints > requiredPoints + allowedExcess) { // Check if usable
                    continue;
                }
                selectedIndizes.push(j);
                this.fodderTracker.remove(fodder.type); // Remove fodder from tracker
                spaceTracker.removeWeapon();
                requiredPoints -= fodder.enhancePoints; // Update points counter
                if (requiredPoints <= 0) { // Break if enough fodder was gathered
                    break;
                }
            }
            if (selectedIndizes.length === 0 || this.settings.onlyEnhanceToMax && requiredPoints > 0) { // Check if target can be maxxed
                selectedIndizes.forEach(idx => {
                    this.fodderTracker.add(this.fodder[idx].type); // Undo tracker changes, as we won't be using the fodder
                    spaceTracker.addWeapon();
                });
                continue;
            }
            let selectedFodder = []; // Map to fodder + remove fodder and target from lists
            selectedIndizes.forEach(idx => {
                let fodder = this.fodder.splice(idx, 1)[0];
                selectedFodder.push(fodder);
            });
            this.targets.splice(i, 1);
            this.targetTracker.remove(target.type);
            console.log(`Enhancing ${target.type} with ${selectedFodder.map(x => x.type + x.skillLevel).join("+")}`);
            let result = (await this.weaponApi.enhance(target.obj.a_weapon_id, selectedFodder.map(x => x.obj.a_weapon_id))).body;
            success = true;
            this.assignWeapon(result.weapon); // Create a new weapon 
            this.sortFodder(); // In case new weapon got assigned as fodder, sort fodder list.
            this.logTracker();
        }
        console.log("Done enhancing.");
        return success;
    }
    getTargetPoints() {
        let points = [0, 0, 0];
        this.targets.forEach(weapon => {
            points[this.settings.getSetting(weapon.type).tier - 1] += weapon.requiredPoints;
        });
        return points;
    }
    getFodderPoints() {
        let points = [0, 0, 0];
        this.fodder.forEach(weapon => {
            points[this.settings.getSetting(weapon.type).tier - 1] += weapon.enhancePoints;
        });
        this.settings.rarities.forEach(setting => {
            points[setting.tier - 1] -= setting.getMinRemaining();
        });
        // Add potential points from leveled tier-1 targets
        this.targets.forEach(weapon => {
            let setting = this.settings.getSetting(weapon.type);
            let tier = setting.tier - 1;
            points[tier] += setting.getMaxedPoints();
        });
        return points;
    }
    logTracker() {
        this.targetTracker.log("Targets");
        this.fodderTracker.logWithMinimum("Fodder");
    }
}
