// Filter presets
const presentFilter = {
    r: x => { return x.kind === "weapon" && !isEnhanceWeapon(x.present_name) && x.weapon_info.rare === "R" && x.present_name !== "Arcane Grail"; },
    rGrails: x => { return x.present_name === "Arcane Grail"; },
    sr: x => { return x.kind === "weapon" && (x.weapon_info.rare === "SR" && !isEnhanceWeapon(x.present_name) && x.present_name !== "False Grail Yaldabaoth"); },
    srGrails: x => { return x.kind === "weapon" && x.present_name === "False Grail Yaldabaoth"; },
    ssr: x => { return x.kind === "weapon" && x.weapon_info.rare === "SSR" && claimSettings.claimSSRs; },
    enhanceWeapon: x => { return x.kind === "weapon" && isEnhanceWeapon(x.present_name); },
    summon: x => { return x.kind === "summon"; },
    accessories: x => { return x.kind === "accessory"; },
    other: x => { return x.kind !== "summon" && (x.kind !== "weapon") && x.kind !== "accessory"; },
    n: x => { return x.kind === "weapon" && x.weapon_info.rare === "N"; }
};
// TODO N weapons (x.weapon_info.rare === "N")
class GiftBox {
    // TODO: Handle +1 weapons
    constructor() {
        this.batchSize = claimSettings.batchSize;
        this.presentsApi = kh.createInstance("apiAPresents");
        this.weaponsApi = kh.createInstance("apiAWeapons");
        this.summonsApi = kh.createInstance("apiASummons");
        this.playersApi = kh.createInstance("apiAPlayers");
    }
    async prepare(enhanceSettings) {
        let mePromise = this.playersApi.getMeNumeric();
        let weaponCountPromise = this.weaponsApi.getList("book_weapon", 1, 100000);
        let summonCountPromise = this.summonsApi.getList("book_summon", 1, 100000);
        let permanentPromise = this.presentsApi.getPresentList("normal", 1, 10000);
        let expiringPromise = this.presentsApi.getPresentList("timelimit", 1, 10000);
        let me = (await mePromise).body;
        let weaponCount = (await weaponCountPromise).body.exists_record_count;
        let summonCount = (await summonCountPromise).body.exists_record_count;
        let tracker = new SpaceTracker(me, weaponCount, summonCount);
        let availablePresents = (await expiringPromise).body.data;
        Array.prototype.push.apply(availablePresents, (await permanentPromise).body.data);
        this.splitPresents(availablePresents, enhanceSettings);
        return tracker;
    }
    splitPresents(availablePresents, enhanceSettings) {
        this.weapons = [];
        this.weapons.push(new WeaponPresent(WeaponType.R, availablePresents.filter(presentFilter.r), enhanceSettings));
        this.weapons.push(new WeaponPresent(WeaponType.SR, availablePresents.filter(presentFilter.sr), enhanceSettings));
        this.weapons.push(new WeaponPresent(WeaponType.R_GRAIL, availablePresents.filter(presentFilter.rGrails), enhanceSettings));
        this.weapons.push(new WeaponPresent(WeaponType.SR_GRAIL, availablePresents.filter(presentFilter.srGrails), enhanceSettings));
        this.weapons.push(new WeaponPresent(WeaponType.SSR, availablePresents.filter(presentFilter.ssr), enhanceSettings));
        this.enhanceWeapons = availablePresents.filter(presentFilter.enhanceWeapon);
        this.summons = availablePresents.filter(presentFilter.summon);
        this.accessories = availablePresents.filter(presentFilter.accessories);
        this.other = availablePresents.filter(presentFilter.other);
        this.nWeapons = availablePresents.filter(presentFilter.n);
    }
    async getWeaponTargets(tier, pointsNeeded, spaceTracker) {
        pointsNeeded = Math.abs(pointsNeeded);
        console.log("Get targets of tier " + tier);
        let freeSlots = spaceTracker.freeWeaponSlots();
        for (let i = 0; i < this.weapons.length; i++) {
            const weaponPresent = this.weapons[i];
            let points = weaponPresent.getRequiredPointsForTier(tier);
            if (points > 0) {
                let min = Math.min(points, pointsNeeded);
                let amount = Math.min(freeSlots, Math.ceil(min / weaponPresent.requiredPointsPerWeapon));
                console.log(`Claim ${amount} ${weaponPresent.weaponType} presents as target!`);
                await this.claim(weaponPresent.weapons, amount);
                pointsNeeded -= amount * weaponPresent.requiredPointsPerWeapon;
                spaceTracker.addWeapon(amount);
            }
            if (pointsNeeded <= 0) {
                break;
            }
        }
        console.log("Done getting targets.");
    }
    async getWeaponFodder(tier, pointsNeeded, spaceTracker) {
        console.log("Try to get fodder of tier " + tier);
        let freeSlots = spaceTracker.freeWeaponSlots();
        for (let i = 0; i < this.weapons.length; i++) {
            const weaponPresent = this.weapons[i];
            let points = weaponPresent.getEnhancePointsForTier(tier);
            if (points > 0) {
                let min = Math.min(points, pointsNeeded);
                let amount = Math.min(freeSlots, Math.ceil(min / weaponPresent.enhancePointsPerWeapon));
                await this.claim(weaponPresent.weapons, amount);
                console.log(`Claim ${amount} ${weaponPresent.weaponType} presents as fodder!`);
                pointsNeeded -= amount * weaponPresent.enhancePointsPerWeapon;
                spaceTracker.addWeapon(amount);
            }
            if (pointsNeeded <= 0) {
                break;
            }
        }
        console.log("Done getting fodder.");
    }
    async claimOther(spaceTracker, autoSellChecker) {
        console.log("Claim other items from Gifts.");
        await this.claim(this.other, this.other.length);
        let amount = Math.min(spaceTracker.freeWeaponSlots(), this.nWeapons.length);
        if (amount == 0) {
            return;
        }
        console.log(`Get ${amount} n weapons.`);
        let presents = [];
        for (let i = 0; i < amount; i++) {
            const present = this.nWeapons.shift();
            present.weapon_info.name = present.present_name;
            if (!autoSellChecker.sellWeapon(present.weapon_info)) {
                spaceTracker.addWeapon();
            }
            presents.push(present);
        }
        await this.claim(presents, amount);
    }
    async claimSummons(spaceTracker, autoSellChecker) {
        console.log("Claim summons from Gifts.");
        let amount = Math.min(spaceTracker.freeSummonSlots(), this.summons.length);
        let presents = [];
        for (let i = 0; i < amount; i++) {
            const present = this.summons.shift();
            present.summon_info.name = present.present_name;
            if (!autoSellChecker.sellSummon(present.summon_info)) {
                spaceTracker.addSummon();
            }
            presents.push(present);
        }
        await this.claim(presents, amount);
    }
    async claimAccessories(spaceTracker, amount) {
        // TODO: Acc stuff
    }
    async claimEnhanceWeapons(spaceTracker, autoSellChecker) {
        if (this.enhanceWeapons.length === 0) {
            return false;
        }
        let amount = Math.min(spaceTracker.freeWeaponSlots(), this.enhanceWeapons.length);
        console.log(`Get ${amount} enhance mats.`);
        let presents = [];
        for (let i = 0; i < amount; i++) {
            const present = this.enhanceWeapons.pop();
            present.weapon_info.name = present.present_name;
            if (!autoSellChecker.sellWeapon(present.weapon_info)) {
                spaceTracker.addWeapon();
            }
            presents.push(present);
        }
        await this.claim(presents);
        return true;
    }
    async claim(items, amount = items.length) {
        while (amount > 0) {
            let min = Math.min(amount, this.batchSize);
            let currentItems = [];
            for (let i = 0; i < min; i++) {
                currentItems.push(items.shift());
            }
            amount -= min;
            await this.tradeChunk(currentItems);
        }
    }
    async tradeChunk(items) {
        console.log("Claim batch.");
        let promises = [];
        for (let i = 0; i < items.length; i++) {
            promises.push(this.presentsApi.receivePresent(items[i].a_present_id));
        }
        await Promise.all(promises);
    }
}
class WeaponPresent {
    constructor(weaponType, weapons, enhanceSettings) {
        this.weaponType = weaponType;
        this.weapons = weapons;
        let setting = enhanceSettings.getSetting(weaponType);
        this.requiredPointsPerWeapon = setting.getMaxRequiredPoints();
        this.enhancePointsPerWeapon = setting.getMaxedPoints();
        this.tier = setting.tier;
    }
    getEnhancePointsForTier(tier) {
        return tier === this.tier ? this.calculateEnhancePoints() : 0;
    }
    getRequiredPointsForTier(tier) {
        return tier === this.tier ? this.calculateRequiredPoints() : 0;
    }
    calculateEnhancePoints() {
        return this.weapons.length * this.enhancePointsPerWeapon;
    }
    calculateRequiredPoints() {
        return this.weapons.length * this.requiredPointsPerWeapon;
    }
}
