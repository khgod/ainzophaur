class WeaponBalancer {
    constructor(enhanceManager, gemcha, spaceTracker, autoSellChecker, giftBox) {
        this.enhanceManager = enhanceManager;
        this.gemcha = gemcha;
        this.spaceTracker = spaceTracker;
        this.autoSellChecker = autoSellChecker;
        this.giftBox = giftBox;
    }
    async startEnhancing() {
        console.log("Do enhance magic!");
        this.enhanceManager.logTracker();
        let canEnhance = true;
        while (canEnhance && this.gemcha.availablePulls > 0) {
            canEnhance = await this.balanceTier(1);
        }
        canEnhance = true;
        while (canEnhance) {
            let t2 = await this.balanceTier(2);
            let t1 = await this.balanceTier(1);
            canEnhance = t1 || t2;
            // If no SR fodder left, but gemcha pulls remaining, do gemcha and continue balancing
            if (!canEnhance && this.gemcha.availablePulls > 0 && this.spaceTracker.freeWeaponSlots() >= 11 && this.spaceTracker.freeSummonSlots() >= 10) {
                await this.gemcha.pull(this.spaceTracker, this.autoSellChecker, this.spaceTracker.freeWeaponSlots() - 1, this.spaceTracker.freeSummonSlots());
                canEnhance = await this.balanceTier(1);
            }
        }
    }
    async balanceTier(tier) {
        let balance = this.getBalance(tier);
        if (balance > 0) { // get fodder (tier)
            if (tier === 1 && this.gemcha.availablePulls > 0) { // Check gemcha
                await this.gemcha.pull(this.spaceTracker, this.autoSellChecker, this.spaceTracker.freeWeaponSlots() - 1, this.spaceTracker.freeSummonSlots());
                // Balance again after gemcha, no new function call to avoid potentially getting stuck in an infinite loop
                let balance = this.getBalance(tier);
                if (balance > 0) {
                    this.giftBox.getWeaponTargets(tier + 1, balance, this.spaceTracker);
                }
                else {
                    this.giftBox.getWeaponFodder(tier, balance, this.spaceTracker);
                }
            }
            else {
                this.giftBox.getWeaponFodder(tier, balance, this.spaceTracker);
            }
        }
        else { // get targets (tier + 1)
            this.giftBox.getWeaponTargets(tier + 1, balance, this.spaceTracker);
        }
        return await this.enhanceManager.enhanceTargets(this.spaceTracker);
    }
    getBalance(tier) {
        let fodderPoints = this.enhanceManager.getFodderPoints();
        let targetPoints = this.enhanceManager.getTargetPoints();
        return targetPoints[tier] - fodderPoints[tier - 1];
    }
    async useEnhanceMats(claim) {
        await this.enhanceManager.prepareWeapons();
        while (true) {
            let availableWeapons = (await this.enhanceManager.weaponApi.getList(1, 1, 1000)).body.data.filter(x => x.is_locked && x.level < x.max_level);
            if (availableWeapons.length === 0) {
                console.log("No more weapons to enhance.");
                return;
            }
            let target = availableWeapons.sort((a, b) => b.level - a.level)[0];
            if (claim && this.enhanceManager.enhanceWeapons.length < enhanceSettings.batchSize) {
                if (await this.giftBox.claimEnhanceWeapons(this.spaceTracker, this.autoSellChecker)) {
                    await this.enhanceManager.prepareWeapons();
                }
            }
            let selectedFodder = [];
            let min = Math.min(enhanceSettings.batchSize, this.enhanceManager.enhanceWeapons.length);
            if (min === 0) {
                console.log("Used up all enhance mats.");
                return;
            }
            for (let i = 0; i < min; i++) {
                selectedFodder.push(this.enhanceManager.enhanceWeapons.pop());
            }
            await this.enhanceManager.weaponApi.enhance(target.a_weapon_id, selectedFodder.map(x => x.obj.a_weapon_id));
            this.spaceTracker.removeWeapon(min);
            console.log(`Used ${min} enhance mats on ${target.name}(${target.level}).`);
        }
    }
}
