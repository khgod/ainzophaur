class AutoSellChecker {
    private playerApi: any;
    private weaponSettings: AutoSellSetting;
    private summonSettings: AutoSellSetting;

    constructor() {
        this.playerApi = kh.createInstance("apiAPlayers");
    }
    
    public async prepare(): Promise<void> {
        let weaponSettingsPromise = this.playerApi.getAutoSpendConfig("weapon");
        let summonSettingsPromise = this.playerApi.getAutoSpendConfig("summon");

        this.weaponSettings = new AutoSellSetting((await weaponSettingsPromise).body.auto_spend_config);
        this.summonSettings = new AutoSellSetting((await summonSettingsPromise).body.auto_spend_config);
    }

    public sellWeapon(weapon: any): boolean {
        if (isEnhanceWeapon(weapon.name)) {
            return this.weaponSettings.checkEnhance(weapon);
        } else {
            return this.weaponSettings.checkNormal(weapon);
        }
    }

    public sellSummon(summon: any): boolean {
        if (isEnhanceSummon(summon.name)) {
            return this.summonSettings.checkEnhance(summon);
        } else {
            return this.summonSettings.checkNormal(summon);
        }
    }
}

class AutoSellSetting {
    private enabled: boolean;
    private sellBonus: boolean;
    private enhanceMatRarities: String[];
    private normalRarities: String[];

    constructor(obj: any) {
        this.enabled = obj.enabled;
        this.sellBonus = obj.includes_has_bonus;
        this.enhanceMatRarities = obj.buildup_rarities;
        this.normalRarities = obj.rarities;
    }

    public checkEnhance(item: any): boolean {
        if (!this.enabled || !this.enhanceMatRarities.includes(item.rare)) {
            return false;
        }

        return item.bonus === 0 || this.sellBonus;
    }

    public checkNormal(item: any): boolean {
        if (!this.enabled || !this.normalRarities.includes(item.rare)) {
            return false;
        }

        return item.bonus === 0 || this.sellBonus;
    }
}