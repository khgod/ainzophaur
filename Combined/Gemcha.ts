class Gemcha {
    private gachaApi: any;
    private gachaCategoriesApi: any;
    private gachaCategoryApi: any;
    private category: number;
    private gachaInfo: any;
    private pulledFreeOne: boolean;

    public availablePulls: number;
    
    constructor() {
        this.gachaApi = kh.createInstance("apiAGacha");
        this.gachaCategoriesApi = kh.createInstance("apiGachaCategories");
        this.gachaCategoryApi = kh.createInstance("apiGachaCategory");        
    }

    public async prepare(): Promise<void> {
        let otherTab = (await this.gachaCategoriesApi.getGachaCategories()).body.tabs.find(x => x.name == "Other");
        if (otherTab == undefined) {
            console.log("No other gacha found, probably no pulls remaining.");
            this.availablePulls = 0;
        } else {
            this.category = otherTab.banners.find(x => x.is_normal_gacha).category_id;
            this.gachaInfo = (await this.gachaCategoryApi.get(this.category)).body;
            this.pulledFreeOne = this.gachaInfo.groups.length > 0 && this.gachaInfo.groups[0].enabled;
            this.availablePulls = this.gachaInfo.rest_times;
        }
    }

    public async pull(spaceTracker: SpaceTracker, autoSellChecker: AutoSellChecker, maxWeapons: number, maxSummons: number): Promise<void> {
        console.log("Start Gemcha.")
        while (maxWeapons >= 10 && maxSummons >= 10 && this.canPull()) {
            let result: any[] = [];
            if (this.pulledFreeOne) {
                result = (await this.gachaApi.playGacha("normal", this.gachaInfo.groups[0].gacha_id)).body.obtained_info;
                this.pulledFreeOne = false;
            } else {
                result = (await this.gachaApi.playGacha("normal", this.gachaInfo.gacha_information[0].gacha_id)).body.obtained_info;
            }
            console.log(result);
            result.forEach(item => {
                if (item.weapon_info) {
                    let weapon = item.weapon_info;
                    if (!autoSellChecker.sellWeapon(weapon)) {                      
                        maxWeapons--;
                        spaceTracker.addWeapon();
                    }
                } else if (item.summon_info) {
                    if (!autoSellChecker.sellSummon(item.summon_info)) {
                        maxSummons--;
                        spaceTracker.addSummon();
                    }
                }              
            });

            this.availablePulls--;
        }
        console.log("Finished Gemcha.")
    }

    private canPull(): boolean {
        return this.availablePulls > 0;
    }
}
