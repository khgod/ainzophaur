class EnhanceTracker {
    private typeTracker: TypeTracker[];

    constructor(settings: EnhanceSettings) {
        this.typeTracker = [];
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.R)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SR)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SSR)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.R_GRAIL)));
        this.typeTracker.push(new TypeTracker(settings.getSetting(WeaponType.SR_GRAIL)));
    }
    
    public getTracker(weaponType: WeaponType): TypeTracker {
        return this.typeTracker.find(x => x.weaponType == weaponType);
    }

    public add(weaponType: WeaponType): void {
        this.getTracker(weaponType).add();
    }
    
    public remove(weaponType: WeaponType): void {
        this.getTracker(weaponType).remove();
    }
       
    public canRemove(weaponType: WeaponType): boolean {
        return this.getTracker(weaponType).canRemove();
    }

    public logWithMinimum(name: String): void {
        console.log(`${name}: r: ${this.getTracker(WeaponType.R).current}(${this.getTracker(WeaponType.R).minimum}), ` + 
            `rg: ${this.getTracker(WeaponType.R_GRAIL).current}(${this.getTracker(WeaponType.R_GRAIL).minimum}), ` + 
            `sr: ${this.getTracker(WeaponType.SR).current}(${this.getTracker(WeaponType.SR).minimum}), ` + 
            `srg: ${this.getTracker(WeaponType.SR_GRAIL).current}(${this.getTracker(WeaponType.SR_GRAIL).minimum}), ` + 
            `ssr: ${this.getTracker(WeaponType.SSR).current}(${this.getTracker(WeaponType.SSR).minimum})`);
    }

    public log(name: String): void {
        console.log(`${name}: r: ${this.getTracker(WeaponType.R).current}, ` + 
            `rg: ${this.getTracker(WeaponType.R_GRAIL).current}, ` + 
            `sr: ${this.getTracker(WeaponType.SR).current}, ` + 
            `srg: ${this.getTracker(WeaponType.SR_GRAIL).current}, ` + 
            `ssr: ${this.getTracker(WeaponType.SSR).current}`);
    }
}

class TypeTracker {
    public minimum: number;
    public current: number;
    public weaponType: WeaponType;
    
    constructor(setting: EnhanceSettingsTuple) {
        this.weaponType = setting.weaponType;
        this.minimum = setting.minRemaining;
        this.current = 0;
    }

    public add(): void {
        this.current++;
    }
    
    public remove(): void {
        this.current--;
    }

    public canRemove(): boolean {
        return this.current > this.minimum;
    }
}

class SpaceTracker {
    private weaponLimit: number;
    private summonLimit: number;
    private weaponCount: number;
    private summonCount: number;
    private minWeaponSlots: number;
    private minSummonSlots: number;

    constructor(me: any, weaponCount: number, summonCount: number) {
        this.weaponLimit = me.max_weapon_num;
        this.summonLimit = me.max_summon_num;
        this.weaponCount = weaponCount;
        this.summonCount = summonCount;
        this.minWeaponSlots = botSettings.minWeaponSlots;
        this.minSummonSlots = botSettings.minSummonSlots;
    }

    public removeWeapon(amount: number = 1): void {
        this.weaponCount -= amount;
    }

    public addWeapon(amount: number = 1): void {
        this.weaponCount += amount;
    }

    public removeSummon(amount: number = 1): void {
        this.summonCount -= amount;
    }

    public addSummon(amount: number = 1): void {
        this.summonCount += amount;
    }

    public freeWeaponSlots(): number {
        return this.weaponLimit - this.weaponCount - this.minWeaponSlots;
    }

    public freeSummonSlots(): number {
        return this.summonLimit - this.summonCount - this.minSummonSlots;
    }

    public log(): void {
        console.log(`${this.weaponCount} weapons, ${this.summonCount} summons.`)
    }
}