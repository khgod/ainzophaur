// ==UserScript==
// @name         KHAutoQuest
// @namespace    http://tampermonkey.net/
// @version      05.24.2020
// @description  Kamihime auto start quests and battle.
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @require      file:///C:/<PATH_GOES_HERE>/khscripts/KHAutoQuest.js
// @require      file:///C:/<PATH_GOES_HERE>/khscripts/Utility/logger.js
// @require      file:///C:/<PATH_GOES_HERE>/khscripts/KHBattleManager.js
// @grant        none
// @run-at       document-end
// ==/UserScript==


var speedUpAnimationBy = 3.2; // Speed up animation by x times, in game fast is 1.6
var refreshTimerInMinutes = 30; // Refresh the NUTAKU top-level browser after this many minutes
var startDelayInMs = 3000; // Number of milliseconds to wait before starting the quest script automatically


// Quest presets - put all your original quest parameters down here!

var aq4 = { "type": "accessory", "ap": 30 };
var dailySP = { "type": "daily", "use_he": true, "summon": "Vampire Vlad Tepes", "summon_element": "phantom", "party": "H", "ap": 30, "index": 1 };
var SundayLeveling = { "type": "exp_Sunday", "use_he": true, "summon": "Vampire Vlad Tepes", "party": "H", "ap": 25 };
var dailyGemQuest = { "type": "gem_daily", "use_he": false, "element": "fire", "summon": "Sleipnir" };
var dailyExpQuest = { "type": "exp", "use_he": false, "party": "H", "summon": "Sleipnir" };
var gemQuest = { "type": "gem", "party": "H", "doFirst": true };
var eventQuest = { "type": "event", "party": "I", "ap": 30, "use_he": true, "summon": "Sleipnir" };
var standardEventRaid = { "type": "event_raid", "ap": 15, "party": "H", "use_he": true };
var expertEventRaid = { "type": "event_raid", "ap": 25, "party": "K", "use_he": true, "priority": "participants", "max_participants": 19, "min_time": 5, "min_hp": 0, "min_bp": 2 };
var ragnarokEventRaid = { "type": "event_raid", "ap": 35, "party": "K", "use_he": true, "priority": "participants", "max_participants": 19, "min_time": 40, "min_hp": 0, "min_bp": 4 };
var lilimRaid = { "type": "lilim", "use_he": false, "min_bp": 2, "ap": 25, "element": "fire" };

let half_off = false;

let gua_ap_cost = half_off ? 30 : 60;
var fireGua = { "type": "disaster", "raid_element": "fire", "party": "B", "use_he": false, "min_bp": 3, "min_hp": 10, "min_time": 50, "max_participants": 19, "ap": gua_ap_cost };
var lightGua = { "type": "disaster", "raid_element": "light", "element": "dark", "use_he": false, "min_bp": 2, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": gua_ap_cost };
var darkGua = { "type": "disaster", "raid_element": "dark", "party": "K", "use_he": false, "min_bp": 4, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": gua_ap_cost };
var windGua = { "type": "disaster", "raid_element": "wind", "party": "B", "use_he": false, "min_bp": 2, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": gua_ap_cost };
var thunderGua = { "type": "disaster", "raid_element": "thunder", "party": "J", "use_he": false, "min_bp": 4, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": gua_ap_cost };
var waterGua = { "type": "disaster", "raid_element": "water", "party": "C", "use_he": false, "min_bp": 3, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": gua_ap_cost };
var phantomGua = { "type": "disaster", "raid_element": "phantom", "party": "B", "use_he": true, "min_bp": 1, "min_hp": 10, "min_time": 40, "max_participants": 24, "ap": 0 };

let rag_ap_cost = half_off ? 25 : 50;
var fireRag = { "type": "disaster", "raid_element": "fire", "party": "B", "use_he": true, "min_bp": 2, "min_hp": 10, "min_time": 45, "max_participants": 19, "ap": rag_ap_cost };
var lightRag = { "type": "disaster", "raid_element": "light", "element": "dark", "use_he": true, "min_bp": 3, "min_hp": 10, "min_time": 50, "max_participants": 19, "ap": rag_ap_cost };
var thunderRag = { "type": "disaster", "raid_element": "thunder", "party": "J", "use_he": true, "min_bp": 4, "min_hp": 10, "min_time": 40, "max_participants": 17, "ap": rag_ap_cost };
var darkRag = { "type": "disaster", "raid_element": "dark", "party": "K", "use_he": true, "min_bp": 4, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": rag_ap_cost };
var windRag = { "type": "disaster", "raid_element": "wind", "element": "fire", "use_he": true, "min_bp": 2, "min_hp": 10, "min_time": 40, "max_participants": 19, "ap": rag_ap_cost };
var waterRag = { "type": "disaster", "raid_element": "water", "party": "C", "use_he": true, "min_bp": 3, "min_hp": 10, "min_time": 45, "max_participants": 15, "ap": rag_ap_cost };

let ult_ap_cost = half_off ? 15 : 30;
let farm_ults = true;
var fireUlti = { "type": "disaster", "raid_element": "fire", "party": "K", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": ult_ap_cost };
var waterUlti = { "type": "disaster", "raid_element": "water", "party": "C", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": ult_ap_cost };
var windUlti = { "type": "disaster", "raid_element": "wind", "element": "fire", "use_he": farm_ults, "min_bp": 5, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": ult_ap_cost };
var thunderUlti = { "type": "disaster", "raid_element": "thunder", "element": "wind", "use_he": farm_ults, "min_bp": 1, "min_hp": 80, "max_participants": 19, "ap": ult_ap_cost };
var lightUlti = { "type": "disaster", "raid_element": "light", "element": "dark", "use_he": farm_ults, "min_bp": 4, "min_hp": 40, "min_time": 20, "max_participants": 19, "ap": ult_ap_cost };
var darkUlti = { "type": "disaster", "raid_element": "dark", "party": "K", "use_he": farm_ults, "min_bp": 4, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": ult_ap_cost };

let exp_ap_cost = half_off ? 7 : 15;
var fireExp = { "type": "disaster", "raid_element": "fire", "element": "water", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": exp_ap_cost };
var waterExp = { "type": "disaster", "raid_element": "water", "element": "water", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": exp_ap_cost };
var windExp = { "type": "disaster", "raid_element": "wind", "element": "water", "use_he": farm_ults, "min_bp": 5, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": exp_ap_cost };
var thunderExp = { "type": "disaster", "raid_element": "thunder", "element": "fire", "use_he": farm_ults, "min_bp": 1, "min_hp": 80, "max_participants": 19, "ap": exp_ap_cost };
var lightExp = { "type": "disaster", "raid_element": "light", "element": "water", "use_he": farm_ults, "min_bp": 4, "min_hp": 40, "min_time": 20, "max_participants": 19, "ap": exp_ap_cost };
var darkExp = { "type": "disaster", "raid_element": "dark", "element": "water", "use_he": farm_ults, "min_bp": 4, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": exp_ap_cost };

let std_ap_cost = half_off ? 5 : 10;
var fireStd = { "type": "disaster", "raid_element": "fire", "element": "water", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": std_ap_cost };
var waterStd = { "type": "disaster", "raid_element": "water", "element": "water", "use_he": farm_ults, "min_bp": 3, "min_time": 40, "max_participants": 16, "ap": std_ap_cost };
var windStd = { "type": "disaster", "raid_element": "wind", "element": "water", "use_he": farm_ults, "min_bp": 5, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": std_ap_cost };
var thunderStd = { "type": "disaster", "raid_element": "thunder", "element": "fire", "use_he": farm_ults, "min_bp": 1, "min_hp": 80, "max_participants": 19, "ap": std_ap_cost };
var lightStd = { "type": "disaster", "raid_element": "light", "element": "water", "use_he": farm_ults, "min_bp": 4, "min_hp": 40, "min_time": 20, "max_participants": 19, "ap": std_ap_cost };
var darkStd = { "type": "disaster", "raid_element": "dark", "element": "water", "use_he": farm_ults, "min_bp": 4, "min_hp": 80, "min_time": 40, "max_participants": 19, "ap": std_ap_cost };

var storyEvent = {
    "type": "story_event",
    "ap": 10,
    "element": "wind",
    "use_he": false,
    "summon": "Sleipnir"
};

let area_1 = [10, 5, 3]; // good
let area_2 = [15, 10, 4]; // bad
let area_3 = [];
let area_4 = [20, 19, 3]; // good
let area_5 = [];
let area_6 = [20, 29, 3]; // good
let area_7 = [20, 33, 3]; // good
let area = area_7;
var manualQuest = {
    "type": "main",
    "ap": area[0],
    "quest_id": area[1],
    "episode_num": area[2],
    "summon": "Anzu",
    "party": "H"
};

let stdQuests = [fireStd, waterStd, windStd, thunderStd, lightStd, darkStd];
let expQuests = [fireExp, waterExp, windExp, thunderExp, lightExp, darkExp];
let ultQuests = [thunderUlti, waterUlti, fireUlti, lightUlti, darkUlti];//, windUlti];
let ragQuests = [lightRag, fireRag, waterRag, thunderRag, windRag, darkRag];
let guaQuests = [thunderGua, windGua, lightGua, darkGua, fireGua];
let extraQuests = [eventQuest];

//Add all quests you want to check in here: gemQuest, fireRag, waterRag, windRag
//var questPriorityList = [thunderUlti, windUlti, waterUlti, fireUlti, lightUlti, darkUlti,
//                         lightRag, waterRag, fireRag, thunderRag, windRag, darkRag,
//                         ragnarokEventRaid, expertEventRaid, standardEventRaid];
//var questPriorityList = [eventQuest];
//var questPriorityList = [ragnarokEventRaid, expertEventRaid, standardEventRaid];
//var questPriorityList = [manualQuest];
//var questPriorityList = ultQuests.concat(expQuests).concat(stdQuests).concat([thunderGua, thunderRag, fireGua, fireRag, windGua, windRag, lightRag, darkRag, darkGua, waterRag, waterGua]);//guaQuests);
var questPriorityList = expQuests.concat(stdQuests).concat(ultQuests).concat([thunderGua, waterGua, fireGua, fireRag, windGua, windRag, darkRag, darkGua, waterRag]);//guaQuests);
//var questPriorityList = expQuests.concat(stdQuests).concat([dailySP]);

//Raid presets
var disasterRag = { "type": "disaster", "ap": 50, "priority": "participants", "min_hp": 50, "min_bp": 5 };
var unionExpert = { "type": "ue_expert", "party": "K", "max_participants": 1, "not_last": false, "min_hp": 60, "min_bp": 0 };
var unionUltimate = { "type": "ue_ultimate", "party": "K", "max_participants": 15, "not_last": false, "min_hp": 40, "min_bp": 0 };


//Add all raids you want to check in here (raids get checked before quests): , disasterRag, darkRag
//var raidPriorityList = [windRag, fireRag, lightRag, waterGua];
var raidPriorityList = [phantomGua, windGua, fireRag, fireGua, windRag, waterGua, waterRag, thunderGua, thunderRag, lightRag, windGua, fireGua, lightGua, darkGua, darkRag];
//var raidPriorityList = [unionExpert];
//var raidPriorityList = [ragnarokEventRaid];

//Your settings
var parties = { "fire": "D", "water": "E", "wind": "G", "thunder": "C", "dark": "F", "light": "B" };
// For the other parties: "party": "G"
var keepRunning = true;         //If no more quests are available and keepRunning is set to true, the script will go through the priorityLists again after 30 seconds.
var refreshTimeout = 10;        //Seconds that it waits before starting a new cycle on keepRunning
var halfElixirLimit = 80;       //If your half elixir count reaches this limit, the script will stop using them.
var seedLimit = 25;            //If your seed count reaches this limit, the script will stop using them.
var timeoutMulti = 1;           //Multiplier for the wait time between some actions. On a slow computer, consider increasing it to 1.5 or 2 for slower, but more consistent performance.
var selectRecommended = true;   //If enabled and no element is specified for a quest, it'll pick the recommended element. Otherwise it will pick your currently active party.
var debug = false;              //For debugging. Will log the quest to the console instead of starting/joining it.
var rejoinRaid = true;         //If enabled, you'll rejoin union raids that you left, disconnected or died in.

//PriorityList for picking out supports, important to also get friends with off element Eidolons (f.e. Fafnir for water)
var summons = [
    {
        "element": "fire", summonPriority: [
            { "name": "Cerberus" },
            { "name": "Belial" },
            { "name": "Phaleg", "min_level": 86 },
            { "name": "Fafnir", "min_level": 41 },
            { "name": "Echidna", "min_level": 41, "element": "dark" }
        ]
    },
    {
        "element": "water", summonPriority: [
            { "name": "Leviathan" },
            { "name": "Rudra" },
            { "name": "Fafnir", "min_level": 41, "element": "fire" },
            { "name": "Behemoth", "min_level": 71, "element": "wind" }
        ]
    },
    {
        "element": "wind", summonPriority: [
            { "name": "Hanuman" },
            { "name": "Hraesvelgr" },
            { "name": "Bethor", "min_level": 86 },
            { "name": "Sleipnir", "min_level": 56 },
            { "name": "Jabberwock", "min_level": 56 }
        ]
    },
    {
        "element": "thunder", summonPriority: [
            { "name": "Nidhoggr" },
            { "name": "Kirin" },
            { "name": "Phul", "min_level": 86 },
            { "name": "Huanglong", "min_level": 56 },
            { "name": "Girimehkala", "min_level": 56 },
            { "name": "Ouroboros", "min_level": 41, "element": "dark" }
        ]
    },
    {
        "element": "dark", summonPriority: [
            { "name": "Anubis" },
            { "name": "Aratron", "min_level": 86 },
            { "name": "Echidna", "min_level": 41 },
            { "name": "Ouroboros", "min_level": 41 }
        ]
    },
    {
        "element": "light", summonPriority: [
            { "name": "Managarmr" },
            { "name": "Haggith", "min_level": 86 },
            { "name": "Thunderbird", "min_level": 41, "element": "thunder" },
            { "name": "Behemoth", "min_level": 71, "element": "wind" },
            { "name": "Hecatonchires" }
        ]
    },
    {
        "element": "phantom", summonPriority: [
            { "name": "Vampire Vlad Tepes" }
        ]
    }
];

/*available parameters

General:
"type": The type of the quest, this is the only parameter that's always mandatory. See quest types below for available ones.
"element": The element that you want to use for this quest. If not set, it'll either pick the recommended one or the currently active party, depending on selectRecommended.
"party": Selects a specific party for this quest ("A" to "L"). Overwrites "element".
"ap": This allows you to select which difficulty you want to pick. If there is no "ap" property, it'll automatically select the highest difficulty (quests)
        or search for all difficulties (raids). For raids, it's the ap that it would take to host it (even for joining).
"summon": First tries to find said summon. If not specified, it picks the first available helper
"summon_element": If you want to set a helper from a different element (f.e. Sleipnir while using a water team), then you need to specify which element the Eidolon is.

Quest only:
"use_he": If set to false, this quest won't be using any half elixirs.
"do_first": Check if this quest is available before you check for raids
"wait_for_ap": If set to true and the quest is available, it will ignore all quests after this.

Raid only:
"raid_element": To select an element for disaster raids. See elements below for available elements. Has to be set for quests, if not set for raids, it will search for all elements.
"union_only": If a raid has this property set to true, you'll only join raids that contain at least 1 union member.
"min_bp": If a raid has this property, it will only search for it if your current BP exceed the min_bp value. If not set, it'll always join if it finds a suitable one.
"max_participants": If a raid has this property, it will ignore all raids with more than the set amount of participants.
"min_participants": If a raid has this property, it will ignore all raids with less than the set amount of participants.
"min_time": If a raid has this property, it will ignore all raids with less than set time remaining (in minutes).
"min_hp": If a raid has this property, it will ignore all raids with less than set HP remaining(percentual, f.e.: 50 for half HP).
"priority": When you want to join a raid and there are multiple equal options, this will decide the sorting method used to determine the one you join.
"hp": Joins the raid with the highest HP. This is the default.
"union": If there are raids fitting the criteria with union members, it'll join those first. If multiple union raids -> takes the highest HP one.
"time": Selects the raid that still has the most amount of time left.
"participants": Selects the raid that has the least amount of people in it.

UE Demon only:
"id": Which of the 3 open raids you want to join (1-3).
"not_last": Set to true to join one of the first 2 raids (which one gets decided by other filters).
*/


// Start the manager for running the script, put all your battle parameters besides speed up here
var core = new KHBattleManager();

// Set the logging level
core.setLogLevel(Logger.DEBUG);

// Set any user parameters
core.callForHelp = true; // if true, will always call for help in a raid
core.callAll = true;
core.callFriends = true;
core.callUnion = true;

// Set any battle parameters
core.alwaysNuke = true; // if false, nukes will be saved until raging or stun
core.alwaysBurst = true; // if false, burst will be held until a full burst is available
core.burstRaging = false; // if true, burst will never be cast against a boss with a gauge and is not raging or stunned
core.skipSummons = false; // if true, summons will never be cast
core.skipAbilities = false; // if true, abilities will never be cast
core.useProvisionalForest = false; // if true, manager will look ahead to try and cast provisional forest

// Set any timing parameters
core.summon_delay_ms = 0; // Delay in milliseconds to wait after summoning an Eidolon
core.ability_delay_ms = 0; // Delay in milliseconds to wait after using an ability
core.attack_delay_ms = 0; // Delay in milliseconds to wait after declaring an attack
core.target_delay_ms = 400; // Delay in milliseconds to wait after attempting to target an enemy
core.reload_delay_ms = 1800; // Amount of time to wait after reloading to get a stabler state
core.idle_delay_ms = 15000; // Maximum amount of time in milliseconds the manager is willing to idle before reloading
core.revive_delay_ms = 10000; // Revive delay when dying to mitigate risk of memory leaks
core.timeout_min = 30; // Final failsafe timeout in minutes, the script will automaticlly navigate to home after this

// Enable to activate paratrain spam attacks when the enemy is paralyzed
core.paratrain = false;
core.paratrain_attacks = 10;
core.paratrain_avoid_overdrive = true;

// Set above 0 to wait for damage cuts when above the minimum lust level while fighting Lust
core.lust_dmg_cut_lvl = 0;
core.min_lust_lvl = 500;

// Set any paraslave parameters
core.paraslave = false; // Whether or not to act as a paraslave
core.paraslave_attack_more = false; // Whether to attack as a paraslave when overdrive is not ready
core.paraslave_avoid_overdrive = true; // Whether to avoid the overdrive at all costs
core.para_duration_sec = 4; // Minimum number of seconds para must be up in order to attack
core.para_delay_ms = 10000; // Amount of time to wait before reloading when unable to apply para and para is down



// Don't touch this stuff, it runs the script using the parameters above.
let coreQuest = new KHAutoQuest();
coreQuest.questPriorityList = questPriorityList;
coreQuest.raidPriorityList = raidPriorityList;
coreQuest.summons = summons;
coreQuest.parties = parties;

coreQuest.selectRecommended = selectRecommended;
coreQuest.halfElixirLimit = halfElixirLimit;
coreQuest.seedLimit = seedLimit;
coreQuest.battleSpeed = speedUpAnimationBy;
coreQuest.refreshTimer = refreshTimerInMinutes;
coreQuest.timeout = startDelayInMs;
coreQuest.rejoinRaid = rejoinRaid;

core.coreQuest = coreQuest;
coreQuest.coreBattle = core;

coreQuest.run();



