// ==UserScript==
// @name         Gemcha
// @namespace    http://tampermonkey.net/
// @version      04.07.2020
// @description  Auto gem gacha
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

class Gemcha {
    constructor() {
        this.gachaApi = kh.createInstance("apiAGacha");
        this.gachaCategoriesApi = kh.createInstance("apiGachaCategories");
        this.gachaCategoryApi = kh.createInstance("apiGachaCategory");
    }

    async start() {
        await this.prepare();
        await this.run();
    }

    async prepare() {
        this.category = (await this.gachaCategoriesApi.getGachaCategories()).body.tabs.find(x => x.name == "Other").banners.find(x => x.is_normal_gacha).category_id;
        this.gachaInfo = (await this.gachaCategoryApi.get(this.category)).body;
    }

    async run() {
        while (!this.gachaInfo.is_max_weapon_or_summon && this.gachaInfo.groups.length === 1 && this.gachaInfo.groups[0].enabled) {
            let info = (await this.gachaApi.playGacha("normal", this.gachaInfo.groups[0].gacha_id)).body;
            console.log(info.obtained_info);
            this.gachaInfo = (await this.gachaCategoryApi.get(this.category)).body;
        }

        if (this.gachaInfo.is_max_weapon_or_summon) {
            console.log("Inventory is full, clear it and start script again. Stop script");
        } else {
            console.log("All normal gacha attempts were used, stop script");
        }
    }
}

function sleep(ms)
{
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function start(){
    while (!(kh && kh.createInstance && kh.Api && kh.Api.AWeapons)) {
        console.log("sleep");
        await sleep(1000);
    }

    try {
        let gemcha = new Gemcha();
        await gemcha.start();
    } catch (exception) {
        console.log(exception.message)
        await sleep(1000)
        start();
    }
}

start();