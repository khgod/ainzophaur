// ==UserScript==
// @name         SpeedUp
// @namespace    http://tampermonkey.net/
// @description  change speed up factor of animation in KamiHime battles
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.g.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/battle/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/battle/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

const defaultSpeedUp = 10; // For single player content
const unionEventSpeedUp = 10; // For union events
const raidEventSpeedUp = 3; // For event raids and Lilims
const disasterSpeedUp = 3; // For disaster raids

async function start() {
    while(!(kh && kh.createInstance && cc && cc.director && cc.director.getRunningScene() != null && cc.director.getRunningScene().getQuestType)) {
        await sleep(100);
    }

    let config = kh.createInstance("playerGameConfig");
    let questType = cc.director.getRunningScene().getQuestType();
    switch (questType) {
        case "raid":
            config.BATTLE_SPEED_SETTINGS.quick = disasterSpeedUp;
            break;
        case "event_raid":
        case "event_union_lilim_raid":
            config.BATTLE_SPEED_SETTINGS.quick = raidEventSpeedUp;
            break;
        case "event_union_demon_raid":
            config.BATTLE_SPEED_SETTINGS.quick = unionEventSpeedUp;
            break;
        default:
            config.BATTLE_SPEED_SETTINGS.quick = defaultSpeedUp;
            break;
    }
}

function sleep(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

start();
