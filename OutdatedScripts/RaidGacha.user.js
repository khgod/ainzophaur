// ==UserScript==
// @name         Raid Gacha
// @namespace    http://tampermonkey.net/
// @version      26.09.2019
// @description  Claim raid gacha
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/raidevent/app.html*
// @include      https://cf.g.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/raidevent/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

// Your settings
const log = true;

var gacha, gachaId, logInfo;

function start(){
    if (has(kh, "createInstance")) {
        claimRaidGacha();
    } else {
        console.log('waiting for page');
        setTimeout(start, 2000);
    }
}

async function claimRaidGacha() {
    gacha = kh.createInstance("apiAGacha");
    if (log) {
        logInfo = new LogInfo();
    }
    let banner = (await kh.createInstance("apiABanners").getMypageBanners()).body.data.find(x => x.event_type === "raid_event" || x.event_type === "raid_event_receive_reward");
    if (banner == undefined) {
        console.log("Banner not found!");
    }
    let raidGacha = (await kh.createInstance("apiAGachaCategories").getEventGachaCategories(banner.event_id)).body.data[0].gacha_information;
    gachaId = raidGacha[raidGacha.length - 1].gacha_id;
    let amount = (await kh.createInstance("apiAEvents").getRaidEvent(banner.event_id)).body.event_tickets[0].amount;
    let possiblePulls = amount % 10 === 1 ? Math.floor(amount / 10) : Math.ceil(amount / 10);
    for (let i=0; i<possiblePulls; i++) {
        console.log(`Pull ${i + 1} / ${possiblePulls}`);
        await pull();
    }
    if (log) {
        logInfo.log();
    }
    console.log("Done.");
}

async function pull() {
    let info = (await gacha.playGacha("event", gachaId)).body.obtained_info;
    if (log) {
        logInfo.addPull(info);
    }
}


class LogInfo {
    constructor() {
        this.jewels = 0;
        this.gems = 0;
        this.ssrWeapon = 0;
        this.ssrEido = 0;
        this.srWeapon = 0;
        this.bonusWeapon = 0;
        this.bonusEido = 0;
        this.seeds = 0;
        this.pullCount = 0;
    }

    addPull(items) {
        this.pullCount += items.length;
        items.forEach(item => {
            if (has(item, "item_info")) {
                switch (item.item_info.name) {
                    case "Gems":
                        this.gems += 1000;
                        break;
                    case "Magic Jewels":
                        this.jewels += 5;
                        break;
                    case "Energy Seed":
                        this.seeds++;
                        break;
                }
            } else if (has(item, "weapon_info")) {
                this.bonusWeapon += item.weapon_info.bonus;
                switch (item.weapon_info.rare) {
                    case "SSR":
                        console.log("Found " + item.weapon_info.name);
                        this.ssrWeapon++;
                        break;
                    case "SR":
                        if(item.weapon_info.name.includes("Seraph ") && item.weapon_info.element_type === 5) {
                            break;
                        }
                        this.srWeapon++;
                        break;
                    default:
                        break;
                }
            } else if (has(item, "summon_info")) {
                this.bonusEido += item.summon_info.bonus;
                switch (item.summon_info.rare) {
                    case "SSR":
                        console.log("Found " + item.summon_info.name);
                        this.ssrEido++;
                        break;
                }
            }
        });
    }

    log() {
        console.log(
`Total Pulls: ${this.pullCount}
SSR Weapon: ${this.ssrWeapon}
SSR Eidos: ${this.ssrEido}
SR Weapons: ${this.srWeapon}
+1 Weapons: ${this.bonusWeapon}
+1 Eidos: ${this.bonusEido}
Jewels: ${this.jewels}
Gems: ${this.gems}
Seeds: ${this.seeds}`);
    }
}

function has(obj) {
	var prop;
	if (obj !== Object(obj)) {
		return false;
	}
	for (var i = 1; i < arguments.length; i++) {
		prop = arguments[i];
		if ((prop in obj) && obj[prop] !== null && obj[prop] !== 'undefined') {
			obj = obj[prop];
		} else {
			return false;
		}
	}
	return true;
}

start();