// ==UserScript==
// @name         Shop
// @namespace    http://tampermonkey.net/
// @version      29.09.2019
// @description  Exchange shop items
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/mypage_quest_party_guild_enh_evo_gacha_present_shop_epi_acce_detail/app.html*
// @include      https://cf.g.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/mypage_quest_party_guild_enh_evo_gacha_present_shop_epi_acce_detail/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/list/app.html*
// @include      https://cf.g.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/list/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

const dragonBones = { name: "Dragon Bone" , shop: "Material", min_rem: 500};
const lithographs = { name: "Lithograph", shop: "Material", min_rem: 500};
const runes = { name: "Rune", shop: "Material", min_rem: 500};
const crystal_1 = { name: "Holy Jewel of Light" , shop: "Material", min_rem: 150 };
const crystal_2 = { name: "Holy Crystal of Light" , shop: "Material", min_rem: 100 };
const crystal_3 = { name: "Holy Star of Light" , shop: "Material", min_rem: 75 };
const regalia = { name: "Regalia" , shop: "Material", max_cost: 5};
const oris = { name: "Orichalcon" , shop: "Material", min_rem: 200}; 
const he = { name: "Half Elixir", shop: "Eidolon Orb Exchange", min_rem: 15000 };

const eye = { name: "Draconic Eye", shop: "Draconic Eye" };

const acc_ring1 = { name: "Inferno Ring", shop: "Acce P Exchange", precise_name: true };
const acc_ring2 = { name: "Deluge Ring", shop: "Acce P Exchange", precise_name: true };
const acc_ring3 = { name: "Hurricane Ring", shop: "Acce P Exchange", precise_name: true };
const acc_ring4 = { name: "Plasmic Ring", shop: "Acce P Exchange", precise_name: true };
const acc_ring5 = { name: "Divine Ring", shop: "Acce P Exchange", precise_name: true };
const acc_ring6 = { name: "Demonic Ring", shop: "Acce P Exchange", precise_name: true };

// Enter the items you want to exchange here!
const items = [dragonBones, lithographs, runes, regalia, oris, crystal_1, crystal_2, crystal_3, eye, acc_ring1, acc_ring2, acc_ring3, acc_ring4, acc_ring5, acc_ring6];

// max_cost: If cost of the item surpasses a certain amount, it won't be added to exchange list (f.e. for regalias).
// min_rem: If materials are below this amount, it won't add it to the exchange list (it will still drop below it by the cost of the x pulls it does).
// precise_name: if turned off or not set, it will look for items that include the specified name, hence f.e. for acc, it will also exchange ancient rings.
// shop: if not specified, any shop having the item will be accepted.
// name: has to be specified.


function start(){
    if (has(kh, "createInstance")){
        exchange();
    } else {
        console.log('waiting for page');
        setTimeout(start, 2000);
    }
}

async function exchange(){
    let shop = kh.createInstance("apiShop");
    for (let c=4; c<9; c++) {   // Loop all categories
        let shops = (await shop.getShop(c)).body.catalogs;
        for (let s=0; s<shops.length; s++) {    // Loop all stores
            let products = shops[s].products.filter(x => x.can_buy);
            for (let i=0; i<products.length; i++) {
                let item = new Item(products[i]);
                if (!item.isIncluded(shops[s].name)) {  // Match item
                    continue;
                }
                let amount = item.calculateAmount();    // Get amount you can exchange

                if (amount > 0) {
                    await shop.buyProduct(item.product.product_id, amount);     // Exchange
                    console.log("Exchanged " + amount + " " + item.product.name);
                }
            }
        }
    }

    console.log("Done exchanging!");
}

class Item {
    constructor (product) {
        this.product = product;
    }

    isIncluded(shopName) {
        let include = false;
        items.forEach(item => {
            if (has(item, "name") 
                    && ((!(has(item, "precise_name") && item.precise_name) && this.product.name.includes(item.name)) || this.product.name === item.name)    // Check if included
                    && (!has(item, "shop") || item.shop === shopName)   // Check if shop matches
                    && (!has(item, "max_cost") || this.product.materials[0].required_amount <= item.max_cost)) {    // Check if max cost matches
                this.info = item;
                include = true;
            }
        });
        return include;
    }

    calculateAmount() {
        let max = this.product.stock_info.amount != "" ? this.product.stock_info.amount : Infinity;
        let minRemaining = has(this.info, "min_rem") ? this.info.min_rem : 0;   // Get min remaining property
        this.product.materials.forEach(function(mat) {
            let can_afford = Math.floor((mat.current_amount - minRemaining) / mat.required_amount); // Calculate maximum amount you can afford
            if (max > can_afford) {
                max = can_afford;
            }
        });

        return max;
    }
}

function has(obj) {
    var prop;
    if (obj !== Object(obj)) {
        return false;
    }
    for (var i = 1; i < arguments.length; i++) {
        prop = arguments[i];
        if ((prop in obj) && obj[prop] !== null && obj[prop] !== 'undefined') {
            obj = obj[prop];
        } else {
            return false;
        }
    }
    return true;
}

console.log("starting exchange");
start();