// ==UserScript==
// @name         Presents
// @namespace    http://tampermonkey.net/
// @version      04.07.2020
// @description  Claim selected presents
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==


const batchSize = 30; // Number of presents claimed at once
const claimPermanent = true;
const claimExpiring = true;
const applyFilterToExpiring = true;

// Filter presets
const rWeapons = x => { return x.kind === "weapon" && x.weapon_info.rare === "R"};
const srWeapons = x => { return x.kind === "weapon" && x.weapon_info.rare === "SR"};
const ssrWeapons = x => { return x.kind === "weapon" && x.weapon_info.rare === "SSR"};
const summon = x => { return x.kind === "summon"};
const other = x => { return x.kind !== "summon" && x.kind !== "weapon" };

// Set your filter here
const filters = [other, summon, rWeapons];


class GiftBox {
    constructor() {
        this.presentsApi = kh.createInstance("apiAPresents");
        this.weaponsApi = kh.createInstance("apiAWeapons");
        this.summonsApi = kh.createInstance("apiASummons");
        this.items = [];
    }

    async run() {
        this.me = (await kh.createInstance("apiAPlayers").getMeNumeric()).body;
        await this.filterItems();
    
        while (this.items.length > 0 && await this.canPull()) {
            console.log("Claim batch.");
            let min = Math.min(batchSize, this.items.length);
            let currentItems = []
            for (let i=0; i<min; i++) {
                currentItems.push(this.items.pop());
            }
            await this.tradeChunk(currentItems);
        }
    }

    async filterItems() {
        if (claimPermanent) {
            Array.prototype.push.apply(this.items, (await this.presentsApi.getPresentList("normal", 1, 10000)).body.data.filter(x => { return this.getFilter(x); }));
        }
        if (claimExpiring) {
            let expiring = (await this.presentsApi.getPresentList("timelimit", 1, 10000)).body.data;
            if(applyFilterToExpiring) {
                expiring = expiring.filter(x => { return this.getFilter(x); });
            }
            Array.prototype.push.apply(this.items, expiring);
        }
    }
    
    async canPull() {
        if((await this.weaponsApi.getList("book_weapon", 1, 100000)).body.exists_record_count + batchSize > this.me.max_weapon_num) {
            console.log("Not enough weapon space!");
            return false;
        }
        if((await this.summonsApi.getList("book_summon", 1, 100000)).body.exists_record_count + batchSize > this.me.max_summon_num) {
            console.log("Not enough eido space!")
            return false;
        }
    
        return true;
    }
    
    async tradeChunk(items) {
        let promises = [];
        for (let i=0; i<items.length; i++) {
            promises.push(this.presentsApi.receivePresent(items[i].a_present_id));
        }
        for (let i=0; i<promises.length; i++) {
            await promises[i];
        }
    }
    
    getFilter(x) {
        for (let index = 0; index < filters.length; index++) {
            const filter = filters[index];
            if (filter(x)) {
                return true;
            }
        }
        return false;
    }
}

function sleep(ms)
{
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function start(){
    while (!(kh && kh.createInstance && kh.Api && kh.Api.AWeapons)) {
        console.log("sleep");
        await sleep(1000);
    }

    try {
        console.log("Starting present script!")
        let giftBox = new GiftBox();
        await giftBox.run();
    } catch (exception) {
        console.log(exception.message)
        await sleep(1000)
        start();
    }
}

start();