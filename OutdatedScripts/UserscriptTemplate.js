// ==UserScript==
// @name         Experimental
// @namespace    http://tampermonkey.net/
// @version      04.05.2018
// @description  full auto in battle in Kamihime game
// @author       You
// @include      https://cf.g.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/battle/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/battle/app.html*
// @require      file:///C:\Users\{YOUR_DIRECTIORY_GOES_HERE}\Utility\logger.js
// @require      file:///C:\Users\{YOUR_DIRECTIORY_GOES_HERE}\KHBattleManager.js
// @grant        none
// @run-at       document-end
// ==/UserScript==

// Start the manager for running the script
var core = new KHBattleManager();

// Set the logging level
core.setLogLevel(Logger.DEBUG);

// Set any user parameters
core.callForHelp = true; // if true, will always call for help in a raid
core.callAll = false;
core.callFriends = true;
core.callUnion = true;

// Set any battle parameters
core.alwaysNuke = true; // if false, nukes will be saved until raging or stun
core.alwaysBurst = false; // if false, burst will be held until a full burst is available
core.burstRaging = false; // if true, burst will never be cast against a boss with a gauge and is not raging or stunned
core.skipSummons = false; // if true, summons will never be cast
core.skipAbilities = false; // if true, abilities will never be cast
core.useProvisionalForest = true; // if true, manager will look ahead to try and cast provisional forest

// Set any timing parameters
core.summon_delay_ms = 0; // Delay in milliseconds to wait after summoning an Eidolon
core.ability_delay_ms = 0; // Delay in milliseconds to wait after using an ability
core.attack_delay_ms = 0; // Delay in milliseconds to wait after declaring an attack
core.target_delay_ms = 100; // Delay in milliseconds to wait after attempting to target an enemy
core.reload_delay_ms = 800; // Amount of time to wait after reloading to get a stabler state
core.idle_delay_ms = 10000; // Maximum amount of time in milliseconds the manager is willing to idle before reloading
core.timeout_min = 30; // Final failsafe timeout in minutes, the script will automaticlly navigate to home after this

// Enable to activate paratrain spam attacks when the enemy is paralyzed
core.paratrain = false;
core.paratrain_attacks = 10;
core.paratrain_avoid_overdrive = true;

// Set above 0 to wait for that number of damage cuts when above the minimum lust level
core.lust_dmg_cut_lvl = 0;
core.min_lust_lvl = 500;

// Set any paraslave parameters
core.paraslave = false; // Whether or not to act as a paraslave
core.paraslave_attack_more = false; // Whether to attack as a paraslave when overdrive is not ready
core.paraslave_avoid_overdrive = true; // Whether to avoid the overdrive at all costs
core.para_duration_sec = 4; // Minimum number of seconds para must be up in order to attack
core.para_delay_ms = 500; // Amount of time to wait before reloading when unable to apply para and para is down

// Runs the script
core.run();