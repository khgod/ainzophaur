# How to use the scripts

## Set up Tampermonkey (Chrome)
1.) Just follow https://www.youtube.com/watch?v=cu4XeYtqXbM  

## How to use a script
1.) Go to repository (https://bitbucket.org/khgod/khscripts/src/master/)  

2.) Click on the script you want to install

![alt text](https://i.imgur.com/VfEImUQ.png)  

3.) Click into the window containing the code, select all text (Ctrl + A) and copy it (Ctrl + C)

![alt text](https://i.imgur.com/ZwudksQ.png)  

4.) Open Tampermonkey dashboard

![alt text](https://i.imgur.com/ZKx2JiI.png)  

5.) Click "New script" (+ sign), replace the default code with the copied code from the repo (Ctrl + V) and save (Ctrl + S)

![alt text](https://i.imgur.com/zcC6ET1.png)  

6.) Now you can enable and disable the script in the dashboard (initially it is enabled by default)

![alt text](https://i.imgur.com/VteFsPh.png)  

If a script is enabled, it will execute once the starting conditions for it are met (see Scripts section).  


## Battle speed up.user.js  

Speeds up battle animations. For use for solo quests or union events.  
DO NOT USE IN PUBLIC RAIDS!  

**Activation:** Automatically upon entering battle  

**Parameters to tune:**  

Speed-up: The amount by which the animation gets sped up  
  
```javascript
var speedUpAnimationBy = 10;//speed up animation by x times, in game fast is 1.6
```

## aab.user.js and ab.user.js  

Toggles on AAB (or AB respectively) upon entering battle and automatically leaves once the battle is over.  

**Activation:** Automatically upon entering battle  

**Parameters to tune:**  
Call for help: Especially for high rags, you might want to set ```callAll``` to false.  

```javascript
var callForHelp = true;     //Call for help during your raids
var callAll = true;        //Call all for help
var callFriends = true;    //Call friends for help
var callUnion = true;       //Call union for help
```

## aab smart big first.user.js and aab smart small first.user.js  

Upon entering battle, automatically selects abilities to use. Basically a smarter version of aab.user.js. Use this script, unless it causes your game to crash more often. The difference between big first and small first is the order in which enemies get focused.
Small first first targets adds (useful for f.e. Water or Wind rag), big first focuses bosses (useful for f.e. Fire rag).  

**Activation:** Automatically upon entering battle  

**Parameters to tune:**  
Call for help: Same as in aab.user.js  

```javascript
var callForHelp = true;     //Call for help during your raids
var callAll = true;        //Call all for help
var callFriends = true;    //Call friends for help
var callUnion = true;       //Call union for help
```

Some parameters for the battle script that you can generally just leave at default.  
```javascript
var autoBattle = true; //set to false to end turns manually
var useBurstAbilitesOnNormalGauge = true; //use (true) or not (false) burst and attack abilities during normal gauge on bosses
var strongEnemyHP = 300000;//HP for Enemy. Script will use all debuffs and damage skills if enemy has more HP than this.
var waitAnimationTime = 40000;// how long to wait animation or hung before force reload
```

## Quest.user.js  

Allows you to define a list of quests/raids that the script will automatically start/join. In combination with one of the AAB scripts, it allows you to farm events automatically. 

**Activation:** Activates at start page, main page and a few other sub pages.  
If your game was already running before enabling the script, then:  
1. Toggle script on  
2. Go to Equipment  
3. Go to MyPage  

**Parameters to tune:**  
Unlike in the previous scripts, here it is important to adjust the settings.

**Settings:**  
```javascript
//Your settings
var parties = {"fire": "A","water": "D","wind": "B","thunder": "E","dark": "F","light": "C"};
var keepRunning = true;         //If no more quests are available and keepRunning is set to true, the script will go through the priorityLists again after 30 seconds.
var refreshTimeout = 15;        //Seconds that it waits before starting a new cycle on keepRunning
var halfElixirLimit = 150;      //If your half elixir count reaches this limit, the script will stop using them.
var seedLimit = 100;            //If your seed count reaches this limit, the script will stop using them.
var timeoutMulti = 1;           //Multiplier for the wait time between some actions. On a slow computer, consider increasing it to 1.5 or 2 for slower, but more consistent performance.
var selectRecommended = true;   //If enabled and no element is specified for a quest, it'll pick the recommended element. Otherwise it will pick your currently active party.
var debug = false;              //For debugging. Will log the quest to the console instead of starting/joining it.
var rejoinRaid = true;         //If enabled, you'll rejoin union raids that you left, disconnected or died in.
var dailyIndex = 0;            //Keep at 0, unless it's half-SP week
```

* **Parties**: This decides which party is used for the different elements.  
In this example: When a fire is used, then party A will be selected. Adjust all the letters appropriately!  
```var parties = {"fire": "A","water": "D","wind": "B","thunder": "E","dark": "F","light": "C"};```  
* **Recovery items:**: You set limits for half elixirs and seeds. If HE/seed count drops below the specified number, the script will stop using them.  
```javascript
var halfElixirLimit = 150;      //If your half elixir count reaches this limit, the script will stop using them.
var seedLimit = 100;            //If your seed count reaches this limit, the script will stop using them.
```  
* The remaining parameters here in settings can be tuned too, but are not as important and can usually be kept at default. The comments should explain what they are used for though.

**Priority lists:**  
```javascript
//Add all quests you want to check in here:
var questPriorityList = [aq5];

//Add all raids you want to check in here (raids get checked before quests):
var raidPriorityList = [unionExpert];
```

These two lists (questPriorityList and raidPriorityList) are the most important part of the script, since they decide what quests/raids the script will look for.  
Generally raids are checked before quests (unless a quest has the ```do_first``` attribute set to true - more on that later).  
The script will go through the list and when it finds a quest/raid that matches the conditions, it will automatically join it.  
The lists accept multiple presets. For example:  

```javascript
var questPriorityList = [aq5, dailySP];

```
First the script will look for an aq5 quest. If none is available (f.e. all entries used up), it will check the dailySP quest.

**Quest Presets and filter parameters:** 
```javascript
var aq5 = {"type": "accessory", "use_he": true, "do_first": true};
var eventQuest = {"type": "event", "use_he": false, "element": "dark", "ap": 30};
var dailySP = {"type":"daily", "use_he": false, "party": "G", "summon": "Sleipnir"};
```

There are a lot of ways to specify which what quests you want to join and when you want to join them.  
I will try to introduce the most important parameters using some examples.  

**Quest Example 1:**  
```javascript
var aq5 = {"type": "accessory", "use_he": true, "do_first": true};
```

* **Type:**  
Every quest preset must have a type to specify the quest you want to do. In Example 1, the type is "accessory", so it will only look for accessory quests.

* **do_first**  
If a quest has this flag, it will be checked before the script looks for available raids to join.  
Can be very useful during f.e. push days where you want to spend your excess AP, but otherwise focus on pushing.  

* **use_he**  
If this is set to false, then the script won't use any elixirs to join the the quest. Very useful when you want to farm on regen.  

**Quest Example 2:**  
```javascript
var eventQuest = {"type": "event", "use_he": false, "element": "dark", "ap": 30};
```

* **ap**  
Using the property ```ap```, you can specify which quest will be chosen when there are multiple of the same type (f.e. accessory, dailySP, advents, disaster rags, etc.).  
You might have noticed that Example 1 did not use the property ```ap```. In this case, it will automatically pick the most expensive one (which in Example 1 would be the 40AP accessory quest).  

* **element**  
The ```element``` property can be used to select an element to use against this quest.  
The script will automatically pick the party you selected in settings for the given element.  
If not specified, the recommended element will be used.  

**Quest Example 3:**  
```javascript
var dailySP = {"type":"daily", "use_he": false, "party": "G", "summon": "Sleipnir"};
```

* **party**  
Alternatively, you can specify a party directly using the ```party``` property. NOTE: Party overwrites element, in case both exist in the same preset.  

* **summon**  
If you want to use a specific helper eidolon, then you can use the ```summon``` property.  
If none is specified, the script will automatically pick the best suited for the selected party / element.

**Raid Presets and filter parameters:**  
```
var fireRag = {"type": "disaster", "raid_element": "fire", "ap": 70, "min_hp": 70, "min_time": 50, "min_bp": 4};
var unionExpert = {"type": "ue_expert", "min_hp": 70, "not_last": false };
var unionUltimate = {"type": "ue_ultimate", "min_hp": 70, "id": 3 };
```

Similarly, there are filters to decide what raids to join.  
Once again I will introduce the most important parameters on working examples.  

**Raid Example 1:**  
```javascript
var fireRag = {"type": "disaster", "raid_element": "fire", "ap": 70, "min_hp": 70, "min_time": 50, "min_bp": 4};
```

* **Type:**  
Just as in quests, ```type``` is mandatory and used to decide which raids to look for (in Example 1, it would search for disaster raids).  

* **raid_element:**  
```raid_element``` is used whenever multiple elements for the same type of raid are available (so currently mostly disasters).  
If no element is selected, it will look for any raids of the given type.  


* **ap**  
Using the property ```ap```, you can specify that only specific raids are joined. The ap cost refers here to the cost that it would take to host the raid.  
As shown in Example 1, to join a disaster rag, you need to set ap to 70.  
If no ap was specified, it will attempt to join any raids (standards, experts, ults and rags).  

* **min_hp and min_time**  
Useful properties to filter out raids you might not want to join. The script will only join raids that have more hp and more time remaining than specified in ```min_hp``` and ```min_time``` respectively.

* **min_bp**  
You can specify a minimum number of BP before the script starts to search for a raid.  
In Example 1, the script will only join the fire rag if the player has 4BP or more, therefore only using 1 seed at best.

**Raid Examples 2 and 3:**  
```javascript
var unionExpert = {"type": "ue_expert", "min_hp": 70, "not_last": true };
var unionUltimate = {"type": "ue_ultimate", "min_hp": 70, "id": 3 };
```

There are two ways of selecting which union demon raid to join.

* **not_last**  
Using the ```not_last``` property (shown in Example 2), the script will only join one of the top 2 raids. Useful when there are not enough people to clear all 3.  

* **id**  all for help during your raids
var callAll = true;        //C
You can select which ```id``` you want to join. Id can be an integer between 1 and 3, 1 being the top raid, 3 being the bottom raid.  
Id overwrites not_last.  

* **element, party, summon**  
Of course all properties to select a party are also available for raids and work the same way.  

There are a few more properties available (see Available properties part in the script itself), but these should be enough to get you started.  


## Nike handler.user.js  

Reloads the page if an error (Nike screen) occurs.

**Activation:** Automatically, just keep it enabled.


## gemcha.user.js  

Automatically does your gem-gacha pulls. 

**Activation:** Activates upon going to weapon sell.  
1. Toggle script on  
2. Go to Equipment  
3. Click sell  

**Parameters to tune:**  
What weapons/eidolons get sold (also sells weapons previously in the inventory, not just new ones).  
Mostly irrelevant now that we have auto sell, but can still be nice occasionally to clean up fodder you have.  

```javascript
var sell_R_Eidolons = true;
var sell_R_enchantment_weapons = true;
var sell_SR_enchantment_eidos = false;
var sell_SR_enchantment_weapons = false;
```

## enhance.user.js  

Skill levels fodder for you. SRs to 4, SR grails to 5, R grails to 3.

**Activation:** Activates upon going to enhance (although MyPage as well as some other pages should work too).  
1. Toggle script on  
2. Go to Equipment  
3. Click enhance  

**Parameters to tune:**  
What weapons to skill level. ```stackRs``` will level all your remaining Rs to slvl 3, the rest should be self-explanatory

```javascript
var enhanceSRgrails = false;
var enhanceRgrails = true;
var enhanceSRs = true;
var stackRs = false;
```


## overlay.user.js  

Adds hp percentage for enemies, but only really looks okish on direct link. Ignore it for the most part, though can be nice for tower 15F.

**Activation:** Automatically activates upon entering battle.  

## AQ5 Quest.user.js  

Same as quest for the most part, but can be used to do AQ5 4 times a day.


## shop.user.js  

Out of date i think. Ignore it.


## PullPlayerData

What we use to parse the data and update the member sheet. Don't use it yourself.