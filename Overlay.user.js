// ==UserScript==
// @name         Overlay
// @namespace    http://tampermonkey.net/
// @description  Shows percental / current and max hp
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

var battleWorld, stage = -1, lastId, instanceId;

async function start(){
    while (true) {
        await sleep(1000);

        if (kh && kh.createInstance && kh.Api && kh.BattleWorld) {
            try {
                battleWorld = await kh.createInstance("battleWorld");
                // console.log("Restarting overlay");
                if (battleWorld.enemyList.length != 0) {
                    instanceId = battleWorld.__instanceId;
                    create();
                    overrideMethods();
                    while (true) {
                        // console.log("Overlay running")
                        await sleep(1000);
                        battleWorld = (await kh.createInstance("battleWorld"));
                        if (battleWorld.__instanceId != instanceId) {
                            // New battleWorld instance (e.g. menu), start from beginning again
                            stage = -1
                            break;
                        }
                    }          
                }      
            } catch (exception) {
                // console.log("Overlay Waiting for battle");
            }
        }
    }
}

function overrideMethods() {
    // Update overlay when enemy hp changes
    kh.Enemy.prototype.adjustHP = function(t) {
        kh.Avatar.prototype.adjustHP.call(this, t);
        updateHPText(this);
    };

    // Update overlay on reload
    kh.ReloadButton.prototype._onReload = function(t) {
        console.log("reloading ");
        battleWorld.reloadBattle();
        waitForReload();
    };

    // Update overlay at stage transition
    kh.Stage.prototype.setStageCleared = function(t) {
        this._stageCleared = t;
        handleSceneChange();
    };
}

async function handleSceneChange() {
    battleWorld = await kh.createInstance("battleWorld");
    if (battleWorld.enemyList.find(x => x != null) != undefined && battleWorld.enemyList.some(x => x.__instanceId != undefined)) {
        // Triggers at the beginning of a stage
        console.log("new stage")
        waitForReload()
    } else {
        // Triggers at the end of a stage
        console.log("cleared stage");
    }        
}

async function waitForReload() {
    battleWorld = await kh.createInstance("battleWorld");
    while (battleWorld.enemyList.find(x => x != null) == undefined || battleWorld.enemyList.find(x => x !=null ).__instanceId === instanceId) {
        await sleep(100);
        battleWorld = await kh.createInstance("battleWorld");
    }

    await create();
    stage = battleWorld.stage._currentStage;
}

async function create() {
    while (battleWorld.enemyList.length === 0) {
        await sleep(100);
    }
    let enemyList = battleWorld.enemyList;
    console.log(enemyList);
    enemyList.filter(x => x != null).forEach(enemy => {
        let hp_text = new ccui.Text();
        hp_text.setName("hp_text");
        hp_text.setFontSize(10);
        hp_text.setPosition([234, 145, 145][enemyList.length -1 ], 42);
        enemy.statusPanel.ui.addChild(hp_text);
        updateHPText(enemy);
    });
}

function sleep(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

function updateHPText(enemy) {
    ccui.helper.seekWidgetByName(enemy.statusPanel.ui, "hp_text").setText(`${formatNumber(enemy.hp)}/${formatNumber(enemy.maxHp)} (${(100 * enemy.hp / enemy.maxHp).toFixed(2)}%)`);
};

start();
