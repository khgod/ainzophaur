// =================================== START: Globals =================================== //
var questTypes = ["daily", "gem", "gem_daily", "accessory", "lilim", "disaster", "event_advent", "event_raid", "story_event"];
var elements = {
	fire    : 0,
	water   : 1,
	wind    : 2,
	thunder : 3,
	dark    : 4,
	light   : 5,
	phantom : 8,
	[0]: "fire",
	[1]: "water",
	[2]: "wind",
	[3]: "thunder",
	[4]: "dark",
	[5]: "light",
	[8]: "phantom"
};
var party_map = {
	"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5,
	"G": 6, "H": 7, "I": 8, "J": 9, "K": 10, "L": 11
};
// ==================================== END: Globals ==================================== //

if (Error != undefined && Error.captureStackTrace != undefined)
{
	Error.captureStackTrace_copy = Error.captureStackTrace;
	Error.captureStackTrace = (obj, t) =>
	{
		Error.captureStackTrace_copy(obj, t);
		let stack = "";
		let lines = obj.stack.split('\n');
		for (const line of lines)
		{
			let substrings = ['chrome-extension', 'userscript', 'tampermonkey', 'greasemonkey'];
			if (substrings.some((e) => { return line.indexOf(e) > -1; }))
				continue;
			stack += line + '\n';
		}
		obj.stack = stack.trim();
	}
}


// URL sanitizer
((() =>
{
	const origOpen = XMLHttpRequest.prototype.open;
	XMLHttpRequest.prototype.open = function (method, url, ...args)
	{
		let blockedurls = ['/a_players/me/register'];
		if (blockedurls.some((e) => { return url.indexOf(e) > -1; }))
			throw { message: "Blocked call to URL=" + url + ' with METHOD=' + method, success: false };
		origOpen.apply(this, arguments);
	};
}))();


class KHQuest
{
	constructor()
	{

	}
}

class KHAutoQuest
{
	constructor()
	{
		// Quest parameters
		this.quest_info = undefined;
		this.player = undefined;
		this.ap = undefined;
		this.bp = undefined;
		this.decks = undefined;
		this.speedUp = false;

		// User-Set parameters
		this.selectRecommended = selectRecommended;
		this.halfElixirLimit = halfElixirLimit;
		this.seedLimit = seedLimit;
		this.battleSpeed = 3.2;
		this.timeout = 15000;
		this.refreshTimer = 30;
		this.rejoinRaid = true;
		this.Muryokusho = (e) => { return; };

		this.coreBattle = null;
	}

	async run()
	{
		try
		{
			await sleep(this.timeout);

			// Re-route path to GAME if the URL matches TOP.
			if (location.pathname == "/front/cocos2d-proj/components-pc/top/app.html")
			{
				location.href = location.href.split("/top/app.html")[0] + "/game/app.html";
				return;
			}
			else if (location.href.startsWith("https://www.nutaku."))
			{
				let ms_per_min = 1000 * 60;
				setTimeout("location.reload(true);", this.refreshTimer * ms_per_min);
				return;
			}
			while (kh == undefined || kh.createInstance == undefined)
			{
				await sleep(this.timeout);
			}

			console.log("Adjusting battle speed...");
			// Speed up
			if (!this.speedUp)
			{
				let config = await kh.createInstance("playerGameConfig");
				if (config != undefined && config.BATTLE_SPEED_SETTINGS.quick != this.battleSpeed)
				{
					config.BATTLE_SPEED_SETTINGS.quick = this.battleSpeed;
					this.speedUp = true;
				}
			}

			// Disable known detection endpoints
			if (kh.Monitor != undefined && kh.Monitor.prototype.checkPath != this.Muryokusho)
			{
				let temp = kh.Monitor.prototype;
				kh.Monitor.prototype = {};
				for (const param in temp)
				{
					kh.Monitor.prototype[param] = temp[param];
				}
				kh.Monitor.Error = Error;
				kh.Monitor.prototype.Muryokusho = this.Muryokusho;
				kh.Monitor.prototype.checkPath = kh.Monitor.prototype.Muryokusho;
				console.log("Domain Expansion: <Infinite Void>");
			}
			else if (kh.Monitor === undefined || kh.Monitor.prototype.checkPath === undefined)
			{
				console.log("KHMonitor was undefined, aborting script. REPORT THIS ERROR ASAP.");
				return;
			}
			if (kh.Api != undefined && kh.Api.APlayers != undefined && kh.Api.APlayers.prototype.register != this.Muryokusho)
			{
				if (kh.Api.APlayers.prototype.register === undefined)
				{
					console.log("ApiAPlayers.register was undefined, aborting script. REPORT THIS ERROR ASAP.");
					return;
				}
				kh.Monitor.Error = Error;
				kh.Api.APlayers.prototype.Muryokusho = this.Muryokusho;
				kh.Api.APlayers.prototype.register = kh.Monitor.prototype.Muryokusho;
			}

			console.log("Retrieving player info...");
			// Initialize player info
			let player_info = await this.getPlayerInfo();
			this.quest_info = player_info.quest_info;
			this.player = player_info.player;
			this.ap = player_info.points.ap;
			this.max_ap = player_info.points.max_ap;
			this.bp = player_info.points.bp;
			this.max_bp = player_info.points.max_bp;
			this.decks = player_info.parties;

			console.log("Retrieving quest info...");
			// Initialize quest info
			let quest_info = await this.getQuests();
			this.sp_quests = quest_info.sp_quests;
			this.raids = quest_info.raids;
			this.banners = quest_info.banners;
			this.advents = quest_info.advents;
			this.raid_events = quest_info.raid_events;
			this.unions = quest_info.unions;
			this.story_events = quest_info.story_events;


			console.log("Now processing...");

			if (this.quest_info.has_unverified)
			{
				console.log('You have an unverified quest or raid!');
				let unfinished = (await queryKHAPI("apiABattles", "getUnverifiedList")).data;
				for (const open_quest of unfinished)
				{
					console.log('Get battle result for ' + open_quest.quest_type);
					let results = await queryKHAPI("apiABattles", "getBattleResult", open_quest.a_battle_id, open_quest.quest_type);
					for (const item of results.items_gained)
						console.log('Gained ' + item.amount + ' item: ' + item.name);
				}
			}
			else if (this.quest_info.in_progress.help_raid_ids.length > 0 && this.rejoinRaid)
			{
				console.log('You have an unfinished raid!');
				let battles = (await queryKHAPI("apiABattles", "getRaidRequestList")).data;
				let eventBattles = (await queryKHAPI("apiABattles", "getInSessionRaidEventList")).data;
				let raid = battles.concat(eventBattles).find((e) => { return e.is_joined || e.is_own_raid; });
				if ((raid == undefined || raid.a_battle_id == undefined) && this.unions.length > 0)
				{
					for (const enemy_type of ["Ultimate", "Expert"])
					{
						let union_request_list = (await queryKHAPI("apiABattles", "getUnionRaidRequestList", this.unions[0].event_id, enemy_type)).data;
						raid = union_request_list.find((e) => { return e.is_joined; });
						if (raid != undefined && raid.a_battle_id != undefined)
							break;
					}
				}
				if (raid == undefined || raid.a_battle_id == undefined)
					throw { message: "Fatal: Cannot find unfinished raid!", success: false };
				await this.advanceRaid({}, raid);
			}
			else if (this.quest_info.in_progress.own_quest != undefined)
			{
				let quest = this.quest_info.in_progress.own_quest;
				console.log('You have an unfinished quest!');
				let state = await queryKHAPI("apiAQuests", "getCurrentState", quest.a_quest_id, quest.type);
				await this.advanceState(state, quest);
			}


			// Execute quest selection
			let quests_do_first = this.questPriorityList.filter((e) => { return e.do_first; });
			for (const quest of quests_do_first)
			{
				let match = await this.matchQuest(quest);
				if (match == undefined || !this.questAvailable(match))
					continue;

				if (this.ap < match.quest_ap && quest.use_he != undefined && !quest.use_he)
				{
					continue;
				}
				else if (this.ap < match.quest_ap && (quest.use_he == undefined || quest.use_he) && !(await this.useElixir(match.quest_ap - this.ap)))
				{
					continue;
				}
				else
				{
					let info = await this.getPartyAndSummon(quest, match);
					await queryKHAPI("apiAParties", "changeDeck", info.party_id);
					let state = await queryKHAPI("apiAQuests", "startQuest", match.quest_id, match.type, info.party_id, info.summon_id,
						match.episode_num, match.starting_lap, match.ifRestart, info.summon_type);
					await this.advanceState(state, match);
				}
			}


			for (const raid of this.raidPriorityList)
			{
				let match = await this.matchRaid(raid);
				if (match == undefined)
					continue;

				if (this.bp < match.battle_bp)
				{
					console.log(this.bp + "/" + match.battle_bp);
					if (!(await this.useSeeds(match.battle_bp - this.bp)))
						continue;
				}

				let info = await this.getPartyAndSummon(raid, match);

				await queryKHAPI("apiAParties", "changeDeck", info.party_id);
				let state = await queryKHAPI("apiABattles", "joinBattle", match.a_battle_id, info.summon_id, info.party_id, match.quest_type, info.summon_type);
				await this.advanceRaid(state, match);
			}


			// Duplicate block of the first quest block
			for (const quest of this.questPriorityList.filter((e) => { return !e.do_first; }))
			{
				let match = await this.matchQuest(quest);
				if (match == undefined || !this.questAvailable(match))
					continue;

				if (this.ap < match.quest_ap && quest.use_he != undefined && !quest.use_he)
				{
					continue;
				}
				else if (this.ap < match.quest_ap && (quest.use_he == undefined || quest.use_he) && !(await this.useElixir(match.quest_ap - this.ap)))
				{
					continue;
				}
				else
				{
					let info = await this.getPartyAndSummon(quest, match);
					await queryKHAPI("apiAParties", "changeDeck", info.party_id);
					let state = await queryKHAPI("apiAQuests", "startQuest", match.quest_id, match.type, info.party_id, info.summon_id,
						match.episode_num, match.starting_lap, match.ifRestart, info.summon_type);
					await this.advanceState(state, match);
				}
			}

			console.log("Hit end of quests.");
			setTimeout(() => { this.run(); }, this.timeout);
		}
		catch (exception)
		{
			if (!exception.success)
			{
				if (!exception.battle_result_bypass)
					console.log("Caught an unexpected exception in KHAutoQuest:");
				if (exception.message != undefined)
					console.log(exception.message);
				else
					console.log(exception);
				console.log("Restarting KHAutoQuest::run...");
				setTimeout(() => { this.run(); }, this.timeout);
			}
			else
			{
				console.log(exception.message);
				if (this.coreBattle != null)
				{
					this.coreBattle.run();
				}
			}
		}
	}

	// Helper member functions
	async advanceRaid(state, raid)
	{
		let exception = {};
		if (state.cannot_progress_info != undefined)
			exception = { message: "Cannot join raid, error: " + state.cannot_progress_info.type, success: true };
		await (await kh.createInstance("router")).navigate("battle", raid);
		exception = { message: "Advancing raid...", success: true };
		throw exception;
	}
	async advanceState(current_state, quest)
	{
		console.log(current_state);
		let exception = { message: "Advancing state...", success: true };
		let next_info = current_state.next_info;
		switch (next_info.next_kind)
		{
			case "talk":
			case "harem-story":
				console.log("Next state is talk/harem-story.");
				let next_state = await queryKHAPI("apiAQuests", "getNextState", current_state.a_quest_id, quest.type);
				await this.advanceState(next_state, quest);
				break;
			case "battle":
				console.log("Next state is talk/harem-story.");
				await (await kh.createInstance("questStateManager")).restartQuest(current_state.a_quest_id, quest.type);
				break;
			case "battle_result":
				console.log("Next state is battle result.");
				let results = await queryKHAPI("apiABattles", "getBattleResult", next_info.id, quest.type);
				for (const item of results.items_gained)
					console.log('Gained ' + item.amount + ' item: ' + item.name);
				exception.battle_result_bypass = true;
				break;
			default:
				console.log(next_info);
				exception = { message: "Unknown state for next_info.next_kind!", success: false };
		}
		throw exception;
	}
	async getPartyAndSummon(quest, match)
	{
		let summon_type = 5;
		let party_id = this.player.selected_party.a_party_id;
		let party_index = isNaN(quest.party) ? party_map[quest.party] : quest.party;

		if (quest.party != undefined)
		{
			summon_type = this.decks[party_index].job.element_type;
		}
		else if (quest.element != undefined)
		{
			summon_type = elements[quest.element];
			if (summon_type == undefined) throw { message: "You misspelled this element: " + quest.element, success: false };
			let party_spec = this.parties[elements[summon_type]];
			party_index = isNaN(party_spec) ? party_map[party_spec] : party_spec;
		}
		else
		{
			// No element selected, get the recommended element out of the quest info.
			if (this.selectRecommended && (match.episodes != undefined && match.episodes[0].recommended_element_type))
				summon_type = match.episodes[0].recommended_element_type;
			// No element selected, get the recommended element out of the raid request info.
			else if (this.selectRecommended && quest.recommended_element_type != undefined)
				summon_type = match.recommended_element_type;
			// No element selected and selectRecommended is false, get the element of the currently selected party.
			else
				summon_type = this.player.selected_party.job.element_type;
			let party_spec = this.parties[elements[summon_type]];
			party_index = isNaN(party_spec) ? party_map[party_spec] : party_spec;
		}

		party_id = this.decks[party_index].a_party_id;
		console.log("Using " + elements[summon_type] + ".");

		// Look up all elements that you need to get the support lists for
		let support = { [summon_type]: (await queryKHAPI("apiASummons", "getSupporters", summon_type)).data };
		for (const item of this.summons.find((e) => { return e.element == elements[summon_type]; }).summonPriority)
		{
			let element_idx = item.element == undefined ? summon_type : elements[item.element];
			if (support[element_idx] == undefined)
				support[element_idx] = (await queryKHAPI("apiASummons", "getSupporters", element_idx)).data;

			let match = support[element_idx].find((e) =>
			{
				return e.summon_info.name == item.name && e.summon_info.level >= item.min_level;
			});
			if (match != undefined)
				return { party_id, summon_id: match.summon_info.a_summon_id, summon_type: element_idx };
		}
		// If nothing from the priority list was found, pick first of main element
		return { party_id, summon_id: support[summon_type][0].summon_info.a_summon_id, summon_type  };
	}
	async getPlayerInfo()
	{
		let questInfoPromise = queryKHAPI("apiAQuestInfo", "get");
		let playerPromise = queryKHAPI("apiAPlayers", "getMeNumeric");
		let questPointsPromise = queryKHAPI("apiAPlayers", "getQuestPoints");
		let partiesPromise = kh.createInstance("apiAParties");

		let quest_info = await questInfoPromise;
		let player = await playerPromise;
		let points = (await questPointsPromise).quest_points;

		let apiAParties = await partiesPromise;
		let list = (await apiAParties.getList()).body.a_party_list;
		let deckPromises = [];
		let parties = [];
		for (const e of list)
		{
			deckPromises.push(apiAParties.getSelectionDeck(e.a_party_id));
		}
		for (const p of deckPromises)
		{
			parties.push((await p).body.deck);
		}

		return { quest_info, player, points, parties };
	}
	async generateQuestList(categories)
	{
		let promises = [];
		let results = [];
		let ret = [];

		for (const c of categories)
		{
			promises.push(queryKHAPI("apiAQuests", "getQuestListByCategory", c));
		}
		for (let i = 0; i < promises.length; i++)
		{
			results.push((await promises[i])[categories[i] + "_quest_list"]);
		}
		for (const temp of results)
		{
			for (const t of Object.keys(temp))
			{
				ret = ret.concat(temp[t].data);
			}
		}
		return ret;
	}
	async generateRaidList()
	{
		let raids = (await queryKHAPI("apiAQuests", "getQuestListByCategory", "raid")).raid_quest_list.raid.data;
		let ret = {};

		let name_to_index =
		{
			"Fire :Fire ": elements.fire,
			"Ice :Water ": elements.water,
			"Wind :Wind ": elements.wind,
			"Lightning :Thunder ": elements.thunder,
			"Light :Light ": elements.light,
			"Darkness :Dark ": elements.dark,
			"Phantom :Phantom ": elements.phantom
		};
		for (const elem of Object.keys(name_to_index))
		{
			let func = (e) =>
			{
				try
				{
					let name = e.raid_info.enemy_name;
					let suffix = elem.split(":");
					return name.startsWith("Prison of " + suffix[0]) || name.startsWith("Guardian of " + suffix[1]);
				}
				catch (exception) { }
				return false;
			};
			ret[name_to_index[elem]] = { data: raids.filter(func) };
		}

		return ret;
	}
	async getQuests()
	{
		// categories include: material, elemental, accessory, raid, epic, guild_order, primal_conquest
		let sp_quests = await this.generateQuestList(["material", "elemental", "accessory"]);
		let raids = await this.generateRaidList();
		let banners = (await queryKHAPI("apiABanners", "getMypageBanners")).data;

		let advents = banners.filter((e) => { return e.event_type == "quest_event"; });
		let raid_events = banners.filter((e) => { return e.event_type === "raid_event"; });
		let unions = banners.filter((e) => { return e.event_type === "union_raid_event"; });
		let story_events = banners.filter((e) => { return e.event_type === "quest_story_event"; });

		return { sp_quests, raids, advents, raid_events, unions, story_events };
	}
	async matchQuest(quest)
	{
		if (quest.type == undefined)
			throw { message: "Please set a quest type!", success: false };

		console.log("Processing quest:");
		console.log(quest);

		// Filter down the quest list by the quest type
		let filtered_quests = [];
		switch (quest.type)
		{
			case "accessory":
				filtered_quests = this.sp_quests.filter((e) => { return e.type === "accessory"; });
				break;
			case "daily":
				filtered_quests = this.sp_quests.filter((e) => { return e.type === "daily"; });
				break;
			case "event":
				if (this.advents.length > 0)
					filtered_quests = (await queryKHAPI("apiAQuests", "getListEventQuest", this.advents[0].event_id)).data;
				else
					console.log("No advent event quests available.");
				break;
			case "gem":
			case "gem_daily":
				filtered_quests = this.sp_quests.filter((e) => { return e.type === "daily" && e.title.includes("Gem Quest"); });
				break;
			case "exp":
			case "exp_Sunday":
				filtered_quests = this.sp_quests.filter((e) => { return e.type === "daily" && e.title.includes("Cave of Training"); });
				break;
			case "event_raid":
				if (this.raid_events.length > 0)
					filtered_quests = (await queryKHAPI("apiAQuests", "getListEventQuest", this.raid_events[0].event_id)).data;
				else
					console.log("No raid event quests available.");
				break;
			case "disaster":
				if (quest.raid_element != undefined)
					filtered_quests = this.raids[elements[quest.raid_element]].data;
				else
					console.log("You need to set a raid_element for disaster raids!\n" + "Skipping quest.");
				break;
			case "lilim":
				if (this.unions.length > 0)
					filtered_quests = (await queryKHAPI("apiAQuests", "getListEventQuest", this.unions[0].event_id)).data;
				else
					console.log("Could not find union event quest!");
				break;
			case "story_event":
				if (this.story_events.length > 0)
					filtered_quests = (await queryKHAPI("apiAQuests", "getListEventQuest", this.story_events[0].event_id)).data;
				else
					console.log("Could not find story event quest!");
				break;
			case "free":
			case "main":
				return quest;
		}

		// Match the quest or return undefined
		if (filtered_quests.length == 0)
			return undefined;
		else if (quest.level != undefined)
		{
			let matches = filtered_quests.filter((e) => { return e.enemy_level == quest.level; });
			return matches[(quest.index != undefined && quest.index < matches.length) ? quest.index : 0];
		}
		else if (quest.ap != undefined)
		{
			let matches = filtered_quests.filter((e) => { return e.quest_ap == quest.ap; });
			return matches[(quest.index != undefined && quest.index < matches.length) ? quest.index : 0];
		}

		var highestAP = Math.max.apply(Math, filtered_quests.map((e) => { return e.quest_ap; }));
		return filtered_quests.find((e) => { return e.quest_ap === highestAP; });
	}
	async matchRaid(raid)
	{
		console.log("Prepare raid: ");
		console.log(raid);

		if (raid.type == undefined)
		{
			console.log("Please set a raid type!");
			return undefined;
		}

		// First check if you even have more BP than you set as minimum
		if (raid.min_bp != undefined && raid.min_bp > this.bp)
		{
			console.log(this.bp + " BP < " + raid.min_bp + " BP. Skipping raid.");
			return undefined;
		}

		let filtered_raids = [];
		switch (raid.type)
		{
			case "disaster":
				if (raid.raid_element === "phantom" && raid.ap === 0)
					filtered_raids.push({ "quest_ap": 0, "raid_info": { "enemy_name": "Guardian of Phantom Och", "enemy_level": 110 } });
				else
				{
					filtered_raids = this.raids[elements[raid.raid_element]].data;
				}
				break;
			case "event_raid":
				if (this.raid_events.length > 0)
				{
					filtered_raids = (await queryKHAPI("apiAQuests", "getListEventQuest", this.raid_events[0].event_id)).data;
				}
				break;
			case "lilim":
			case "ue_ultimate":
			case "ue_expert":
				if (this.unions.length > 0)
				{
					if (raid.type === "lilim")
					{
						filtered_raids = (await queryKHAPI("apiAQuests", "getListEventQuest", this.unions[0].event_id)).data;
						filtered_raids = filtered_raids.filter((e) => { return e.type === "event_union_lilim_raid"; });
					}
					else
					{
						let enemy_type = raid.type === "ue_ultimate" ? "Ultimate" : "Expert";
						let union_request_list = (await queryKHAPI("apiABattles", "getUnionRaidRequestList", this.unions[0].event_id, enemy_type)).data;

						if (raid.id != undefined && [1, 2, 3].includes(raid.id))
							return union_request_list[raid.id - 1];
						if (raid.not_last != undefined && raid.not_last)
							union_request_list.pop();

						let match = union_request_list.find((e) => {
							if (raid.min_hp != undefined && raid.min_hp > 100 * e.enemy_hp / e.enemy_max)
								return false;
							if (raid.max_participants != undefined && raid.max_participants < e.participants)
								return false;
							if (raid.min_participants != undefined && raid.min_participants > e.participants)
								return false;
							return true;
						});
						if (match != undefined)
							return match;

						console.log("No raid passed the filters, pick the one with lowest participants.");
						return union_request_list.sort((a, b) => {
							return (a.participants > b.participants) ? 1 : ((b.participants > a.participants) ? -1 : 0);
						})[0];
					}
				}
				break;
		}


		// Match the quest or return undefined
		if (raid.level != undefined)
			filtered_raids = filtered_raids.filter((e) => { return e.enemy_level == raid.level; });
		if (raid.ap != undefined)
			filtered_raids = filtered_raids.filter((e) => { return e.quest_ap == raid.ap; });
		if (filtered_raids.length == 0)
			return undefined;

		// Get all active raid requests
		let battles = (await queryKHAPI("apiABattles", "getRaidRequestList")).data;
		let eventBattles = (await queryKHAPI("apiABattles", "getInSessionRaidEventList")).data;
		battles = battles.concat(eventBattles).filter((e) =>
		{
			if (raid.union_only && !e.has_union_member)
				return false;
			if (raid.max_participants != undefined && raid.max_participants < e.participants)
				return false;
			if (raid.min_participants != undefined && raid.min_participants > e.participants)
				return false;
			if (raid.min_time != undefined)
			{
				let timeArray = e.time_left.split(':');
				let minutes = ((+timeArray[0]) * 3600 + (+timeArray[1]) * 60 + (+timeArray[2])) / 60;
				if (raid.min_time > minutes)
					return false;
			}
			if (raid.min_hp != undefined && raid.min_hp > 100 * e.enemy_hp / e.enemy_max)
				return false;
			for (const item of filtered_raids)
			{
				if (item.raid_info == undefined)
					continue;
				if (e.enemy_name == item.raid_info.enemy_name && e.enemy_level == item.raid_info.enemy_level)
					return true;
			}
			return false;
		});
		if (raid.priority != undefined)
		{
			switch (raid.priority)
			{
				case "union":
					let raids_with_union = battles.filter((e) => { return e.has_union_member; });
					if (raids_with_union.length !== 0)
						battles = raids_with_union;
					break;
				case "time":
					return battles.sort(function (a, b)
					{
						return (a.time_left < b.time_left) ? 1 : ((b.time_left < a.time_left) ? -1 : 0);
					})[0];
				case "participants":
					return battles.sort(function (a, b)
					{
						return (a.participants > b.participants) ? 1 : ((b.participants > a.participants) ? -1 : 0);
					})[0];
				case "hp":
					break;
			}
		}
		return battles.sort(function (a, b)
		{
			return ((a.enemy_hp / a.enemy_max) < (b.enemy_hp / b.enemy_max)) ? 1 : (((b.enemy_hp / b.enemy_max) < (a.enemy_hp / a.enemy_max)) ? -1 : 0);
		})[0];
	}
	async useElixir(amount)
	{
		let cure_items = (await queryKHAPI("apiAItems", "getCure", 1, 10)).data;
		let elixir = cure_items.find((e) => { return e.name === 'Half Elixir'; });

		let ap_restore = Math.ceil(this.max_ap / 2);
		let he_amount = Math.ceil(amount / ap_restore);
		if (elixir.a_item_id != undefined && elixir.num >= he_amount && elixir.num >= this.halfElixirLimit)
		{
			await queryKHAPI("apiAItems", "useItem", elixir.a_item_id, he_amount);

			let old_ap = this.ap;
			for (let i = 0; i < 10; i++)
			{
				let quest_points = await queryKHAPI("apiAPlayers", "getQuestPoints");
				this.ap = quest_points.ap;
				this.bp = quest_points.bp;
				if (this.ap != old_ap)
				{
					console.log(he_amount + ' elixirs used');
					return true;
				}
				else
					await sleep(1000);
			}

			console.log("Elixirs were used, but AP did not change!");
		}
		else
		{
			console.log("Too many elixirs used. Can't start quest.");
		}
		return false;
	}
	async useSeeds(amount)
	{
		let cure_items = (await queryKHAPI("apiAItems", "getCure", 1, 10)).data;
		let seed = cure_items.find((e) => { return e.name === 'Energy Seed'; });
		if (seed.a_item_id != undefined && seed.num >= this.seedLimit)
		{
			await queryKHAPI("apiAItems", "useItem", seed.a_item_id, amount);

			let old_bp = this.bp;
			for (let i = 0; i < 10; i++)
			{
				let quest_points = await queryKHAPI("apiAPlayers", "getQuestPoints");
				this.ap = quest_points.ap;
				this.bp = quest_points.bp;
				if (this.bp != old_bp)
				{
					console.log(amount + ' seeds used');
					return true;
				}
				else
					await sleep(1000);
			}

			console.log("Seeds were used, but BP did not change!");
		}
		else
		{
			console.log("Too many seeds used. Skipping raid.");
		}
		return false;
	}
	questAvailable(quest)
	{
		if ((quest.limit_info != undefined && quest.limit_info.remaining_challenge_count == 0) ||
			(quest.type === "accessory" && !this.quest_info.accessory_quest_remaining_challenge_count))
		{
			console.log("No entries left!");
			return false;
		}
		if (quest.required_item != undefined && quest.required_item.possession_amount < quest.required_item.amount)
		{
			console.log("Not enough materials!");
			return false;
		}
		return true;
	}
}


// Helper functions
async function queryKHAPI(api_parm, get_parm, ...args)
{
	return (await kh.createInstance(api_parm)[get_parm](...args)).body;
}
function sleep(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}