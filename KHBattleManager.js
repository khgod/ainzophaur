// =================================== START: Globals =================================== //
var abilitiesResistDownByName = [ //resist down skills/etc will be use first (case 2)
	"Libra Judge",
	"Vicissitudes of Fortune",
	"Trick Rook",
	"Gladius Burst",
	"Evil Eye",
	"King of Flies' Dignity",
	"Esthetic Set", //arian
	"Dead End",
	"Kaleidoscope Puff",
	"Weakning Spark", // Not misspelled
	"Gleasononia",
	"Moonbeam Arrow"
];
var abilitiesParalyzeByName = [
	"Blitz Donner",        // thor
	"Usha Ritz Tether",
	"Evil Eye's Curse",    // balor
	"Entrap Across",
	"Place Under",
	"Transfer Refill"
];
var abilitiesSoulBurstGaugeUp = {
	"Miracle Chalice": 100,
	"Roaring Blaze": 35,
	"Kamikaze Arrow": 30
};
var abilitiesTeamBurstGaugeUp = {
	"Encourage Inspiration": 20,
	"San Michel": 20,
	"Laced Brandy": 20,
	"Billowing Spout": 20,
	"Deal Energie": 20,
	"Adjustment Step": 10,
	"Dark Harvest": 15
};
var enemiesDisasterRagnarok = [
	"Prison of Light Catastrophe",
	"Prison of Wind Catastrophe",
	"Prison of Darkness Catastrophe",
	"Prison of Fire Catastrophe",
	"Prison of Lightning Catastrophe",
	"Prison of Ice Catastrophe"
];
// ==================================== END: Globals ==================================== //

class KHAbilityData
{
	constructor(abi_data, battle_status, parent)
	{
		// Set the owner of this ability
		this.parent = parent;

		this.origin = abi_data;
		this.index = abi_data._index;
		this.name = abi_data._abilityData.name;
		this.category = abi_data._abilityData.category;
		this.color = abi_data._abilityData.color;
		this.description = abi_data._abilityData.description;
		this.ready = abi_data._abilityData.ready;
		this.cooldown = abi_data._abilityData.turn;
		this.type = abi_data._abilityData.type; // This may be a key component to understanding

		this.usable = abi_data.isUsable();
		this.is_banned = abi_data._abilityData.is_banned;
		this.is_condition_satisfy = abi_data._abilityData.is_condition_satisfy;

		this.targetable = abi_data.isPartyMemberSelectable();
		this.target_type = abi_data.isPartyMemberSelectable() ? abi_data.getPartyMemberSelectableType() : null;

		this.remaining_cooldown = this.usable ? 0 : this.cooldown;
		if (isDefined(battle_status) && has(battle_status, "_abilityTurns", parent.index, this.index))
		{
			this.remaining_cooldown = battle_status._abilityTurns[parent.index][this.index];
		}

		if (this.name.startsWith("Innocent"))
			this.type = 24;

		let yellow = [
			"Roaring Blaze",
			"Thunder Bow Sange",
			"Thunder Bow Haja"
		];
		if (hasPrefix(this.name, yellow))
			this.color = "yellow";
	}
	isUsable()
	{
		return this.usable;
	}

	// Activate over the party
	async cast(target_index)
	{
		await this.origin._input.execute(target_index);
	}
}

class KHUnitData
{
	constructor(char_data, abi_data, status_list, battle_status)
	{
		if (!has(char_data, "index"))
			throw "KHUnitData could not construct object, unit is most likely dead.";

		this.origin = char_data;
		this.index = char_data.index;
		this.name = char_data.name;
		this.hp = char_data.hp;
		this.bg = char_data.burst;
		this.element = char_data.elementType;
		this.maxHp = char_data.maxHp;
		this.isSoul = char_data.isJob;
		this.isBurstBanned = char_data.isBurstBanned;
		this.hitSe = char_data.hitSe; // WTF is this?

		this.abilities = [];
		for (const abi of abi_data)
		{
			this.abilities.push(new KHAbilityData(abi, battle_status, this));
		}

		this.status_effects = {};
		for (const key of Object.keys(status_list))
		{
			let status = status_list[key];
			if (has(status, "_characters", this.index) && status._characters[this.index].length > 0)
				this.status_effects[status._id] = status;
		}
	}
	canBurst()
	{
		return this.bg == 100;
	}
	isLow()
	{
		return (this.isCriticallyLow(0.5) || this.getMissingHP() >= 4000);
	}
	isCriticallyLow(threshold)
	{
		return this.getPercentHP() < threshold;
	}
	isDebuffed()
	{
		let debuffs = [10, 11, 12, 16, 17, 18, 22, 25, 26, 54, 55, 56, 57, 58, 59,
			60, 61, 62, 63, 64, 65, 69, 89, 119, 120, 121, 122, 123, 124, 134, 213];
		return debuffs.find((e) => { return this.hasStatus(e); }) !== undefined;
	}
	canUse(abi_name)
	{
		let abi = this.abilities.find((e) => { return e.name.startsWith(abi_name); });
		if (abi !== undefined)
			return abi.isUsable();
		return false;
	}
	getRemainingCooldown(abi_name)
	{
		let abi = this.abilities.find((e) => { return e.name.startsWith(abi_name); });
		if (abi !== undefined)
			return abi.remaining_cooldown;
		return 99;
	}
	getSoulBurstGaugeUp()
	{
		// Check if abilities are banned for the next two turns
		if (this.getStatusDuration(65) >= 2)
			return 0;

		let abilityNames = Object.keys(abilitiesSoulBurstGaugeUp);
		let bg = 0;
		for (const abi of this.abilities)
		{
			if (abi.remaining_cooldown <= 2 && hasPrefix(abi.name, abilityNames))
			{
				bg += abilitiesSoulBurstGaugeUp[abi.name];
			}
		}
		return bg;
	}
	getTeamBurstGaugeUp()
	{
		// Check if abilities are banned for the next two turns
		if (this.getStatusDuration(65) > 2)
			return 0;

		let abilityNames = Object.keys(abilitiesTeamBurstGaugeUp);
		let bg = 0;
		for (const abi of this.abilities)
		{
			let key = abilityNames.find((e) => { return abi.name.startsWith(e); });
			if (abi.remaining_cooldown <= 2 && key !== undefined)
				bg += abilitiesTeamBurstGaugeUp[key];
		}
		return bg;
	}
	hasStatus(effect_id)
	{
		return this.getStatusDuration(effect_id) > 0;
	}
	getStatusDuration(effect_id)
	{
		if (has(this.status_effects, effect_id, "_characters", this.index)) {
			let status = this.status_effects[effect_id]._characters[this.index][0];
			if (has(status, "turn"))
				return status.turn;
			else
				return 1;
		}
		return 0;
	}
	getStatusLevel(effect_id)
	{
		if (has(this.status_effects, effect_id, "_characters", this.index))
		{
			let status = this.status_effects[effect_id]._characters[this.index][0];
			if (has(status, "level"))
				return status.level;
			else
				return 1;
		}
		return 0;
	}
	getPercentHP()
	{
		return this.hp / this.maxHp;
	}
	getMissingHP()
	{
		return this.maxHp - this.hp;
	}
}

class KHPartyData
{
	// Pass in kh.createInstance("battleWorld").{characterList, characterAbilityList, statusEffectList, battleStatus}
	constructor(char_list, down_list, abi_list, status_list, battle_status)
	{
		this.units = [];
		this.fallen = [];

		for (let i = 0; i < char_list.length; i++)
		{
			try
			{
				let u = new KHUnitData(char_list[i], abi_list[i], status_list, battle_status);
				this.units.push(u);
			}
			catch (err)
			{
				Logger.debug("Exception caught in KHPartyData: " + err);
			}
		}
		for (const unit of down_list)
		{
			this.fallen.push(unit.index);
		}
	}
	// Returns whether or not all active party members can burst
	canFullBurst()
	{
		// KNOWN BUG - May not work when there is only one member on the field
		let bg_base = 0;
		for (const unit of this.units)
		{
			let bg_unit = unit.bg;
			let bg_needed = 100;

			if (!unit.isBurstBanned && bg_unit + bg_base >= bg_needed)
			{
				bg_base += 10;
				if (unit.name == "Michael [Awakened]")
					bg_base += 20;
			}
			else
				return false;
		}
		return true;
	}
	// Returns whether provisional forest should be cast
	canPFBurst()
	{
		// Determine if the ability can be casted in the first place
		let soul = this.getSoul();
		if (soul === undefined || soul.bg < 50 || !soul.canUse("Provisional Forest"))
			return false;

		// Determine if a full burst can be executed two turns from now
		let bg_base = 20 + this.getTeamBurstGaugeUp();
		for (const unit of this.units)
		{
			let bg_unit = unit.bg;
			let bg_needed = 100;

			// Special Soul and burst up logic for provisional forest
			if (unit.isSoul)
			{
				bg_unit += unit.getSoulBurstGaugeUp() + 5;
				if (unit.getRemainingCooldown("Provisional Forest") == 0)
					bg_needed = 150;
			}

			if (!unit.isBurstBanned && bg_unit + bg_base >= bg_needed)
			{
				bg_base += 10;
				if (unit.name == "Michael [Awakened]")
					bg_base += 20;
			}
			else
				return false;
		}
		return true;
	}
	getAbilities()
	{
		let list = [];
		for (const unit of this.units)
		{
			list = list.concat(unit.abilities);
		}
		return list;
	}
	getAbilityByName(abi_name)
	{
		for (const unit of this.units)
		{
			let abi = unit.abilities.find((e) => { return e.name == abi_name; });
			if (abi !== undefined)
				return abi;
		}
		return undefined;
	}
	getSoul()
	{
		return this.units.find((e) => { return e.isSoul; });
	}
	getLowest()
	{
		let lowest_unit = this.units[0];
		for (let i = 1; i < this.units.length; i++)
		{
			if (this.units[i].getPercentHP() < lowest_unit.getPercentHP())
				lowest_unit = this.units[i];
		}
		return lowest_unit;
	}
	getTeamBurstGaugeUp()
	{
		let bg = 0;
		for (const unit of this.units)
			bg += unit.getTeamBurstGaugeUp();
		return bg;
	}

	getStatusLevel(effect_id)
	{
		let min_level = 0;
		for (let i = 0; i < this.units.length; i++)
		{
			const level = this.units[i].getStatusLevel(effect_id);
			if (i == 0 || level < min_level)
				min_level = level;
		}
		return min_level;
	}

	hasStatus(effect_id)
	{
		return this.units.filter((e) => { return e.hasStatus(effect_id); }).length
	}
	needHeal()
	{
		if (this.hasStatus(58) || this.hasStatus(59))
			return 0;
		return this.units.filter((e) => { return e.isLow(); }).length;
	}
	needCriticalHeal(threshold)
	{
		return this.units.filter((e) => { return e.isCriticallyLow(threshold); }).length;
	}
	needCleanse()
	{
		return this.units.filter((e) => { return e.isDebuffed(); }).length;
	}

	getAbilityTarget(ability)
	{
		if (!ability.targetable)
			return null;

		// Tishtrya bypass
		if (ability.name.startsWith("Fond in Part"))
		{
			for (const unit of this.units)
			{
				if (unit.hp != 0 && (!unit.hasStatus(5) || !unit.hasStatus(24) || !unit.hasStatus(139)))
					return unit;
			}
			return this.units.find((e) => { return e.hp != 0; });
		}
		else if (ability.name.startsWith("Tear Deviation"))
		{
			if (ability.parent.getPercentHP() <= 0.65)
				return ability.parent;
			return this.getLowest();
		}
		else if (ability.name.startsWith("Immunity Up"))
		{
			return this.getLowest();
		}


		if (ability.target_type == "revive" && this.fallen.length > 0)
		{
			Logger.debug("Looking for a dead character...");
			return this.fallen[0];
		}
		else if (ability.target_type == "heal")
		{
			let lowest_unit = this.getLowest();
			if (lowest_unit.getPercentHP() < 0.8)
				return lowest_unit;
		}
		else if (ability.target_type == "buff")
		{
			Logger.debug("Looking for a buff target...");
			let ret = this.units.find((e) => { return e.hp != 0; });
			if (isDefined(ret))
				return ret;
		}
		else
			Logger.debug("Unknown ability target type encountered: " + ability.target_type);

		Logger.debug("Returning null...");
		return null;
	}
	logBurstProfile()
	{
		for (let i = 0; i < this.units.length; i++)
		{
			Logger.info(i + ". " + this.units[i].name + ": " + this.units[i].bg);
		}
	}
}

class KHEnemyData
{
	constructor(enemy_data, bar_data)
	{
		if (!has(enemy_data, "index"))
		{
			throw "KHEnemyData -> enemy_data is undefined";
		}

		// Enemy data
		this.name = enemy_data.name;
		this.index = enemy_data.index;
		this.hp = enemy_data.hp
		this.maxHp = enemy_data.maxHp;
		this.id = enemy_data.id;
		this.isBoss = enemy_data.isBoss;
		this.currentMode = enemy_data.currentMode;
		this.hitSe = enemy_data.hitSe; // What is this????
		this.level = enemy_data._avatarData.level;

		// Bar data
		this.charge = bar_data._chargeTurnDotsActiveCount;
		this.maxCharge = bar_data._chargeTurnDotsCount;
		this.statusEffects = bar_data.getStatusEffects();
		this.gauge = bar_data._modeGaugeBase._visible;
	}

	// Returns whether or not this enemy can overdrive, disregarding stun
	overdriveReady()
	{
		return this.charge == this.maxCharge;
	}
	// Returns whether or not this enemy is raging
	isRaging()
	{
		return this.currentMode == "raging";
	}
	// Returns whether or not this enemy is stunned
	isStunned()
	{
		return this.currentMode == "stun";
	}
	// Returns whether or not this enemy is in normal gauge gain
	isNormal()
	{
		return !this.isRaging() && !this.isStunned();
	}
	// Returns whether or not this enemy is paralyzed within threshold
	isParalyzed()
	{
		return this.getStatusDuration(62);
	}
	getPercentHP() {
		return this.hp / this.maxHp;
	}
	// Returns the remaining effect duration of effect_id
	getStatusDuration(effect_id)
	{
		let effect = this.statusEffects.find((e) => { return e._id == effect_id; });
		if (effect !== undefined && has(effect, "_enemies", this.index))
		{
			//return effect._enemies[index][0].seconds.remaining[0].remain;
			return Date.now() - effect._enemies[this.index][0].seconds.timestamp;
		}
		return 0;
	}

	isStrong()
	{
		return (this.isBoss || this.hp >= 350000);
	}
	isBuffed()
	{
		var buffs = [5, 6, 7, 9, 13, 14, 15, 19, 21, 23, 24, 27, 35, 38, 75, 78, 84, 40003, 40001, 40005];
		return this.statusEffects.find((e) => { return buffs.indexOf(e._id) != -1; }) !== undefined;
	}
	hasStatus(effect_id)
	{
		return this.statusEffects.find((e) => { return e._id == effect_id; }) !== undefined;
	}
	hasGauge()
	{
		return this.gauge;
	}
	hasCharge()
	{
		return this.charge > 0;
	}
}

class KHBattleManager
{
	constructor()
	{
		// User raid parameters
		this.callForHelp = false;
		this.callAll = false;
		this.callFriends = true;
		this.callUnion = true;

		// User battle parameters
		this.alwaysNuke = true;
		this.alwaysBurst = false;
		this.burstRaging = false;
		this.skipSummons = false;
		this.skipAbilities = false;
		this.useProvisionalForest = true;

		// Operational parameters
		this.battleWorld = null;
		this.target_enemy = null;

		// Sleep parameters
		this.summon_delay_ms = 1000;
		this.ability_delay_ms = 1000;
		this.attack_delay_ms = 1000;
		this.target_delay_ms = 400;
		this.reload_delay_ms = 1800;
		this.idle_delay_ms = 20000;
		this.revive_delay_ms = 180000;
		this.timeout_min = 30;

		// Paratrain parameter
		this.paratrain = false;
		this.paratrain_attacks = 10;
		this.paratrain_avoid_overdrive = true;

		// Damage cut parameter
		this.lust_dmg_cut_lvl = 0;
		this.min_lust_lvl = 500;

		// Paraslave parameters
		this.paraslave = false;
		this.paraslave_attack_more = false; // Whether to attack as a paraslave when overdrive is not ready
		this.paraslave_avoid_overdrive = true; // Whether to avoid the overdrive at all costs
		this.para_delay_ms = 500;
		this.para_duration_sec = 4;

		// Set-up logger
		Logger.setLevel(Logger.INFO);
		Logger.setHandler((messages, context) =>
		{
			if (context.level >= Logger.getLevel())
				Function.prototype.apply.call(console.log, console, messages);
		});
	}

	// Main execution loop
	async run()
	{

		let start_time = Date.now();
		// Execute until all enemies are defeated
		for (let execution_count = 0; execution_count < 50; execution_count++)
		{
			try
			{
				// Escape for when enemies are all dead but execution loop cannot route out of quest
				if (await this.hasEnemies())
					execution_count = 0;
				// Final failsafe for when a quest has obviously gone on too long
				if (Date.now() - start_time > this.timeout_min * 60 * 1000)
				{
					Logger.error("Final timeout triggered, now escaping KHBattleManager::run() loop...");
					break;
				}

				await sleep(200);

				// Always end the battle if possible
				try
				{
					let finishBtn = cc.director.getRunningScene().seekWidgetByName("a_q_001_process_description_ok");
					if (isDefined(finishBtn))
					{
						Logger.info("KHBattleManager is now finishing battle...");
						this.pressButton(finishBtn);
					}
				}
				catch (err)
				{
					Logger.debug("KHBattleManager cannot end the battle: " + err + ". Now continuing...");
					await sleep(500);
					continue;
				}

				let vbtn = null;
				let turn = null;
				let btn_rescue = cc.director.getRunningScene().seekWidgetByName("btn_rescue");
				let help_enabled = isDefined(btn_rescue) && btn_rescue.enabled && btn_rescue.parent._visible;
				let participants = 0;

				// Call helpers doRescue or doCancel
				if (has(this.battleWorld, "raidInfo", "_raidInfo", "participants", "current")) {
					participants = this.battleWorld.raidInfo._raidInfo.participants.current;
					// Get the current time and escape if raid has failed
				}
				if (this.callForHelp && help_enabled && (await this.hasEnemies()) && this.enemies[0].level > 50) {
					await this.callHelp();
					await this.doOK();
					continue;
				}
				else {
					await this.doOK();
					await this.dismissHelp();
				}

				// Fill vbtn when available and not NONE, waiting up to 15 seconds
				vbtn = await this.getAvailableVButton(this.idle_delay_ms);
				// Resolve REVIVE and NEXT, reloading if NONE is encountered instead
				if (vbtn == 1)
				{
					Logger.info("Party is dead, breaking loop and then routing home...");
					await sleep(this.revive_delay_ms);
					break;
				}
				else if (vbtn == 2)
				{
					Logger.info("KHBattleManager is now advancing stage...");
					if (this.battleWorld.stage._currentStage >= this.battleWorld.stage._maxStage)
					{
						await this.doNext();
						await sleep(4000);
						break;
					}
					else
					{
						await this.doNext();
						await sleep(1500);
						continue;
					}
				}
				else if (vbtn != 0)
				{
					if (vbtn == 3)
						Logger.error("Error: visible button was unexpectedly NONE after timeout.");
					else
						Logger.error("Error: visible button yielded error code '" + vbtn + "' after timeout.");
					await this.reload();
					continue;
				}

				// Fill turn
				if (has(this.battleWorld, "turn", "turnNumber"))
					turn = this.battleWorld.turn.turnNumber;
				else
				{
					Logger.warn("Error: member 'turnNumber' of KHBattleWorld does not exist.");
					await sleep(500);
					break;
				}

				// Get potion from stamps
				if (!this.hasSuperPotion() && turn < 2)
				{
					let myStamps = cc.director.getRunningScene().seekWidgetByName("btn_stamp");
					if (isDefined(myStamps))
					{
						this.pressButton(myStamps);
						await sleep(500);
					}
				}
				let myStamp = cc.director.getRunningScene().seekWidgetByName("stamp_7");
				if (isDefined(myStamp))
				{
					this.pressButton(myStamp);
					await sleep(500);
				}

				// Set target to enemy if none
				if (await this.hasEnemies())
				{
					// Disaster targetting
					let rag = this.enemies.find((e) => { return e.level == 90 && hasPrefix(e.name, enemiesDisasterRagnarok); });
					if (rag !== undefined)
					{
						Logger.debug("Special ragnarok targetting enabled for " + rag.name);
						if (rag.name == "Prison of Fire Catastrophe") {
							if (this.battleWorld.getTarget() != rag.index) {
								await this.target(rag.index);
								await sleep(this.target_delay_ms);
								await this.update();
							}
						}
						else if (rag.name == "Prison of Ice Catastrophe") {
							if (this.enemies.length > 1 && this.battleWorld.getTarget() != this.enemies[1].index) {
								await this.target(this.enemies[1].index);
								await sleep(this.target_delay_ms);
								await this.update();
							}
						}
						else if (rag.name == "Prison of Wind Catastrophe") {
							if (this.enemies.length == 2 && this.enemies.find((e) => { return e.index == 2; }) !== undefined) {
								if (this.battleWorld.getTarget() != 2) {
									await this.target(2);
									await sleep(this.target_delay_ms);
									await this.update();
								}
							}
						}
						else if (rag.name == "Prison of Lightning Catastrophe") {
							if (this.battleWorld.getTarget() != this.enemies[0].index) {
								await this.target(this.enemies[0].index);
								await sleep(this.target_delay_ms);
								await this.update();
							}
						}
						else if (rag.name == "Prison of Light Catastrophe") {
							if (this.enemies.length == 2) {
								let idx = this.enemies[0].index == 1 ? 2 : 0;
								await this.target(idx);
								await sleep(this.target_delay_ms);
								await this.update();
							}
						}
						else if (rag.name == "Prison of Darkness Catastrophe") {
							let idx = rag.index;
							let adds = this.enemies.filter((e) => { return e.index != idx; });
							if (adds.length == 2)
								idx = adds[0].getPercentHP() >= adds[1].getPercentHP() ? adds[0].index : adds[1].index;
							else if (adds.length == 1)
								idx = adds[0].index;
							await this.target(idx);
							await sleep(this.target_delay_ms);
							await this.update();
						}
					}
				}
				// Default targeting when normal targeting fails
				for (let i = 0; i < 20; i++)
				{
					if (!await this.hasEnemies() || this.battleWorld.getTarget() != -1)
						break;

					Logger.debug("No target set. Now targeting first enemy...");
					Logger.debug(this.enemies);
					await this.target(this.enemies[0].index);
					await sleep(this.target_delay_ms);
					await this.update();
				}
				this.target_enemy = this.enemies.find((e) => { return e.index == this.battleWorld.getTarget(); });
				if (!isDefined(this.target_enemy))
				{
					Logger.warn("Target enemy was unexpectedly undefined. Now setting to first enemy...");
					this.target_enemy = this.enemies[0];
				}


				let quest_type = cc.director.getRunningScene().getQuestType();
				if (turn < 2)
					Logger.info("===== Quest Type: <" + quest_type + "> =====");
				if (cc.director.getRunningScene().getQuestType() === "guerrilla")
				{
					// skip all skills
					this.skipAbilities = true;
					this.skipSummons = true;
				}

				// Paraslave bypass
				if (this.paraslave)
				{
					// Switch burst off until full burst, then immediately reload
					let fb_ready = this.party.canFullBurst();
					if (fb_ready)
					{
						Logger.info("Full burst available, now enabling...");
						this.party.logBurstProfile();
						await this.toggleBurst(true);
					}
					else
						await this.toggleBurst(false);

					// Always try to do a debuff followed by a paralyze 
					await this.doAbility("debuff");
					let paraDone = await this.doAbility("para");
					if (paraDone)
					{
						Logger.debug("Paraslave ability casted. Now checking if paralyzed...");
						await this.parse_world();
					}

					if (this.target_enemy.isParalyzed() > this.para_duration_sec)
					{
						Logger.debug("Target is paralyzed, now attacking...");
						await this.doAttack();
						if (fb_ready)
							await this.reload();
						continue;
					}
					else if (paraDone)
					{
						await sleep(this.ability_delay_ms);
						continue;
					}
				}
				else if (this.paratrain)
				{
					if (this.target_enemy.isParalyzed() > this.para_duration_sec)
					{
						Logger.debug("Target is paralyzed, now attacking...");
						await this.doAttack();
						continue;
					}
				}

				// Whether to hold burst for raging enemies or not
				let holdRaging = this.burstRaging && this.target_enemy.hasGauge() && this.target_enemy.isNormal();
				// Provisional Forest bypass
				if (this.useProvisionalForest && this.party.canPFBurst() && !holdRaging)
				{
					Logger.debug("Provisional Forest usable within two turns.");
					await this.useAbility(this.party.getAbilityByName("Provisional Forest"));
					await sleep(this.ability_delay_ms);
					continue;
				}

				// Attempt to summon if possible
				if (await this.doSummon())
				{
					Logger.debug("Eidolon summoned.");
					await sleep(this.summon_delay_ms);
					continue;
				}

				// do heals
				if (await this.doAbility("green"))
				{
					Logger.debug("Heal cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// do buffs
				if (await this.doAbility("yellow"))
				{
					Logger.debug("Buff cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// do resist downs
				if (await this.doAbility("debuff"))
				{
					Logger.debug("Resist down cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// do debuffs
				if (await this.doAbility("blue"))
				{
					Logger.debug("Debuff cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// do damage
				if (await this.doAbility("red"))
				{
					Logger.debug("Nuke cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// do potion
				if (await this.usePotion())
				{
					Logger.debug("Potion cast.");
					await sleep(this.ability_delay_ms);
					continue;
				}

				// Switch burst off while enemy has gauge and not in rage
				let fb_ready = this.party.canFullBurst();
				if (holdRaging)
				{
					Logger.info("Enemy is not raging or stunned, disabling burst...");
					await this.toggleBurst(false);
				}
				else if (this.alwaysBurst || fb_ready)
				{
					if (fb_ready)
					{
						Logger.info("Full burst available, now enabling...");
						this.party.logBurstProfile();
					}
					await this.toggleBurst(true);
				}
				else
					await this.toggleBurst(false);

				// Can only reload when paraslaving and have nothing to do
				if (this.paraslave && !this.paraslave_attack_more && (!this.paraslave_avoid_overdrive || this.target_enemy.overdriveReady()))
				{
					await sleep(this.para_delay_ms);
					await this.reload();
					continue;
				}
				else if (this.lust_dmg_cut_lvl > 0 && this.enemies[0].name.startsWith("Lust") && this.enemies[0].level >= this.min_lust_lvl)
				{
					let dmg_cut_lvl = this.party.getStatusLevel(37) + this.party.getStatusLevel(115) + this.party.getStatusLevel(40017);
					if (dmg_cut_lvl < this.lust_dmg_cut_lvl)
					{
						Logger.debug("Damage cut level too low while battling 'Lust', looping back to top...");
						await sleep(5000);
						await this.reload();
						continue;
					}
				}
				else if (this.paratrain && (!this.paratrain_avoid_overdrive || this.target_enemy.overdriveReady()))
				{
					Logger.debug("Paratrain is to avoid overdrive, now reloading...");
					await sleep(this.para_delay_ms);
					await this.reload();
					continue;
				}

				// Attack
				await this.doAttack();
			}
			catch (err)
			{
				Logger.error("Exception caught at top level KHBattleManager::run(): " + err);
				await this.reload();
			}
		}

		// Route home when loop is broken
		Logger.info("Battle loop escaped, now routing home...");
		await this.routeHome();
	}

	// Updates KHBattleManager using the KHBattleWorld API
	async update()
	{
		let KHDefined = false;
		for (let i = 0; i < 10; i++)
		{
			try
			{
				// Query KHBattleWorld for latest information
				this.battleWorld = await kh.createInstance("battleWorld");
				KHDefined = true;
			}
			catch (err)
			{
				Logger.debug("Waiting for battle world to update: " + err);
				await sleep(200);
				continue;
			}

			if (KHDefined)
				break;
		}

		if (!KHDefined)
			throw "KHBattleManager::update() has timed out.";
	}
	async parse_world()
	{
		let char_list = null;
		let down_list = null;
		let abi_list = null;
		let status_list = null;
		let battle_status = null;

		let KHDefined = false;
		for (let i = 0; i < 20; i++)
		{
			try
			{
				// Query KHBattleWorld for latest information
				this.battleWorld = await kh.createInstance("battleWorld");
				char_list = this.battleWorld.characterList;
				down_list = this.battleWorld.fallenList;
				abi_list = this.battleWorld.characterAbilityList;
				status_list = this.battleWorld.statusEffectList;
				battle_status = this.battleWorld.battleStatus;
			}
			catch (err)
			{
				Logger.debug("Waiting for battle world to update: " + err);
				await sleep(200);
				continue;
			}

			try
			{
				this.party = new KHPartyData(char_list, down_list, abi_list, status_list, battle_status);
				KHDefined = true;
			}
			catch (err)
			{
				Logger.debug("Failed to construct KHPartyData: " + err);
				Logger.debug(this.battleWorld);
				await this.reload();
				await sleep(2000);
				continue;
			}

			if (KHDefined)
				break;
		}

		if (!KHDefined)
			throw "KHBattleManager::parse_world() has timed out.";

		// Construct enemy data
		this.enemies = [];
		for (let i = 0; i < this.battleWorld.enemyList.length; i++)
		{
			try
			{
				let e = new KHEnemyData(this.battleWorld.enemyList[i], this.battleWorld.enemyStatusBarList[i]);
				this.enemies.push(e);
			}
			catch (err)
			{
				Logger.trace("Caught exception while constructing enemy data: " + err);
				Logger.debug("Enemy " + i + " is probably dead.");
			}
		}
	}

	// Temporary 'do' functions
	async doOK()
	{
		var btn = cc.director.getRunningScene().seekWidgetByName("blue_btn");
		if (isDefined(btn))
		{
			this.pressButton(btn);
			await sleep(800);
		}
	}
	async doNext()
	{
		let btn = cc.director.getRunningScene().seekWidgetByName("btn_next");
		for (let i = 0; i < 10; i++)
		{
			if (isDefined(btn) && btn.enabled && btn._visible)
			{
				this.pressButton(btn);
				break;
			}
			else
			{
				await sleep(1000);
			}
		}
	}
	async doAttack()
	{
		Logger.debug("KHBattleManager is now attacking...");
		let btn = cc.director.getRunningScene().seekWidgetByName("btn_attack");

		// Paratrain bypass
		if (this.paratrain && this.target_enemy.isParalyzed() > this.para_duration_sec)
		{
			this.toggleBurst(false);
			for (let i = 0; i < this.paratrain_attacks / 2 + 0.5; i++)
			{
				if (isDefined(btn) && btn.enabled)
					this.pressButton(btn);
			}
			this.toggleBurst(true);
			for (let i = 0; i < this.paratrain_attacks / 2 + 0.5; i++)
			{
				if (isDefined(btn) && btn.enabled)
					this.pressButton(btn);
			}
			Logger.debug("KHBattleManager has finished spam attacking.");
			await this.reload();
			return;
		}

		for (let i = 0; i < 10; i++)
		{
			if (isDefined(btn) && btn.enabled && btn._visible)
			{
				this.pressButton(btn);
				await this.update();
			}
			else
			{
				break;
			}
		}
		Logger.debug("KHBattleManager has finished attacking.");
	}
	async doSummon()
	{
		if (this.skipSummons || this.paratrain)
		{
			Logger.debug("Skipping summons...");
			return false;
		}

		let eidolonsNotNeeded = ["Saint Nicholas", "Sphinx"];
		let ban = cc.director.getRunningScene().seekWidgetByName("banfilter_summon");
		let panelList = this.battleWorld.battleUI.SummonPanelGroup.panelList;
		let summonID = -1;
		if (isDefined(ban) && ban.visible) 
		{
			return false;
		}
		for (let i = 0; i < panelList.length; i++)
		{
			if (has(panelList, i, "turn") && has(panelList, i, "locked") && panelList[i].turn === 0 && !panelList[i].locked) 
			{
				let name = this.battleWorld.summonList[i].model.attributes.name;
				if (eidolonsNotNeeded.indexOf(name) == -1)
				{
					if (name == "Behemoth" && !this.party.needCleanse())
					{
						return false;
					}
					else if (name == "Anubis" && !this.target_enemy.hasCharge())
					{
						return false;
					}
					Logger.debug("Valid summon '" + name + "' found. Now casting...");
					summonID = i;
					break;
				}
			}
		}
		//  summon eidolon
		if (summonID > -1)
			return this.useSummon(summonID);
		return false;
	}
	async doAbility(color)
	{
		if (!this.paraslave && this.skipAbilities)
		{
			Logger.debug("Skipping abilities...");
			return false;
		}

		Logger.debug("Now searching for a " + color + " ability to cast...");
		let abi_list = this.party.getAbilities();

		// Special keyword bypasses
		if (color == "debuff")
		{
			let abi = abi_list.find((e) => { return e.isUsable() && hasPrefix(e.name, abilitiesResistDownByName); });
			if (abi !== undefined)
				return this.useAbility(abi);
			else
			{
				Logger.debug("No usable ability found.");
				return false;
			}
		}
		else if (color == "para")
		{
			let abi = abi_list.find((e) => { return e.isUsable() && hasPrefix(e.name, abilitiesParalyzeByName); });
			if (abi !== undefined)
				return this.useAbility(abi);
			else
			{
				Logger.debug("No usable ability found.");
				return false;
			}
		}

		// Normal execution
		abi_list = abi_list.filter((e) => { return e.isUsable() && e.color == color; });
		for (const abi of abi_list)
		{
			if (checkAbilityNeeded(abi, this.party, this.target_enemy, this.enemies, 
				this.alwaysNuke, this.min_lust_lvl, this.lust_dmg_cut_lvl))
			{
				return this.useAbility(abi);
			}
		}

		Logger.debug("No usable ability found.");
		return false;
	}

	// These functions modify the battle world, hence they immediately do a retrieve
	async attack()
	{
		Logger.debug("KHBattleManager is now attacking...");
		let btn = cc.director.getRunningScene().seekWidgetByName("btn_attack");
		if (isDefined(btn) && btn.enabled && btn._visible)
		{
			this.pressButton(btn);
			Logger.debug("KHBattleManager has finished attacking.");
		}
		else
		{
			Logger.debug("KHBattleManager could not attack.");
		}
	}
	async reload()
	{
		Logger.debug("KHBattleManager is now reloading...");
		try
		{
			await kh.createInstance("battleWorld").reloadBattle();
			if (!this.paratrain && !this.paraslave)
				await sleep(this.reload_delay_ms);
		}
		catch (err)
		{
			Logger.error("Failed to reload: ");
			Logger.error(err);
		}
	}
	async target(index)
	{
		Logger.debug("KHBattleManager is now targeting...");
		let enemy_hitbox = cc.director.getRunningScene().seekWidgetByName("enemy_hitbox_" + index);
		if (isDefined(enemy_hitbox))
		{
			await kh.createInstance("battleWorld").enemyStatusBarList[index]._targetEnemy(enemy_hitbox, 2);
			Logger.debug("Targeting success.");
		}
		else
		{
			Logger.debug("Targeting has failed. Hitbox does not exist.");
		}
	}
	async useAbility(abi)
	{
		Logger.debug("KHBattleManager is now casting '" + abi.name + "'...");
		try
		{
			let target = this.party.getAbilityTarget(abi);
			if (target != null)
			{
				Logger.debug("Casting '" + abi.name + "' on target " + target.name + "...");
				await abi.cast(target);
			}
			else
			{
				Logger.debug("Casting '" + abi.name + "' without a target...");
				await abi.cast();
			}
			return true;
		}
		catch (err)
		{
			Logger.debug("Exception thrown while using ability: " + abi.name + "': " + err);
		}
		return false;
	}
	async useSummon(ID)
	{
		let summon_ui_name = "battlecard_summon_ui_" + ID;
		let summon_ui = cc.director.getRunningScene().seekWidgetByName(summon_ui_name);
		if (summon_ui && summon_ui !== "null" && summon_ui !== "undefined")
		{
			summon_ui._children[0]._releaseUpEvent();
			var btn = cc.director.getRunningScene().seekWidgetByName("a_q_001_summongo_ok");
			if (btn && btn !== "null" && btn !== "undefined")
			{
				if (btn._visible)
				{
					this.pressButton(btn);
					return true;
				}
				else
				{
					var cancel = cc.director.getRunningScene().seekWidgetByName("a_q_001_summongo_cancel");
					this.pressButton(cancel);
					return false;
				}
			}
		}
		return false;
	}
	async usePotion()
	{
		if (this.hasSuperPotion() && this.party.needCriticalHeal(0.7) > 1)
		{
			this.battleWorld.useItem("cure-medic");
			return true;
		}
		else if (this.hasPotion() && this.party.needHeal())
		{
			let lowest = this.party.getLowest();
			if (lowest.getPercentHP() <= 0.55)
			{
				this.battleWorld.useItem("cure-bottle", lowest.index);
				return true;
			}
		}
		return false;
	}
	hasSuperPotion()
	{
		if (has(this.battleWorld, "battleStatus", "_cureItems", 1, "count"))
			return this.battleWorld.battleStatus._cureItems[1].count > 0;
		return false;
	}
	hasPotion()
	{
		if (has(this.battleWorld, "battleStatus", "_cureItems", 0, "count"))
			return this.battleWorld.battleStatus._cureItems[0].count > 0;
		return false;
	}
	// Calls allies for assistance
	async callHelp()
	{
		// let btn_id = cc.director.getRunningScene().seekWidgetByName("btn_generate_id");
		var btnAll = cc.director.getRunningScene().seekWidgetByName("checkbox_ui_all");
		var btnFriends = cc.director.getRunningScene().seekWidgetByName("checkbox_ui_friend");
		var btnUnion = cc.director.getRunningScene().seekWidgetByName("checkbox_ui_union");
		if (!this.callAll && isDefined(btnAll))
		{
			this.pressButton(btnAll.children[0]);
			await sleep(1000);
		}
		if (!this.callFriends && isDefined(btnFriends))
		{
			this.pressButton(btnFriends.children[0]);
			await sleep(1000);
		}
		if (!this.callUnion && isDefined(btnUnion))
		{
			this.pressButton(btnUnion.children[0]);
			await sleep(1000);
		}

		if (isDefined(btnFriends))
		{
			var btn_rescue = cc.director.getRunningScene().seekWidgetByName("btn_rescue");
			if (isDefined(btn_rescue) && btn_rescue.enabled && btn_rescue.parent._visible)
			{
				this.pressButton(btn_rescue);
				await sleep(2000);
			}
			else
			{
				await this.dismissHelp();
			}
		}
	}
	async dismissHelp()
	{
		var btn_rescue = cc.director.getRunningScene().seekWidgetByName("btn_rescue");
		if (isDefined(btn_rescue) && btn_rescue.enabled && btn_rescue.parent._visible)
		{
			var cancel_btn = btn_rescue.parent.seekWidgetByName("btn_cancel");
			if (isDefined(cancel_btn))
			{
				this.pressButton(cancel_btn);
				await sleep(800);
			}
		}
	}

	async toggleBurst(bool)
	{
		for (let i = 0; i < 10; i++)
		{
			if (this.battleWorld.battleUI.isFlgUseSpecialAttack() != bool)
			{
				this.pressButton(this.battleWorld.battleUI.BurstButton._widget);
				// Bypass the sleep to save time
				if (!this.paraslave && !this.paratrain)
					await sleep(200);
			}
			else
				break;
		}
	}
	async toggleAuto()
	{
		var btn = cc.director.getRunningScene().seekWidgetByName("btn_auto");
		if (btn && btn !== "null" && btn !== "undefined")
		{
			this.pressButton(btn);
		}
	}
	async useItem()
	{
		Logger.debug("KHBattleManager is now using an item...");
		Logger.debug(await kh.createInstance("battleWorld").useItem());
	}
	async routeHome()
	{
		await kh.createInstance("router").navigate("mypage");
		if (this.coreQuest != undefined)
			setTimeout((e) => { this.coreQuest.run(); }, 3000);
	}
	async hasEnemies()
	{
		await this.parse_world();
		return this.enemies.length > 0;
	}

	async getAvailableVButton(timeout)
	{
		let cycles = 20;
		for (let i = 0; i < cycles; i++)
		{
			this.update();
			if (has(this.battleWorld, "battleUI", "CenterPanel", "_visibleButton"))
			{
				let vbtn = this.battleWorld.battleUI.CenterPanel._visibleButton;
				if (vbtn == 0 || vbtn == 1 || vbtn == 2)
					return vbtn;
				else if (vbtn == 3)
					await sleep(timeout / cycles);
				else
				{
					Logger.debug("Warning: unexpected visible button value '" + vbtn + "' encounted while waiting.");
					return vbtn;
				}
			}
			else
				await sleep(timeout / cycles);
		}
		return -1;
	}
	async pressButton(btn)
	{
		btn._pushDownEvent();
		await sleep(this.attack_delay_ms / 2);
		btn._releaseUpEvent();
		await sleep(this.attack_delay_ms / 2);
	}

	// Logging
	setLogLevel(lvl)
	{
		Logger.setLevel(lvl);
	}
}

// FUTURE WORK - Asynchronous ability queuing
class KHActionQueue
{
	constructor()
	{
		this.readies = [];
		this.actions = [];
		this.running = false;
	}
	enqueue(ready, action)
	{
		this.readies.push(ready);
		this.actions.push(action);
	}
	async execute()
	{
		if (this.running)
			return false;

		this.running = true;
		while (!this.empty())
		{
			if ((this.readies[0])())
				(this.actions[0])();
			else
			{
				this.readies.shift();
				this.actions.shift();
			}
		}
		this.running = false;
		return true;
	}
	empty()
	{
		return this.actions.length == 0;
	}
	front()
	{
		if (this.isEmpty())
			return "undefined";
		return this.actions[0];
	}
	print()
	{
		let str = "";
		for (let i = 0; i < this.actions.length; i++)
			str += this.actions[i] + " ";
		return str;
	}
}

// Logs to console when debug is enabled
function has(obj)
{
	if (obj !== Object(obj))
	{
		return false;
	}
	for (let i = 1; i < arguments.length; i++)
	{
		let prop = arguments[i];
		if ((prop in obj) && obj[prop] !== null && obj[prop] !== undefined && obj[prop] !== 'undefined')
		{
			obj = obj[prop];
		}
		else
		{
			return false;
		}
	}
	return true;
}
function isDefined(obj)
{
	return (obj && obj !== null && obj !== undefined && obj !== 'undefined');
}
function sleep(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms));
}
function hasPrefix(name, abilities)
{
	return abilities.find((e) => { return name.startsWith(e); }) !== undefined;
}
function checkAbilityNeeded(abi, party, target, enemies, alwaysNuke, min_lust_lvl, lust_dmg_cut_lvl)
{
	var abilitiesNotNeededByName = [
		"Provisional Forest",
		"King of Flies",
		"Misty MoonLight",
		"Mega Therion",
		"Zombie Powder",
		"Charis Ring",
		"Abduction"
	];
	var abilitiesAlwaysUseByName = [
		"Sovereign Current",
		"Thunder Bolt Wall",
		"Ballet Religio",
		"Harvest Moon Gem",
		"Sacred Convict",
		"Cheer",
		"Immunity Up",
		"Revitalize",
		"Indomitable",
		"Hard Faculty",
		"Thunder Bow Sange",
		"Thunder Bow Haja"
	];
	var abilitiesNotNeeded = [];//55 - rampaging, 16 - buff with debuff (BUT THIS INCLUDES HITTING ENEMIES WITH A DEBUFF, e.g Marduk's Royal Tactics)
	var abilitiesBurstGaugeUp = [1, 41, 17];//41 - Burst Gauge↑ self, 17 - Burst Gauge↑ All, 1 - Refills Burst Gauge
	var abilitiesStackableByName = [
		"Golden Age",
		"Fury",
		"Meginjord",
		"Sun Spread",
		"Raizu and Fight",
		"Crimson Fury",
		"Crimson Eruption",
		"Nymph's Dance",
		"Adverse Wind",
		"Book of Raziel",
		"Overlimit",
		"Saint Breath"
	];
	var abilitiesForBossByName = ["Outrage", "Vicissitudes of Fortune", "Lovesick", "Lovely Concert", "Hero's Sword",
		"Mental Abberation", "Chaos Inferno", "Deep Attraction", "Aqua Drowning", "Death Swords", "Soul Reaper",
		"Wind of Lovesickness", "Immolation", "Blackout", "Love Perfume", "Evil Eye's Curse", "Black Shroud", "Putrify",
		"Chaotic Fog", "Walpurgis Night", "Cursed Undead", "Enchanting Harp", "To Catch a Thief", "Berserker", "Icicle Prison",
		"Striking Thunder Blast", "Blitz Donner", "Moonbeam Arrow", "Mutsuru Sakuya", "Admiration of Wei", "Particle Flash", "Sniper Shot"];//always use if enemy with gauge else do not use
	var abilitiesDebuffEnemy = [25, 53, 49, 45, 44, 34, 43, 50];//25 - DEF↓, 53 - ATK↓, 49 - ATK↓, 45 - Dizziness to an enemy, 34 - Chain attack rate↓, 43 - Enemy's max Overdrive Meter↑
	var abilitiesEnemyBuffedByName = ["Alfrodull", "Sugary Crush"];
	var abilitiesEnemyBuffed = [56];
	var abilitiesDuringStunByName = ["Finish Impact", "Current of Despair", "Dragon Blood", "Dragon Buster", "Epic of a Military Hero", "Paralyzer", "Curse+", "Enuma Elis", "Plasma Bind"];
	var abilitiesDuringStun = [39];//39 - Extends stun on enemy
	var abilitiesDuringRageByName = ["Hydro Burst", "Land of Ire", "Quell Riot"];
	var abilitiesDuringRage = [46, 35, 57];//46 - enemy's Mode Gauge↓, 35 - x DMG to raging enemies, 57 - Mode Gauge reduction
	var abilitiesReduceActiveDotByName = ["Otherworldy Call"];
	var abilitiesReduceActiveDot = [54];
	var abilitiesDefenceType1 = [3];
	var abilitiesDefenceType2 = [];
	var abilitiesDefenceType3 = [];
	var abilitiesAttackType1ByName = ["Valkyrie Assault"];
	var abilitiesAttackType1 = [15];
	var abilitiesAttackType2 = [];
	var abilitiesAttackType3 = [];
	var abilitiesAbilityDMGType = [32];
	var abilitiesDoubleAttackType1 = [5];//for All
	var abilitiesDoubleAttackType2 = [10];//single buff
	var abilitiesDoubleAttackType3 = [];
	var abilitiesHealAndRecoverByName = ["Sunlight Furnace", "Makina Regenerate", "Medical Check"];
	var abilitiesRecover = [19];
	var abilitiesPreventNegativeAffliction = [8];
	var abilitiesRevive = [23];
	var abilitiesHealAlly = [21];
	var abilitiesHealSelf = [24];
	var abilitiesRegeneration = [22];
	var abilitiesOnEnemySpecialAttack = [14, 11, 7, 6];//14 - Nullify, 11 - Damage or Res Cut, 7 - Cuts DMG, 6 - Reflect
	var abilitiesNotOnEnemySpecialAttack = [28];//28 - Intercept(Transfer)
	var abilitiesResetByName = ["Unit Charge", "Samildanach", "Hour of the Overlord"];
	var abilitiesTeamRegenByName = ["Orlean Call", "Eternal Gale"];

	// Yorimitsu bypass
	if (abi.name.startsWith("Selfless Soar"))
	{
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Yorimitsu");
		return !party.canFullBurst()
	}
	// Tishtrya bypass
	if (abi.name.startsWith("Fond in Part"))
	{
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Tishtrya");
		if (abi.parent.getPercentHP() > 0.3)
			return true;
		return false;
	}
	if (abi.name.startsWith("Tear Deviation"))
	{
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Tishtrya");
		if ((abi.parent.getPercentHP() <= 0.65) || party.needCriticalHeal(0.45))
			return true;
		return false;
	}
	if (abi.name.startsWith("Act Arbitration"))
	{
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Berith");
		return abi.parent.abilities.filter((e) => { return e.remaining_cooldown == 0; }).length == 1 && !party.canFullBurst();
	}
	if (abi.name.startsWith("Sunlight Furnace")) {
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Sol");
		if (abi.parent.canUse("Caldo Luce"))
			return false;
	}
	if (abi.name.startsWith("Medical Check")) {
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Dian Cecht");
		if (abi.parent.canUse("Immunity Up"))
			return false;
	}

	if (abi.name.startsWith("Revitalize")) {
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Azazel");
		if (!abi.parent.hasStatus(278)) {
			Logger.debug("Azazel does not have status 278, not casting...");
			return false;
		}
		else {
			Logger.debug("Azazel does have status 278, now casting...");
			return true;
		}
	}
	if (abi.name.startsWith("Storm Zeal")) {
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Azazel");
		if (!abi.parent.hasStatus(278) || abi.parent.canUse("Revitalize"))
			return false;
	}
	if (abi.name.startsWith("Mother Earth++")) {
		Logger.debug("Matching hit: " + abi.name + " -> Special bypass for Gaia");
		return target.overdriveReady();
	}

	if (hasPrefix(abi.name, abilitiesTeamRegenByName)) {
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesTeamRegenByName");
		return party.getStatusLevel(35) == 0;
	}

	if (hasPrefix(abi.name, Object.keys(abilitiesSoulBurstGaugeUp)))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesSoulBurstGaugeUp");

		let soul = abi.parent;
		if (soul.bg == 100)
			return false;

		if (abi.name.startsWith("Roaring Blaze") && soul.bg <= 55)
			return true;

		let pf_cd = soul.getRemainingCooldown("Provisional Forest");
		if (pf_cd > 6 || pf_cd - abi.cooldown > 0 || abi.cooldown - pf_cd < 3)
			return true;
		return false;
	}
	if (hasPrefix(abi.name, Object.keys(abilitiesTeamBurstGaugeUp)))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesTeamBurstGaugeUp");
		return !party.canFullBurst();
	}
	if (hasPrefix(abi.name, abilitiesNotNeededByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesNotNeededByName");
		return false;
	}
	if (hasPrefix(abi.name, abilitiesAlwaysUseByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesAlwaysUseByName");
		return true;
	}
	if (abilitiesNotNeeded.indexOf(abi.type) > -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesNotNeeded");
		return false;
	}
	if (abilitiesBurstGaugeUp.indexOf(abi.type) > -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesBurstGaugeUp");
		if (abi.type == 17)
			return !party.canFullBurst();
		return !party.canFullBurst() && abi.parent.bg < 100;
	}
	if (hasPrefix(abi.name, abilitiesStackableByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesStackableByName");
		return true;
	}

	if (hasPrefix(abi.name, abilitiesResetByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesResetByName");
		return abi.parent.abilities.filter((e) => { return e.remaining_cooldown == 0; }).length == 1;
	}
	if (hasPrefix(abi.name, abilitiesHealAndRecoverByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesHealAndRecoverByName");
		return party.needCleanse() || party.needHeal();
	}
	if (abilitiesHealSelf.indexOf(abi.type) > -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesHealSelf");
		return abi.parent.isLow();
	}
	if (abi.color == "green" && abilitiesRecover.indexOf(abi.type) && abilitiesRevive.indexOf(abi.type) === -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesHeal");
		return party.needHeal();
	}
	if (abilitiesRecover.indexOf(abi.type) > -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesRecover");
		return party.needCleanse();
	}
	if (abilitiesRevive.indexOf(abi.type) > -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesRevive");
		return party.fallen.length > 0;
	}

	if (hasPrefix(abi.name, abilitiesReduceActiveDotByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesReduceActiveDotByName");
		return (target.hasGauge() || target.isStrong()) && target.hasCharge();
	}
	if (hasPrefix(abi.name, abilitiesForBossByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesForBossByName");
		return target.hasGauge() || target.isStrong();
	}
	if (hasPrefix(abi.name, abilitiesEnemyBuffedByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesEnemyBuffedByName");
		return target.isBuffed();
	}
	if (hasPrefix(abi.name, abilitiesDuringRageByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDuringRageByName");
		return target.isRaging() || (abi.color === "red" && target.isStrong());
	}
	if (hasPrefix(abi.name, abilitiesDuringStunByName))
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDuringStunByName");
		return target.isStunned() || (abi.color === "red" && target.isStrong());
	}

	if (abilitiesDebuffEnemy.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDebuffEnemy");
		return target.hasGauge() || target.isStrong();
	}
	if (abilitiesDuringStun.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDuringStun");
		return target.isStunned() || (abi.color === "red" && target.isStrong());
	}
	if (abilitiesDuringRage.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDuringRage");
		return target.isRaging() || (abi.color === "red" && target.isStrong());
	}
	if (abilitiesEnemyBuffed.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesEnemyBuffed");
		return target.isBuffed();
	}
	if (abi.type == 55)
	{
		return !party.canFullBurst() && abi.parent.bg < 100 - abi.parent.index * 10;
	}
	if (abilitiesReduceActiveDot.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesReduceActiveDot");
		return (target.hasGauge() || target.isStrong()) && target.hasCharge();
	}
	if (abilitiesOnEnemySpecialAttack.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesOnEnemySpecialAttack");

		// Lust
		let lust = enemies.find((e) => { return e.name.startsWith("Lust") && e.level >= min_lust_lvl; });
		if (lust !== undefined)
		{
			let dmg_cut_lvl = party.getStatusLevel(37) + party.getStatusLevel(41) + party.getStatusLevel(115) + party.getStatusLevel(40017);
			if (dmg_cut_lvl < lust_dmg_cut_lvl)
				return true;
		}

		// Trag
		let trag = enemies.find((e) => { return e.level == 90 && e.name.startsWith("Prison of Lightning Catastrophe"); });
		if (trag !== undefined)
		{
			if (party.getStatusLevel(37) > 1 && party.needHeal() < 2)
				return false;
			if (trag.isStunned())
				return false;
			if (trag.overdriveReady())
				return true;
			if (enemies.length > 1 && !enemies[0].isStunned() && trag.maxCharge - trag.charge <= 1)
				return true;
			return false;
		}

		// Lrag
		let lrag = enemies.find((e) => { return e.level == 90 && e.name.startsWith("Prison of Light Catastrophe"); });
		if (lrag !== undefined)
		{
			if (party.getStatusLevel(37) > 2)
				return false;
			if (lrag.isStunned())
				return false;
			if (lrag.overdriveReady())
				return true;
			return false;
		}

		// Seraph Thunder and Phul
		let seraph = enemies.find((e) => { return e.name.startsWith("Seraph Kindness") || e.name.startsWith("Guardian of Thunder Phul"); });
		if (seraph !== undefined && target.overdriveReady())
		{
			// Let gaia block this shit
			let gaia = party.units.find((e) => { return e.name.startsWith("Gaia"); });
			if (gaia !== undefined && (gaia.canUse("Mother Earth++") || gaia.getStatusDuration(71) > 0))
				return target.hasGauge() && target.isRaging();

			if (abi.name.startsWith("Voice of the Gods") && gaia.canUse("Genesis"))
				return false;

			let dmg_cut_lvl = party.getStatusLevel(37) + party.getStatusLevel(41) + party.getStatusLevel(116) + party.getStatusLevel(40018);
			if (dmg_cut_lvl <= 2)
				return true;

			return false;
		}

		return ((target.hasGauge() && !target.isStunned()) || target.isStrong()) && target.overdriveReady();
	}
	if (abilitiesNotOnEnemySpecialAttack.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesNotOnEnemySpecialAttack");
		return !(target.hasGauge() && !target.isStunned() && target.overdriveReady());
	}
	if (abilitiesAttackType1.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesAttackType1");
		return !party.hasStatus(6);
	}
	if (abilitiesAttackType2.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesAttackType2");
		return !party.hasStatus(7);
	}
	if (abilitiesAttackType3.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesAttackType3");
		return !party.hasStatus(40001);
	}
	if (abilitiesDefenceType1.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDefenceType1");
		return !party.hasStatus(14);
	}
	if (abilitiesDefenceType2.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDefenceType2");
		return !party.hasStatus(15);
	}
	if (abilitiesDefenceType3.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDefenceType3");
		return !party.hasStatus(40003);
	}
	if (abilitiesAbilityDMGType.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesAbilityDMGType");
		return !party.hasStatus(19);
	}
	if (abilitiesDoubleAttackType1.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDoubleAttackType1");
		return !party.hasStatus(23);
	}
	if (abilitiesDoubleAttackType3.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesDoubleAttackType3");
		return !party.hasStatus(40005);
	}
	if (abilitiesPreventNegativeAffliction.indexOf(abi.type) != -1)
	{
		Logger.debug("Matching hit: " + abi.name + " -> abilitiesPreventNegativeAffliction");
		return !party.hasStatus(47);
	}
	if (abi.color === "red" && !alwaysNuke)
	{
		Logger.debug("Matching hit: " + abi.name + " -> alwaysNuke");
		return !target.hasGauge() || target.isRaging() || target.isStunned();
	}

	Logger.debug("No matching hit, casting '" + abi.name + "'...");
	return true;
}
