﻿using System.Collections.Generic;

namespace SpreadsheetConnector
{
    public interface IData
    {
        List<object> GetData();
    }
}
