﻿using System;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace SpreadsheetConnector
{
    public class GoogleSpreadsheet
    {
        private const string CredentialsPath = "credentials.json";
        private const string AppName = "Parse Memberdata";
        private readonly string _spreadsheetId;
        private readonly int _gridId;
        private static readonly string[] Scope = {SheetsService.Scope.Spreadsheets};
        private UserCredential _credentials;
        private SheetsService _service;

        public GoogleSpreadsheet(string spreadsheetId, int gridId)
        {
            _spreadsheetId = spreadsheetId;
            _gridId = gridId;
            GetCredentials();
        }
       
        private void GetCredentials()
        {
            using (var stream = new FileStream(CredentialsPath, FileMode.Open, FileAccess.Read))
            {
                _credentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scope,
                    "user",
                    CancellationToken.None
                ).Result;
            }

            _service = new SheetsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = _credentials,
                ApplicationName = AppName
            });
        }

        public IList<IList<object>> GetData(string dataArea)
        {
            var request = _service.Spreadsheets.Values.Get(_spreadsheetId, dataArea);
            return request.Execute().Values;
        }

        public void SetData<T>(List<T> datas, int startingRow, int startingColumn) where T : IData
        {
            var requests = new List<Request>();
            for (var i = 0; i < datas.Count; i++)
            {
                var values = new List<CellData>();
                var data = datas[i].GetData();
                
                foreach (var entry in data)
                {
                    var number = entry.IsNumber() ? (double?)Convert.ToDouble(entry) : null;
                    values.Add(new CellData
                    {
                        UserEnteredValue = new ExtendedValue
                        {
                            BoolValue = entry as bool?,
                            NumberValue = number,
                            StringValue = entry as string
                        }
                    });
                }

                requests.Add(new Request
                    {
                        UpdateCells = new UpdateCellsRequest
                        {
                            Start = new GridCoordinate
                            {
                                SheetId = _gridId,
                                RowIndex = i + startingRow,
                                ColumnIndex = startingColumn
                            },
                            Rows = new List<RowData> { new RowData { Values = values } },
                            Fields = "userEnteredValue"
                        }
                    }
                );
            }

            var batchRequest = new BatchUpdateSpreadsheetRequest
            {
                Requests = requests
            };

            _service.Spreadsheets.BatchUpdate(batchRequest, _spreadsheetId).Execute();
        }
    }
}