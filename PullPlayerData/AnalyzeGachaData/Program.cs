﻿using System;

namespace AnalyzeGachaData
{
    internal class Program
    {
        private static void Main()
        {
            var analyzer = new Analyzer();
            analyzer.SortAllPulls();
            analyzer.PublishAll();

            Console.WriteLine("Finished adding the Data!");
            Console.ReadLine();
        }
    }
}
