﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AnalyzeGachaData
{
    internal class Analyzer
    {
        private readonly Spreadsheet _spreadsheet;
        public List<TenPullData> TenPulls { get; }
        public List<SinglePullData> SinglePulls { get; }
        public List<PullData> SortedPulls { get; private set; } = new List<PullData>();
        
        public Analyzer()
        {
            _spreadsheet = new Spreadsheet();
            TenPulls = _spreadsheet.GetTenPullData();
            SinglePulls = _spreadsheet.GetSinglePullData();
        }

        public void SortAllPulls()
        {
            var singlePulls = SinglePulls.Clone().ToList();
            var tenPulls = TenPulls.Clone().ToList();
            SortedPulls = SortPulls(tenPulls, SortPulls(singlePulls));
        }

        public List<PullData> SortPulls<T>(List<T> data, List<PullData> previousPulls = null) where T : PullData
        {
            previousPulls = previousPulls ?? new List<PullData>();
            var sortedPulls = new List<PullData>(previousPulls);

            foreach (var pull in data)
            {
                var existingPulls = sortedPulls.Where(p => p.Time == pull.Time).ToList();
                if (existingPulls.Count != 0)
                {
                    var matchingPull = existingPulls.FirstOrDefault(p => p.Type == pull.Type);

                    if (matchingPull != null)
                    {
                        matchingPull.Add(pull);
                        continue;
                    }
                }
                sortedPulls.Add(pull);
            }
            sortedPulls.Sort();
            return sortedPulls;
        }

        private List<TypeStats> ComputeStats()
        {
            var totalPulls = new List<PullData>();
            totalPulls.AddRange(TenPulls);
            totalPulls.AddRange(SinglePulls);
            
            if (totalPulls.Count == 0) return null;

            var types = GetTypes(totalPulls);
            var typeStats = new List<TypeStats>();

            foreach (var type in types)
            {
                var pulls = totalPulls.Where(p => p.Type == type && !p.OutOfContext).ToList();
                if (pulls.Count == 0) continue;

                var stats = new TypeStats(pulls[0].Type);
                foreach (var pull in pulls)
                {
                    if (pull.Type == "SSR Guaranteed")
                    {
                        pull.Ssr--;
                    }
                    stats.Add(pull);
                }
                typeStats.Add(stats);
            }
            return typeStats;
        }

        private static IEnumerable<string> GetTypes(IEnumerable<PullData> pulls)
        {

            var types = new List<string>();
            foreach (var pull in pulls)
            {
                if (!types.Contains(pull.Type))
                {
                    types.Add(pull.Type);
                }
            }
            return types;
        }
        
        private List<PullData> GroupAll()
        {
            var groupedPulls = new List<PullData>(SortedPulls);
            foreach (var pull in groupedPulls)
            {
                pull.Type = "All";
            }
            return SortPulls(groupedPulls);
        }

        public void PublishAll()
        {
            _spreadsheet.SetPullData(_spreadsheet.JewelTab, SortedPulls.Where(p => p.Type == "Jewel").ToList());
            _spreadsheet.SetPullData(_spreadsheet.NewCharTab, SortedPulls.Where(p => p.Type == "New Character").ToList());
            _spreadsheet.SetPullData(_spreadsheet.PremiumTab, SortedPulls.Where(p => p.Type == "Premium").ToList());
            _spreadsheet.SetPullData(_spreadsheet.KhTicketTab, SortedPulls.Where(p => p.Type == "KH Ticket").ToList());
            _spreadsheet.SetPullData(_spreadsheet.SsrGuaranteed, SortedPulls.Where(p => p.Type == "SSR Guaranteed").ToList());
            
            _spreadsheet.SetStats(ComputeStats());

            _spreadsheet.SetPullData(_spreadsheet.AllTab, GroupAll());
        }
    }

    internal static class Helper
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
}
