﻿using System;
using System.Collections.Generic;
using SpreadsheetConnector;

namespace AnalyzeGachaData
{
    internal class PullData : IData, IComparable, ICloneable
    {
        public string Type { get; set; }
        public string Time { get; }
        public int Ssr { get; set; }
        public int Sr { get; protected set; }
        public int R { get; protected set; }
        public int IsBad { get; protected set; }
        public int SingleSsr { get; protected set; }
        public int SingleSr { get; protected set; }
        public int SingleR { get; protected set; }
        public bool OutOfContext { get; }

        protected int TotalTenPulls { get; set; }

        protected PullData(string type, string time, bool outOfContext)
        {
            Type = type == "SSR Guaranteed" || type == "SSR Eido Guaranteed" || type == "SSR KH Guaranteed" ? "SSR Guaranteed" : type;
            Time = time;
            OutOfContext = outOfContext;
        }
        
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            if (obj is PullData otherPull)
            {
                return string.Compare(Time, otherPull.Time, StringComparison.Ordinal);
            }
            throw new ArgumentException("Object isn't a TenPullData");
        }

        public List<object> GetData()
        {
            decimal totalPulls = Ssr + Sr + R + SingleR + SingleSr + SingleSsr;
            return new List<object>
            {
                Time,
                Type,
                TotalTenPulls,
                Ssr,
                Sr,
                R,
                IsBad,
                SingleSsr,
                SingleSr,
                SingleR,
                decimal.Round((Ssr + SingleSsr) * 100 / totalPulls, 2, MidpointRounding.AwayFromZero),
                decimal.Round((Sr + SingleSr) * 100 / totalPulls, 2, MidpointRounding.AwayFromZero),
                decimal.Round((R + SingleR) * 100 / totalPulls, 2, MidpointRounding.AwayFromZero)
            };
        }

        public void Add(PullData data)
        {
            if (data.GetType() == typeof(TenPullData))
            {
                Add((TenPullData)data);
            }
            else
            {
                Add((SinglePullData)data);
            }
        }

        public void Add(TenPullData data)
        {
            Ssr += data.Ssr;
            Sr += data.Sr;
            R += data.R;
            IsBad += data.IsBad;
            TotalTenPulls++;
        }

        public void Add(SinglePullData data)
        {
            SingleSsr += data.Ssr;
            SingleSr += data.Sr;
            SingleR += data.R;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
