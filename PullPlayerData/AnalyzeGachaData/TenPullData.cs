﻿namespace AnalyzeGachaData
{
    internal class TenPullData : PullData
    {
        public TenPullData(string type, string time, int ssr, int sr, int r, int isBad, bool outOfContext) : base(type, time, outOfContext)
        {
            Ssr = ssr;
            Sr = sr;
            R = r;
            IsBad = isBad;
            TotalTenPulls = 1;
        }
    }
}