﻿using System;
using System.Collections.Generic;
using SpreadsheetConnector;

namespace AnalyzeGachaData
{
    internal class TypeStats : IData
    {
        private readonly string _type;
        private int _ssr;
        private int _sr;
        private int _r;

        public TypeStats(string type)
        {
            _type = type;
        }

        public List<object> GetData()
        {
            decimal totalPulls = _ssr + _sr + _r;
            return new List<object>
            {
                _type,
                (int) totalPulls,
                _ssr,
                _sr,
                _r,
                decimal.Round(_ssr * 100 / totalPulls, 2, MidpointRounding.AwayFromZero),
                decimal.Round(_sr * 100 / totalPulls, 2, MidpointRounding.AwayFromZero),
                decimal.Round(_r * 100 / totalPulls, 2, MidpointRounding.AwayFromZero)
            };
        }

        public void Add(PullData data)
        {
            _ssr += data.Ssr + data.SingleSsr;
            _sr += data.Sr + data.SingleSr;
            _r += data.R + data.SingleR;
        }
    }
}
