﻿namespace AnalyzeGachaData
{
    internal class SinglePullData : PullData
    {
        internal SinglePullData(string type, string time, int ssr, int sr, int r, bool outOfContext) : base(type, time, outOfContext)
        {
            SingleSsr = ssr;
            SingleSr = sr;
            SingleR = r;
        }
    }
}