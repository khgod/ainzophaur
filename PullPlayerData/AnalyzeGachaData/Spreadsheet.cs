﻿using SpreadsheetConnector;
using System.Collections.Generic;
using System.Linq;

namespace AnalyzeGachaData
{
    internal class Spreadsheet
    {
        private const string SpreadsheetId = "1AdeEEo4LK1VljkPn74Gs_EBsleQnY0gHpV6xRSSUU-o";
        private readonly GoogleSpreadsheet _dataSpreadsheet = new GoogleSpreadsheet(SpreadsheetId, 0);
        public GoogleSpreadsheet AllTab { get; } = new GoogleSpreadsheet(SpreadsheetId, 789593064);
        public GoogleSpreadsheet JewelTab { get; } = new GoogleSpreadsheet(SpreadsheetId, 1117246061);
        public GoogleSpreadsheet NewCharTab { get; } = new GoogleSpreadsheet(SpreadsheetId, 1468133502);
        public GoogleSpreadsheet PremiumTab { get; } = new GoogleSpreadsheet(SpreadsheetId, 265597509);
        public GoogleSpreadsheet KhTicketTab { get; } = new GoogleSpreadsheet(SpreadsheetId, 930246455);
        public GoogleSpreadsheet SsrGuaranteed { get; } = new GoogleSpreadsheet(SpreadsheetId, 170579199);
        private readonly GoogleSpreadsheet _statsSpreadsheet = new GoogleSpreadsheet(SpreadsheetId, 1084929370);

        public List<TenPullData> GetTenPullData()
        {
            var values = _dataSpreadsheet.GetData("Data!A2:G1000");
            if (values == null || values.Count <= 0) return null;

            return (
                from row in values
                where 
                    row.Count>3 && 
                    row[0] is string && !string.IsNullOrEmpty(row[0].ToString()) &&
                    row[1] is string && !string.IsNullOrEmpty(row[1].ToString())
                select new TenPullData(
                    row[0].ToString(),
                    row[1].ToString(),
                    string.IsNullOrWhiteSpace(row[2].ToString()) ? 0 : int.Parse(row[2].ToString()),
                    row.Count >= 4 ? string.IsNullOrWhiteSpace(row[3].ToString()) ? 0 : int.Parse(row[3].ToString()) : 0,
                    row.Count >= 5 ? string.IsNullOrWhiteSpace(row[4].ToString()) ? 0 : int.Parse(row[4].ToString()) : 0,
                    row.Count >= 6 ? int.Parse(row[5].ToString()) : 0,
                    row.Count == 7
                )).ToList();
        }

        public List<SinglePullData> GetSinglePullData()
        {
            var values = _dataSpreadsheet.GetData("Data!I2:N1000");
            if (values == null || values.Count <= 0) return null;

            return (
                from row in values
                where
                    row.Count > 3 && 
                    row[0] is string && !string.IsNullOrEmpty(row[0].ToString()) && 
                    row[1] is string && !string.IsNullOrEmpty(row[1].ToString())
                select new SinglePullData(
                    row[0].ToString(), 
                    row[1].ToString(), 
                    string.IsNullOrWhiteSpace(row[2]?.ToString()) ? 0 : int.Parse(row[2]?.ToString()), 
                    row.Count >= 4 ? string.IsNullOrWhiteSpace(row[3]?.ToString()) ? 0 : int.Parse(row[3]?.ToString()) : 0,
                    row.Count >= 5 ? string.IsNullOrWhiteSpace(row[4]?.ToString()) ? 0 : int.Parse(row[4]?.ToString()) : 0,
                    row.Count == 6
                    )).ToList();
        }


        public void SetPullData(GoogleSpreadsheet spreadsheet, List<PullData> datas)
        {
            spreadsheet.SetData(datas, 2, 0);
        }

        public void SetStats(List<TypeStats> stats)
        {
            _statsSpreadsheet.SetData(stats, 1, 0);
        }
    }
}