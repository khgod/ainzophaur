﻿using SpreadsheetConnector;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PullPlayerData
{
    internal class Spreadsheet
    {
        private const string SpreadsheetId = "1Plvg4qcJHQlpqwzmghsYBKe-lgbvzpVOX4xaJpZ-BFs";
        private const int  GridId = 941392042;
        private readonly GoogleSpreadsheet _spreadsheet = new GoogleSpreadsheet(SpreadsheetId, GridId);

        public List<uint> GetIDs()
        {
            var values = _spreadsheet.GetData("Member Stats Data!B4:B33");
            if (values == null || values.Count <= 0)
                return null;

            var ids = new List<uint>();
            ids.AddRange(values.Select(row => uint.Parse(row[0].ToString())));
            return ids;
        }

        public void SetData(List<Player> players)
        {
            foreach (var player in players)
            {
                Console.WriteLine("Updating " + player.Name);
            }
            _spreadsheet.SetData(players, 3, 2);
        }
    }
}