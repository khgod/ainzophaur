﻿using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PullPlayerData
{
    internal class Selenium
    {
        public IWebDriver Driver { get; private set; }

        public bool SetupKh()
        {
            if (!XmlParser.LoadUserData(out var email, out var password, out var loginDelay))
                return false;

            Driver = new ChromeDriver {Url = @"https://www.nutaku.net/games/kamihime-r/play/"};
            Driver.FindElement(By.Id("playnow")).Click();
            Driver.FindElement(By.Id("loginButtonSingle")).Click();
            Driver.FindElement(By.CssSelector("input[type=email]")).SendKeys(email);
            Driver.FindElement(By.CssSelector("input[type=password]")).SendKeys(password + Keys.Enter);

            Thread.Sleep(loginDelay * 1000);
            Driver.Url =
                @"https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/mypage_quest_party_guild_enh_evo_gacha_present_shop_epi_acce_detail/app.html";
            return true;
        }


        public Player GetPlayerData(uint id)
        {
            Driver.Url = @"https://cf.r.kamihimeproject.dmmgames.com/v1/a_players/" + id;
            var profile = Driver.FindElement(By.TagName("Pre")).Text;
            Driver.Url = @"https://cf.r.kamihimeproject.dmmgames.com/v1/a_players/" + id + "/selected_party_info";
            var party = Driver.FindElement(By.TagName("Pre")).Text;
            Driver.Url = @"https://cf.r.kamihimeproject.dmmgames.com/v1/a_players/" + id + "/currency";
            var currency = Driver.FindElement(By.TagName("Pre")).Text;

            return new Player(profile, currency, party);
        }

        public void CloseBrowser()
        {
            Driver.Quit();
        }
    }
}