﻿using System;
using System.Linq;

namespace PullPlayerData
{
    internal class Program
    {
        private static void Main()
        {
            var spreadSheet = new Spreadsheet();
            var ids = spreadSheet.GetIDs();

            var selenium = new Selenium();
            if (!selenium.SetupKh())
            {
                Console.ReadKey();
                return;
            }
            spreadSheet.SetData(ids.Select(id => selenium.GetPlayerData(id)).ToList());
            selenium.CloseBrowser();

            Console.WriteLine("\n\n\nAll data parsed to the Spreadsheet!");
            Console.ReadKey();
        }
    }
}