﻿using System;
using System.IO;
using System.Xml.Linq;

namespace PullPlayerData
{
    internal class XmlParser
    {
        private const string UserDataConfigPath = @"..\..\..\UserData.xml";

        //Get Connection String
        public static bool LoadUserData(out string email, out string password, out int loginDelay)
        {
            XDocument document;

            if (!File.Exists(UserDataConfigPath))
            {
                document = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XComment("Enter Nutaku user details below:"),
                    new XElement("UserData", new XAttribute("email", "email"), new XAttribute("password", "password"),
                        new XAttribute("LoginDelay", 15)));
                email = null;
                password = null;
                loginDelay = 15;
                try
                {
                    document.Save(UserDataConfigPath);
                    Console.WriteLine("Created userdata file. Please insert your Nutaku username and password.");

                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to create UserData.xml: " + ex.Message + " - " + ex.StackTrace);
                    return false;
                }
            }

            try
            {
                document = XDocument.Load(UserDataConfigPath);

                email = document.Element("UserData").Attribute("email").Value;
                password = document.Element("UserData").Attribute("password").Value;
                loginDelay = int.Parse(document.Element("UserData").Attribute("LoginDelay").Value);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to load UserData.xml: " + ex.Message + " - " + ex.StackTrace);
                email = null;
                password = null;
                loginDelay = 15;
                return false;
            }
        }
    }
}