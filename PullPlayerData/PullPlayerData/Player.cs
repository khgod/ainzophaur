﻿using Newtonsoft.Json.Linq;
using SpreadsheetConnector;
using System.Collections.Generic;

namespace PullPlayerData
{
    internal class Player : IData
    {
        public Player(string profileJson, string currencyJson, string selectedPartyJson)
        {
            dynamic profile = JToken.Parse(profileJson);
            dynamic currency = JToken.Parse(currencyJson);
            dynamic selectedParty = JToken.Parse(selectedPartyJson);

            Name = profile.name;
            string lastLogin = profile.login_date;
            DaysOff = lastLogin.Contains("day") ? int.Parse(lastLogin.Split(' ')[0]) : 0;
            Rank = profile.rank;
            Hp = selectedParty.selected_party_info.total_hp;
            Atk = selectedParty.selected_party_info.total_attack;
            XpRemaining = profile.next_exp;
            Souls = profile.job_info.total_acquired_jobs;
            SoulPoints = currency.job_point;
            HolySoulPoints = currency.purgatory_job_point;
            Friends = profile.friend_num;
            FriendCap = profile.friend_max_num;
            Kamihime = profile.character_num;
            Weapons = profile.weapon_num;
            WeaponCap = profile.max_weapon_num;
            Eidolons = profile.summon_num;
            EidolonCap = profile.max_summon_num;
            Gems = currency.gem;
        }

        public string Name { get; }
        public int DaysOff { get; }
        public int Rank { get; }
        public int Hp { get; }
        public int Atk { get; }
        public int XpRemaining { get; }
        public int Souls { get; }
        public int SoulPoints { get; }
        public int HolySoulPoints { get; }
        public int Friends { get; }
        public int FriendCap { get; }
        public int Kamihime { get; }
        public int Weapons { get; }
        public int WeaponCap { get; }
        public int Eidolons { get; }
        public int EidolonCap { get; }
        public int Gems { get; }

        public List<int> GetData()
        {
            return new List<int>
            {
                DaysOff,
                Rank,
                Hp,
                Atk,
                XpRemaining,
                Souls,
                SoulPoints,
                HolySoulPoints,
                Friends,
                FriendCap,
                Kamihime,
                Weapons,
                WeaponCap,
                Eidolons,
                EidolonCap,
                Gems
            };
        }

        List<object> IData.GetData()
        {
            return new List<object>
            {
                DaysOff,
                Rank,
                Hp,
                Atk,
                XpRemaining,
                Souls,
                SoulPoints,
                HolySoulPoints,
                Friends,
                FriendCap,
                Kamihime,
                Weapons,
                WeaponCap,
                Eidolons,
                EidolonCap,
                Gems
            };
        }
    }
}