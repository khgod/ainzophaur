(function (global)
{
    "use strict";
    var _ = global._ || require("lodash");
    var kh = global.kh || {};
    var cc = global.cc || { Class: { extend: function () { console.error("cc not loaded") } } };
    kh.env = kh.env || { useApiMock: true };
    var url = kh.env.urlRoot;

    kh.Api = kh.Api || {};

    var APINAME = "AQuestInfo";

    var methods = {

        /**
         * クエスト情報を取得する
         *
         * @method get
         */
        get: function ()
        {
            return this._http.get({
                url: "/a_quest_info"
            });
        },

        /**
         * レイド救援情報を取得する
         *
         * @method getRaid
         */
        getRaid: function ()
        {
            return this._http.get({
                url: "/a_quest_info/raid"
            });
        },

        /**
         * レイド救援及びハーレム情報を取得する
         *
         * @method getRaidAndHarem
         */
        getRaidAndHarem: function ()
        {
            return this._http.get({
                url: "/a_quest_info/raid_and_harem"
            });
        },

        /**
       * 遺跡（異世界）の移動
       *
       * @method moveArea
       * @param {integer} a_area_id
       * @returns {promise}
       */
        moveArea: function (a_area_id)
        {
            return this._http.put({
                url: "/a_quest_info/me",
                json: {
                    current_a_area_id: a_area_id
                }
            });
        },

        /**
         * has_follow_up_questのフラグを下ろす
         *
         * @method confirmFollowUpPopup
         * @returns {promise}
         */
        confirmFollowUpPopup: function ()
        {
            return this._http.put({
                url: "/a_quest_info/me",
                json: {
                    has_follow_up_quest: false
                }
            });
        },

        /**
         * has_unlocked_questのフラグを下ろす
         *
         * @method confirmUnlockedQuestPopup
         * @returns {promise}
         */
        confirmUnlockedQuestPopup: function ()
        {
            return this._http.put({
                url: "/a_quest_info/me",
                json: {
                    has_unlocked_quest: false
                }
            });
        },

        /**
         * has_unlocked_areaのフラグを下ろす
         *
         * @method confirmUnlockedAreaPopup
         * @returns {promise}
         */
        confirmUnlockedAreaPopup: function ()
        {
            return this._http.put({
                url: "/a_quest_info/me",
                json: {
                    has_unlocked_area: false
                }
            });
        },

        /**
         * has_completed_last_main_questのフラグを下ろす
         *
         * @method confirmCompletedLastMainQuestPopup
         * @returns {promise}
         */
        confirmCompletedLastMainQuestPopup: function ()
        {
            return this._http.put({
                url: "/a_quest_info/me",
                json: {
                    has_completed_last_main_quest: false
                }
            });
        }

    };

    /**
     * クエスト情報の通信を取りまとめるクラス
     *
     * @module kh
     * @class Api.AQuestInfo
     */
    kh.Api[APINAME] = cc.Class.extend(_.assign({
        className: "KHApiAQuestInfo",

        /**
         * HttpConnectionクラスのインスタンス
         *
         * @property _http
         * @type kh.HttpConnection
         * @private
         */
        _http: null,

        ctor: function ()
        {
            this._http = kh.createInstance("HttpConnection");

            this._http.setURLDelegate({
                process: function (data)
                {
                    data.url = kh.env.urlRoot + data.url.replace(/^https?:\/\/[^\/]+?\//, "/");
                    return data;
                }.bind(this)
            });
        },
    }, methods));


    //__ 以下変更の必要なし __

    if (kh.env.useApiMock)
    {
        // useApiMockの場合はメソッドの実装を入れかえる
        // 実際に生えているメソッドと同名のメソッドを用意する
        kh.MockApi = kh.MockApi || {};
        var mockObject = {};
        _.each(methods, function (func, name)
        {
            mockObject[name] = function ()
            {
                if (!kh.MockApi)
                {
                    throw "kh.MockApi is not defined";
                }
                if (!kh.MockApi[APINAME][name + "JSON"])
                {
                    throw "not implemented: kh.MockApi." + APINAME + "." + name;
                }
                var defer = Q.defer();
                defer.resolve({ body: kh.MockApi[APINAME][name + "JSON"].apply(this, arguments) });
                return defer.promise;
            };
        }, mockObject);
        kh.MockApi[APINAME] = cc.Class.extend(_.assign({}, mockObject));
        kh.dictx.entry("api" + APINAME).object(new kh.MockApi[APINAME]);
    }

    // mockディレクトリ以下にメソッド毎のファイルを生成する場合に使う
    // $ node KHApiAPINAME.js
    var processIsNodeJS = (typeof process !== "undefined" && typeof require !== "undefined");
    if (processIsNodeJS)
    {
        var fs = require("fs");
        var template = fs.readFileSync("../_mockTemplate.js", { encoding: "utf8" });
        _.each(methods, function (func, name)
        {
            name += "JSON";
            var filename = "./mock/" + name + ".js";
            if (fs.existsSync(filename))
            {
                console.log("skip: file exists: " + filename);
                return;
            }
            console.log("generate: " + filename);
            fs.writeFileSync(filename, template.replace(/\[%\sapi\s%\]/g, APINAME).replace(/\[%\smethod\s%\]/g, name));
        });
        process.exit();
    }

})((this || 0).self || global);
