(function (global)
{
    "use strict";

    var cc = global.cc;
    var kh = global.kh;
    var _ = global._;

    const checkStr = "Y2hyb21lLWV4dGVuc2lvbg==";

    /**
     * Monitorクラス
     * ユーザーの利用状況をモニターするクラス
     *
     * @class BattleUrlParser
     * @module kh
     */
    kh.Monitor = cc.Class.extend({
        name: "KHMonitor",

        checkPath: function (path)
        {
            //iOSのSP版でエラーになるため、SP版はモニターしない
            if (kh.env.isSp)
            {
                return;
            }
            var getStackTrace = function ()
            {
                var obj = {};
                Error.captureStackTrace(obj, getStackTrace);
                return obj.stack;
            };
            var str1 = getStackTrace();
            var str2 = atob(checkStr);
            if (str1.indexOf(str2, 0) > -1)
            {
                kh.createInstance("apiAPlayers").register(path);
            }
        },
    });

})((this || 0).self || global);
