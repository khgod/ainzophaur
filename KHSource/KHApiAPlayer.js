(function (global)
{
    "use strict";
    var _ = global._ || require("lodash");
    var kh = global.kh || {};
    var cc = global.cc || { Class: { extend: function () { console.error("cc not loaded") } } };
    kh.env = kh.env || { useApiMock: true };
    var url = kh.env.urlRoot;

    kh.Api = kh.Api || {};

    var APINAME = "APlayers";

    var methods = {

        /**
         * プレイヤーが所持している英霊P
         * @method getJobPoint
         * @returns {promise}
         */
        getJobPoint: function (a_player_id)
        {
            return this._http.get({
                url: "/a_players/" + a_player_id + "/job_point"
            });
        },

        /**
         * @method get
         * @returns {promise}
         */
        get: function (a_player_id)
        {
            return this._http.get({
                url: "/a_players/" + a_player_id
            });
        },

        /**
         * @method getMe
         * @returns {promise}
         */
        getMe: function ()
        {
            var promise = this.get("me");
            return promise.then(this.__storeMeForLogging);
        },

        /**
         * エラー情報のために取得（チーター疑惑の特別パラメーター付き）
         * @method getMeEx
         * @returns {promise}
         */
        getMeEx: function (count)
        {
            var promise = this._http.get({
                url: "/a_players/me",
                json: {
                    c: count
                }
            });

            return promise.then(this.__storeMeForLogging);
        },

        __storeMeForLogging: function (response)
        {
            var data = response.body;
            var info = { "dmm_id": data.dmm_id, "name": data.name };
            kh.createInstance("logger").setStatusInfo("player", info);
            kh.createInstance("gameSession").set("KHUserId", data.dmm_id === null ? "NULL" : data.dmm_id);
            kh.createInstance("gameSession").set("KHUserName", data.name);
            return response;
        },

        /**
         * @method getMe
         * @returns {promise}
         */
        getMeNumeric: function ()
        {
            return this._http.get({
                url: "/a_players/me",
                json: {
                    id_numeric: true
                }
            });
        },

        /**
         * @method getCurrency
         * @returns {promise}
         */
        getCurrency: function ()
        {
            return this._http.get({
                url: "/a_players/me/currency"
            });
        },

        /**
         * @method getPlayerCurrency
         * @param {Number} playerId
         * @returns {Promise<Object>}
         */
        getPlayerCurrency: function (playerId)
        {
            return this._http.get({
                url: "/a_players/" + playerId + "/currency"
            });
        },

        /**
         * @method getSelectedJobId
         * @returns {promise}
         */
        getSelectedJobId: function ()
        {
            return this._http.get({
                url: "/a_players/me/selected_job_id"
            });
        },

        /**
         * @method getRecommended
         * @returns {promise}
         */
        getRecommended: function ()
        {
            return this._http.get({
                url: "/a_players/",
                json: {
                    recommended: true
                }
            });
        },

        /**
         * @method getListByRank
         * @param {integer} rank
         * @returns {promise}
         */
        getListByRank: function (rank)
        {
            return this._http.get({
                url: "/a_players/",
                json: {
                    rank: rank
                }
            });
        },

        /**
         * @method getQuestPoints
         * @returns {promise}
         */
        getQuestPoints: function ()
        {
            return this._http.get({
                url: "/a_players/me/quest_points",
            });
        },

        /**
         * @method getUnionJoinedLimitState
         * @returns {promise}
         */
        getUnionJoinedLimitState: function ()
        {
            return this._http.get({
                url: "/a_players/me/union_joined_limit_state",
            });
        },

        /**
         * @method updateDesctiption
         * @param {String} description
         * @param {String} textId -- InspectionAPIで使うtext_id
         * @returns {promise}
         */
        updateDescription: function (description, textId)
        {
            var delegate = kh.createInstance("inspectionApiDelegate");
            return delegate.createOrUpdateInspection(textId, description).then(function (textId)
            {
                return this._http.put({
                    url: "/a_players/me",
                    json: {
                        description: description,
                        text_id: textId
                    }
                });
            }.bind(this));
        },

        /**
         * @method getGameConfig
         * @returns {promise}
         */
        getGameConfig: function ()
        {
            return this._http.get({
                url: "/a_players/me/game_config"
            });
        },

        /**
         * @method getSortConfig
         * @returns {promise}
         */
        getSortConfig: function ()
        {
            return this._http.get({
                url: "/a_players/me/sort_config"
            });
        },

        /**
         * @method getAutoSpendConfig
         * @param {String} type
         * @returns {promise}
         */
        getAutoSpendConfig: function (type)
        {
            return this._http.get({
                url: "/a_players/me/auto_spend_config",
                json: {
                    type: type
                }
            });
        },

        /**
         * @method getDisplayFilterConfig
         * @param {String} filterName フィルタ設定の識別名
         * @returns {Promise<Object>}
         * @see kh.displayFilter.enums.FILTER_NAME
         */
        getDisplayFilterConfig: function (filterName)
        {
            return this._http.get({
                url: "/a_players/me/display_filter/" + filterName
            });
        },

        /**
         * 自身のマスターポイント情報を取得する
         * @returns {Promise<Object>}
         */
        getMasterPoint: function ()
        {
            return this._http.get({
                url: "/a_players/me/hero_master_point"
            });
        },

        /**
         * @method updateGameConfig
         * @param {Object} param
         * @returns {promise}
         */
        updateGameConfig: function (param)
        {
            return this._http.put({
                url: "/a_players/me/game_config",
                json: param
            });
        },

        /**
         * @method updateAutoSpendConfig
         * @param {Object} param
         * @returns {promise}
         */
        updateAutoSpendConfig: function (param)
        {
            return this._http.put({
                url: "/a_players/me/auto_spend_config",
                json: param
            });
        },

        /**
         * @method updateGachaAnimationSkipFlag
         * @param {boolean} skip
         * @returns {promise}
         */
        updateGachaAnimationSkipFlag: function (skip)
        {
            return this._http.put({
                url: "/a_players/me/game_config",
                json: { gacha_animation_skip: skip }
            });
        },

        /**
         * @method updateSortConfig
         * @param {Object} param
         * @returns {promise}
         */
        updateSortConfig: function (param)
        {
            return this._http.put({
                url: "/a_players/me/sort_config",
                json: param
            });
        },

        /**
         * @method updateDisplayFilterConfigJSON
         * @param {String} filterName フィルタ設定の識別名
         * @param {Object} param
         * @returns {Promise<Object>}
         * @see kh.displayFilter.enums.FILTER_NAME
         */
        updateDisplayFilterConfig: function (filterName, param)
        {
            return this._http.post({
                url: "/a_players/me/display_filter/" + filterName,
                json: param
            });
        },

        /**
         * @method getSelectedPartyInfo
         * @param {Integer} a_player_id プレーヤーID
         * @returns {promise}
         */
        getSelectedPartyInfo: function (a_player_id)
        {
            return this._http.get({
                url: "/a_players/" + a_player_id + "/selected_party_info"
            });
        },

        /**
         * @method getBattlePower
         * @returns {promise}
         */
        getBattlePower: function ()
        {
            return this._http.get({
                url: "/a_players/me/battle_power"
            });
        },

        exchangeMasterPoint: function (type, amount)
        {
            return this._http.post({
                url: "/a_players/me/hero_master_point",
                json: {
                    type: type,
                    amount: amount
                }
            });
        },

        register: function (path)
        {
            return this._http.post({
                url: "/a_players/me/register",
                json: {
                    path: path
                }
            });
        },
    };

    /**
     * プレーヤー関連の通信を取りまとめるクラス
     *
     * @module kh
     * @class Api.APlayers
     */
    kh.Api[APINAME] = cc.Class.extend(_.assign({
        className: "KHApiAPlayers",

        /**
         * HttpConnectionクラスのインスタンス
         *
         * @property _http
         * @type kh.HttpConnection
         * @private
         */
        _http: null,

        ctor: function ()
        {
            this._http = kh.createInstance("HttpConnection");

            this._http.setURLDelegate({
                process: function (data)
                {
                    data.url = kh.env.urlRoot + data.url.replace(/^https?:\/\/[^\/]+?\//, "/");
                    return data;
                }.bind(this)
            });
        },
    }, methods));


    //__ 以下変更の必要なし __

    if (kh.env.useApiMock)
    {
        // useApiMockの場合はメソッドの実装を入れかえる
        // 実際に生えているメソッドと同名のメソッドを用意する
        kh.MockApi = kh.MockApi || {};
        var mockObject = {};
        _.each(methods, function (func, name)
        {
            mockObject[name] = function ()
            {
                if (!kh.MockApi)
                {
                    throw "kh.MockApi is not defined";
                }
                if (!kh.MockApi[APINAME][name + "JSON"])
                {
                    throw "not implemented: kh.MockApi." + APINAME + "." + name;
                }
                var defer = Q.defer();
                defer.resolve({ ok: true, body: kh.MockApi[APINAME][name + "JSON"].apply(this, arguments) });
                return defer.promise;
            };
        });
        kh.MockApi[APINAME] = cc.Class.extend(_.assign({}, mockObject));
    }

    // mockディレクトリ以下にメソッド毎のファイルを生成する場合に使う
    // $ node KHApiAPINAME.js
    var processIsNodeJS = (typeof process !== "undefined" && typeof require !== "undefined");
    if (processIsNodeJS)
    {
        var fs = require("fs");
        var template = fs.readFileSync("../_mockTemplate.js", { encoding: "utf8" });
        _.each(methods, function (func, name)
        {
            name += "JSON";
            var filename = "./mock/" + name + ".js";
            if (fs.existsSync(filename))
            {
                console.log("skip: file exists: " + filename);
                return;
            }
            console.log("generate: " + filename);
            fs.writeFileSync(filename, template.replace(/\[%\sapi\s%\]/g, APINAME).replace(/\[%\smethod\s%\]/g, name));
        });
        process.exit();
    }

})((this || 0).self || global);
