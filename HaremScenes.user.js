// ==UserScript==
// @name         Harem Scenes
// @namespace    http://tampermonkey.net/
// @version      04.07.2020
// @description  Do all unconfirmed Harem Scenes
// @author       You
// @include      https://www.nutaku.net/games/kamihime-r/play/
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/top/app.html*
// @include      https://cf.r.kamihimeproject.dmmgames.com/front/cocos2d-proj/components-pc/game/app.html*
// @grant        none
// @run-at       document-end
// ==/UserScript==

class Harem {
    constructor() {
        this.questApi = kh.createInstance("apiAQuests");
        this.battleApi = kh.createInstance("apiABattles");
    }

    async start() {
        this.unconfirmedScenes = (await this.questApi.getUnconfirmedHaremEpisodes(1, 100000)).body.data;
        while(this.unconfirmedScenes.length > 0) {
            await this.doQuest();
            this.unconfirmedScenes = (await this.questApi.getUnconfirmedHaremEpisodes(1, 100000)).body.data;
        }
        console.log("Done.")
    }

    async doQuest() {
        let scene = this.unconfirmedScenes[0];
        console.log(`Watching ${scene.harem_episode_info.heroines[0].name} - ${scene.title}`);

        await this.questApi.startQuest(scene.quest_id, scene.type); // Start quest
        while ((await this.questApi.getNextState(scene.a_quest_id, scene.type)).body.next_info.next_kind !== "episode_result") {} // Get next stage until we get episode result
        await this.battleApi.getBattleResult(scene.a_quest_id, scene.type);
    }
}

function sleep(ms)
{
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function start(){
    while (!(kh && kh.createInstance && kh.Api && kh.Api.AWeapons)) {
        console.log("sleep");
        await sleep(1000);
    }

    try {
        console.log("Start Harem!")
        let harem = new Harem();
        await harem.start();
    } catch (exception) {
        console.log(exception.message)
        await sleep(1000)
        start();
    }
}

start();